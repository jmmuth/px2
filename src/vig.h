//
// Created by chenwx on 1/16/17.
//

#ifndef UBCSAT_VIG_H
#define UBCSAT_VIG_H

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/filtered_graph.hpp>

namespace boost {
    typedef adjacency_list<hash_setS, vecS, undirectedS> Graph;
}

#endif //UBCSAT_VIG_H
