/*

      ##  ##  #####    #####   $$$$$   $$$$   $$$$$$    
      ##  ##  ##  ##  ##      $$      $$  $$    $$      
      ##  ##  #####   ##       $$$$   $$$$$$    $$      
      ##  ##  ##  ##  ##          $$  $$  $$    $$      
       ####   #####    #####  $$$$$   $$  $$    $$      
  ======================================================
  SLS SAT Solver from The University of British Columbia
  ======================================================
  ...... Developed & Maintained by Dave Tompkins .......
  ------------------------------------------------------
  ...... consult legal.txt for legal information .......
  ------------------------------------------------------
  .... project website: http://ubcsat.dtompkins.com ....
  ------------------------------------------------------
  source repository: https://github.com/dtompkins/ubcsat
  ------------------------------------------------------
  ..... contact us at ubcsat [@] googlegroups.com ......
  ------------------------------------------------------

*/

#include "ubcsat.h"

#ifdef __cplusplus
namespace ubcsat {
#endif

/*
    This file contains the code to make the various data triggers work
*/

/***** Trigger ReadCNF *****/

    void ReadCNF();

    UINT32 iNumVars;
    UINT32 iNumClauses;
    UINT32 iNumLits;

    UINT32 *aClauseLen;
    LITTYPE **pClauseLits;
    UINT32 iMaxClauseLen;

    UBIGINT *aClauseWeight;
    UBIGINT iTotalClauseWeight;

    UINT32 iVARSTATELen;


/***** Trigger LitOccurence *****/

    void CreateLitOccurence();

    UINT32 *aNumLitOcc;
    UINT32 *aLitOccData;
    UINT32 **pLitClause;


/***** Trigger CandidateList *****/

    void CreateCandidateList();

    UINT32 *aCandidateList;
    UINT32 iNumCandidates;
    UINT32 iMaxCandidates;


/***** Trigger InitVarsFromFile *****/
/***** Trigger DefaultInitVars *****/

    void InitVarsFromFile();

    void DefaultInitVars();

    UINT32 *aVarInit;
    UINT32 iInitVarFlip;
    BOOL bVarInitGreedy;

/***** Trigger DefaultStateInfo *****/

    void CreateDefaultStateInfo();

    void InitDefaultStateInfo();

    UINT32 *aNumTrueLit;
    BOOL *aVarValue;
    UINT32 iNumFalse;
    UBIGINT iSumFalseWeight;


/***** Trigger DefaultFlip[W] *****/

    void DefaultFlip();

    void DefaultFlipW();


/***** Trigger CheckTermination *****/

    void CheckTermination();


/***** Trigger FalseClauseList *****/
/***** Trigger Flip+FalseClauseList[W] *****/

    void CreateFalseClauseList();

    void InitFalseClauseList();

    void UpdateFalseClauseList();

    void FlipFalseClauseList();

    void FlipFalseClauseListW();

    UINT32 *aFalseList;
    UINT32 *aFalseListPos;
    UINT32 iNumFalseList;


/***** Trigger VarScore[W] *****/
/***** Trigger Flip+VarScore[W] *****/

    void CreateVarScore();

    void InitVarScore();

    void UpdateVarScore();

    void FlipVarScore();

    void CreateVarScoreW();

    void InitVarScoreW();

    void UpdateVarScoreW();

    void FlipVarScoreW();

    SINT32 *aVarScore;
    SBIGINT *aVarScoreWeight;


/***** Trigger MakeBreak[W] *****/
/***** Trigger Flip+MakeBreak[W] *****/

    void CreateMakeBreak();

    void InitMakeBreak();

    void UpdateMakeBreak();

    void FlipMakeBreak();

    void CreateMakeBreakW();

    void InitMakeBreakW();

    void UpdateMakeBreakW();

    void FlipMakeBreakW();

    UINT32 *aBreakCount;
    UINT32 *aMakeCount;
    UINT32 *aCritSat;
    UBIGINT *aBreakCountWeight;
    UBIGINT *amakeCountWeight;


/***** Trigger VarInFalse *****/
/***** Trigger Flip+VarInFalse *****/
/*
    aVarInFalseList[j]    variable # for the jth variable that appears in false clauses
    aVarInFalseListPos[j] for variable[j], position it occurs in aVarInFalseList
    iNumVarsInFalseList   # variables that appear in false clauses
*/

    void CreateVarInFalse();

    void InitVarInFalse();

    void UpdateVarInFalse();

    void FlipVarInFalse();

    UINT32 iNumVarsInFalseList;
    UINT32 *aVarInFalseList;
    UINT32 *aVarInFalseListPos;


/***** Trigger VarLastChange *****/
/*
    aVarLastChange[j]     the step # of the most recent time variable[j] was flipped
    iVarLastChangeReset   the step # of the last time all aVarLastChange values were reset
*/
    void CreateVarLastChange();

    void InitVarLastChange();

    void UpdateVarLastChange();

    UBIGINT *aVarLastChange;
    UBIGINT iVarLastChangeReset;


/***** Trigger TrackChanges[W] *****/
/*
    iNumChanges           # of changes to aVarScore[] values this step
    aChangeList[j]        variable # of the jth variable changed this step
    aChangeOldScore[j]    the previous score of variable[j]
    aChangeLastStep[j]    the step of the last change for variable[j]
*/

    void CreateTrackChanges();

    void InitTrackChanges();

    void UpdateTrackChanges();

    void FlipTrackChanges();

    void FlipTrackChangesFCL();

    void CreateTrackChangesW();

    void InitTrackChangesW();

    void UpdateTrackChangesW();

    void FlipTrackChangesW();

    void FlipTrackChangesFCLW();

    UINT32 iNumChanges;
    UINT32 *aChangeList;
    SINT32 *aChangeOldScore;
    UBIGINT *aChangeLastStep;

    UINT32 iNumChangesW;
    UINT32 *aChangeListW;
    SBIGINT *aChangeOldScoreWeight;
    UBIGINT *aChangeLastStepWeight;


/***** Trigger DecPromVars[W] *****/

    void CreateDecPromVars();

    void InitDecPromVars();

    void UpdateDecPromVars();

    void CreateDecPromVarsW();

    void InitDecPromVarsW();

    void UpdateDecPromVarsW();

    UINT32 *aDecPromVarsList;
    UINT32 iNumDecPromVars;

    UINT32 *aDecPromVarsListW;
    UINT32 iNumDecPromVarsW;

/***** Trigger Flip+DecPromVars+FCL *****/
    void FlipDecPromVarsFCL();


/***** Trigger BestScoreList *****/

    void CreateBestScoreList();

    void InitBestScoreList();

    void UpdateBestScoreList();

    UINT32 iNumBestScoreList;
    UINT32 *aBestScoreList;
    UINT32 *aBestScoreListPos;


/***** Trigger ClausePenaltyFL[W] *****/

    void CreateClausePenaltyFL();

    void InitClausePenaltyFL();

    void InitClausePenaltyFLW();

    FLOAT *aClausePenaltyFL;
    BOOL bClausePenaltyCreated;
    BOOL bClausePenaltyFLOAT;
    FLOAT fBasePenaltyFL;
    FLOAT fTotalPenaltyFL;


/***** Trigger MakeBreakPenaltyFL *****/

    void CreateMakeBreakPenaltyFL();

    void InitMakeBreakPenaltyFL();

    void UpdateMakeBreakPenaltyFL();

    void FlipMBPFLandFCLandVIF();

    void FlipMBPFLandFCLandVIFandW();

    FLOAT *aMakePenaltyFL;
    FLOAT *aBreakPenaltyFL;


/***** Trigger ClausePenaltyINT *****/

    void CreateClausePenaltyINT();

    void InitClausePenaltyINT();

    void InitClausePenaltyINTW();

    UINT32 *aClausePenaltyINT;
    UINT32 iInitPenaltyINT;
    UINT32 iBasePenaltyINT;
    UINT32 iTotalPenaltyINT;


/***** Trigger MakeBreakPenalty *****/

    void CreateMakeBreakPenaltyINT();

    void InitMakeBreakPenaltyINT();

    void UpdateMakeBreakPenaltyINT();

    void FlipMBPINTandFCLandVIF();

    void FlipMBPINTandFCLandVIFandW();

    UINT32 *aMakePenaltyINT;
    UINT32 *aBreakPenaltyINT;


/***** Trigger NullFlips *****/

    UBIGINT iNumNullFlips;

    void InitNullFlips();

    void UpdateNullFlips();


/***** Trigger LocalMins *****/

    UBIGINT iNumLocalMins;

    void InitLocalMins();

    void UpdateLocalMins();


/***** Trigger LogDist *****/

    void CreateLogDist();

    UBIGINT *aLogDistValues;
    UINT32 iNumLogDistValues;
    UINT32 iLogDistStepsPerDecade;


/***** Trigger BestFalse *****/

    void InitBestFalse();

    void UpdateBestFalse();

    UINT32 iBestNumFalse;
    UBIGINT iBestStepNumFalse;
    UBIGINT iBestSumFalseWeight;
    UBIGINT iBestStepSumFalseWeight;


/***** Trigger SaveBest *****/

    void CreateSaveBest();

    void UpdateSaveBest();

    VARSTATE vsBest;


/***** Trigger StartFalse *****/

    void UpdateStartFalse();

    UINT32 iStartNumFalse;
    UBIGINT iStartSumFalseWeight;


/***** Trigger CalcImproveMean *****/

    void CalcImproveMean();

    FLOAT fImproveMean;
    FLOAT fImproveMeanW;


/***** Trigger FirstLM *****/

    void InitFirstLM();

    void UpdateFirstLM();

    void CalcFirstLM();

    UINT32 iFirstLM;
    UBIGINT iFirstLMStep;
    UBIGINT iFirstLMWeight;
    UBIGINT iFirstLMStepW;


/***** Trigger FirstLMRatio *****/

    void CalcFirstLMRatio();

    FLOAT fFirstLMRatio;
    FLOAT fFirstLMRatioW;


/***** Trigger TrajBestLM *****/

    void UpdateTrajBestLM();

    void CalcTrajBestLM();

    UINT32 iTrajBestLMCount;
    FLOAT fTrajBestLMSum;
    FLOAT fTrajBestLMSum2;
    UINT32 iTrajBestLMCountW;
    FLOAT fTrajBestLMSumW;
    FLOAT fTrajBestLMSum2W;

    FLOAT fTrajBestLMMean;
    FLOAT fTrajBestLMMeanW;
    FLOAT fTrajBestLMCV;
    FLOAT fTrajBestLMCVW;


/***** Trigger NoImprove *****/

    void CheckNoImprove();

    UBIGINT iNoImprove;


/***** Trigger EarlyTerm *****/

    void CheckEarlyTerm();

    UBIGINT iEarlyTermSteps;
    UINT32 iEarlyTermQual;
    UBIGINT iEarlyTermQualWeight;


/***** Trigger Strikes *****/

    void CheckStrikes();

    UINT32 iStrikes;


/***** Trigger StartSeed *****/

    void StartSeed();

    UINT32 iStartSeed;


/***** Trigger CountRandom *****/


/***** Trigger CheckTimeout *****/

    void CheckTimeout();

    UINT32 iTimeResolution;


/***** Trigger CheckForRestarts *****/

    void CheckForRestarts();


/***** Trigger FlipCounts *****/

    void CreateFlipCounts();

    void InitFlipCounts();

    void UpdateFlipCounts();

    UBIGINT *aFlipCounts;


/***** Trigger FlipCountStats *****/

    void FlipCountStats();

    FLOAT fFlipCountsMean;
    FLOAT fFlipCountsCV;
    FLOAT fFlipCountsStdDev;


/***** Trigger BiasCounts *****/

    void CreateBiasCounts();

    void PreInitBiasCounts();

    void UpdateBiasCounts();

    void FinalBiasCounts();

    UBIGINT *aBiasTrueCounts;
    UBIGINT *aBiasFalseCounts;


/***** Trigger BiasStats *****/

    void BiasStats();

    FLOAT fMeanFinalBias;
    FLOAT fMeanMaxBias;


/***** Trigger UnsatCounts *****/

    void CreateUnsatCounts();

    void InitUnsatCounts();

    void UpdateUnsatCounts();

    UBIGINT *aUnsatCounts;


/***** Trigger UnsatCountStats *****/

    void UnsatCountStats();

    FLOAT fUnsatCountsMean;
    FLOAT fUnsatCountsCV;
    FLOAT fUnsatCountsStdDev;


/***** Trigger NumFalseCounts *****/

    void CreateNumFalseCounts();

    void InitNumFalseCounts();

    void UpdateNumFalseCounts();

    UBIGINT *aNumFalseCounts;
    UBIGINT *aNumFalseCountsWindow;


/***** Trigger DistanceCounts *****/

    void CreateDistanceCounts();

    void InitDistanceCounts();

    void UpdateDistanceCounts();

    UBIGINT *aDistanceCounts;
    UBIGINT *aDistanceCountsWindow;


/***** Trigger ClauseLast *****/

    void CreateClauseLast();

    void InitClauseLast();

    void UpdateClauseLast();

    UBIGINT *aClauseLast;


/***** Trigger SQGrid *****/

    void CreateSQGrid();

    void InitSQGrid();

    void UpdateSQGrid();

    void FinishSQGrid();

    UBIGINT *aSQGridWeight;
    UINT32 *aSQGrid;


/***** Trigger PenaltyStats *****/

    void CreatePenaltyStats();

    void InitPenaltyStats();

    void UpdatePenaltyStatsStep();

    void UpdatePenaltyStatsRun();

    FLOAT *aPenaltyStatsMean;
    FLOAT *aPenaltyStatsStddev;
    FLOAT *aPenaltyStatsCV;

    FLOAT *aPenaltyStatsSum;
    FLOAT *aPenaltyStatsSum2;

    FLOAT *aPenaltyStatsMeanSum;
    FLOAT *aPenaltyStatsMeanSum2;
    FLOAT *aPenaltyStatsStddevSum;
    FLOAT *aPenaltyStatsStddevSum2;
    FLOAT *aPenaltyStatsCVSum;
    FLOAT *aPenaltyStatsCVSum2;


/***** Trigger VarFlipHistory *****/

    void CreateVarFlipHistory();

    void UpdateVarFlipHistory();

    UINT32 *aVarFlipHistory;
    UINT32 iVarFlipHistoryLen;


/***** Trigger MobilityWindow *****/

    void CreateMobilityWindow();

    void InitMobilityWindow();

    void UpdateMobilityWindow();

    UINT32 *aMobilityWindowVarChange;
    UINT32 *aMobilityWindow;
    FLOAT *aMobilityWindowSum;
    FLOAT *aMobilityWindowSum2;


/***** Trigger MobilityFixedFrequencies *****/

    void CreateMobilityFixedFrequencies();

    void InitMobilityFixedFrequencies();

    void UpdateMobilityFixedFrequencies();

    UINT32 *aMobilityFixedFrequencies;


/***** Trigger VarAgeFrequencies *****/

    void CreateVarAgeFrequencies();

    void InitVarAgeFrequencies();

    void UpdateVarAgeFrequencies();

    UINT32 iMaxVarAgeFrequency;
    UBIGINT *aVarAgeFrequency;


/***** Trigger AutoCorr *****/

    void CreateAutoCorr();

    void InitAutoCorr();

    void UpdateAutoCorr();

    void CalcAutoCorr();

    UINT32 iAutoCorrMaxLen;
    FLOAT fAutoCorrCutoff;
    UINT32 iAutoCorrLen;
    FLOAT *aAutoCorrValues;
    FLOAT *aAutoCorrStartBuffer;
    FLOAT *aAutoCorrEndCircBuffer;
    FLOAT fAutoCorrSum;
    FLOAT fAutoCorrSum2;
    FLOAT *aAutoCorrCrossSum;

/***** Trigger AutoCorrOne *****/

    void InitAutoCorrOne();

    void UpdateAutoCorrOne();

    void CalcAutoCorrOne();

    FLOAT fAutoCorrOneVal;
    FLOAT fAutoCorrOneEst;
    FLOAT fAutoCorrOneStart;
    FLOAT fAutoCorrOneLast;
    FLOAT fAutoCorrOneSum;
    FLOAT fAutoCorrOneSum2;
    FLOAT fAutoCorrOneCrossSum;


/***** Trigger BranchFactor *****/

    void BranchFactor();

    void BranchFactorW();

    FLOAT fBranchFactor;
    FLOAT fBranchFactorW;


/****** Trigger StepsUpDownSide *****/

    void InitStepsUpDownSide();

    void UpdateStepsUpDownSide();

    UBIGINT iNumUpSteps;
    UBIGINT iNumDownSteps;
    UBIGINT iNumSideSteps;
    UBIGINT iNumUpStepsW;
    UBIGINT iNumDownStepsW;
    UBIGINT iNumSideStepsW;

/****** Trigger NumRestarts *****/

    void NumRestarts();

    UINT32 iNumRestarts;

/***** Trigger LoadKnownSolutions *****/

    void LoadKnownSolutions();

    VARSTATELIST vslKnownSoln;
    BOOL bKnownSolutions;


/***** Trigger SolutionDistance *****/

    void CreateSolutionDistance();

    void UpdateSolutionDistance();

    VARSTATE vsSolutionDistanceCurrent;
    VARSTATE vsSolutionDistanceClosest;
    UINT32 iSolutionDistance;


/***** Trigger FDCRun *****/

    void CreateFDCRun();

    void InitFDCRun();

    void UpdateFDCRun();

    void CalcFDCRun();

    FLOAT fFDCRun;

    FLOAT fFDCRunHeightDistanceSum;
    FLOAT fFDCRunHeightSum;
    FLOAT fFDCRunHeightSum2;
    FLOAT fFDCRunDistanceSum;
    FLOAT fFDCRunDistanceSum2;
    UINT32 iFDCRunCount;


/***** Trigger DynamicParms *****/

    void DynamicParms();


/***** Trigger FlushBuffers *****/

    void FlushBuffers();


/***** Trigger CheckWeighted *****/

    void CheckWeighted();


/***** Trigger UniqueSolutions *****/

    void CreateUniqueSolutions();

    void UpdateUniqueSolutions();

    VARSTATELIST vslUnique;
    VARSTATE vsCheckUnique;
    UINT32 iNumUniqueSolutions;
    UINT32 iLastUnique;


/***** Trigger VarsShareClauses *****/

    UINT32 *aNumVarsShareClause;
    UINT32 **pVarsShareClause;

    void CreateVarsShareClauses();


/***** Trigger InitVIG *****/
    void InitVIG();

    void PxBestnPn();

    void PxMultiBsf();

    std::vector<std::vector<UINT32>> DecomposeVigPseudoBackbone();

    void FindOffspringCur(const std::vector<std::vector<UINT32>> &vvComponentToVar);

    UINT32 FindOffspring(boost::dynamic_bitset<> &vP1, UINT32 iP1Eval,
                         const std::vector<std::vector<UINT32>> &vvComponentToVar, BOOL);

    UINT32 NaiveComputeEvaluation(boost::dynamic_bitset<> &vSol);

    UINT32 NaiveComputeEvaluation(BOOL *aSol);

    boost::Graph *G;
    std::vector<UINT32> *vVarToComponentId;
    std::vector<UINT32> *vPxVars;
    BOOL bPxFlip;
    UINT32 *aTempNumTrueLit;
    BOOL *aPseudoBackbone;
    UBIGINT iNumPx;
    double fPxInitTime;

    std::vector<UINT32> vNumDiffBits;
    std::vector<UINT32> vNumComp;
    std::vector<UINT32> vEvalImpr;

/***** Trigger SaveEqualBest *****/
    void CreateSaveEqualBest();

    void UpdateSaveEqualBest();

    BOOL *aEqualBest;
    FLOAT fPxNoImproveRatio;
    UINT32 iPxNoImprove;


/***** Trigger SaveBestInLastNStep *****/
    void CreateSaveBestInLastNStep();

    void InitSaveBestInLastNStep();

    void UpdateSaveBestInLastNStepPx0();

    void UpdateSaveBestInLastNStepPx1();

    void UpdateCompVars(const std::vector<UINT32> &vComponentToVar);

    // best solution among last pxratio * n steps
    BOOL *aBestInLastNStep; // solution
    UINT32 iBestNumFalseInLastNStep; // evaluation
    UBIGINT iBestStepInLastNStep;   // step when the solution is first reached.
    // Only update on improvement

    UINT32 iPxId;

    FXNPTR fxnPx;
    FXNPTR fxnUpdateSaveBestInLastNStep;

    // wrapper function
    void UpdateSaveBestInLastNStep() { fxnUpdateSaveBestInLastNStep(); }

    // px1: multi bsf
    BOOL bStuck;
    std::vector<boost::dynamic_bitset<>> vvMultiBsf; // vBsf index starts with 1
    BOOL bAllBsf; // weather to apply px to all previous bsf
    BOOL bPxDelay; // delay the application of px after hitting an equal bsf

/***** Trigger PxSuccRate *****/
    UBIGINT iNumPxSucc;
    FLOAT fPxSuccRate;

    void InitPxSuccRate() {
        iNumPxSucc = 0;
        iNumPx = 0;
    }

    void CalPxSuccRate() {
//        printf("px succ %d, number %d\n", iNumPxSucc, iNumPx);
        fPxSuccRate = iNumPxSucc / (FLOAT) iNumPx;
    }

/***** Trigger PxNumComp *****/
    UBIGINT iSumNumComp;
    FLOAT fPxMeanNumComp;

    void InitPxNumComp() {
        iSumNumComp = 0;
    }

    void CalPxMeanNumComp() {
        fPxMeanNumComp = iSumNumComp / (FLOAT) iNumPxSucc;
    }

/***** Trigger VerifySol *****/
    BOOL bNoVerifySol;

    void VerifyVsBest();

    void VerifySol(boost::dynamic_bitset<> &vSol, UINT32 iEval);

/***** Trigger MemLim *****/
    UINT32 iMemLim;

    void SetMemLim();

/***** Trigger CrossRunPx *****/
    BOOL bAcrossRunPx;

    std::vector<boost::dynamic_bitset<>> vvAcrossRunBsf; // vBsf index starts with 1

    void AcrossRunPx();

/***** Trigger LoadBestSolutions *****/
    std::vector<boost::dynamic_bitset<>> vvBestSols;
    std::vector<UINT32> vBestEvals;

    void LoadBestSolutions();

    void PxBestSols();

    void AddDataTriggers() {

        CreateTrigger("ReadCNF", ReadInInstance, ReadCNF, "", "");

        CreateTrigger("LitOccurence", CreateData, CreateLitOccurence, "", "");

        CreateTrigger("CandidateList", CreateData, CreateCandidateList, "", "");

        CreateTrigger("InitVarsFromFile", PreStart, InitVarsFromFile, "DefaultStateInfo", "");
        CreateTrigger("DefaultInitVars", InitData, DefaultInitVars, "DefaultStateInfo", "");

        CreateTrigger("CreateDefaultStateInfo", CreateStateInfo, CreateDefaultStateInfo, "", "");

        CreateTrigger("InitDefaultStateInfo", InitStateInfo, InitDefaultStateInfo, "", "");
        CreateContainerTrigger("DefaultStateInfo", "CreateDefaultStateInfo,InitDefaultStateInfo");

        CreateTrigger("DefaultFlip", FlipCandidate, DefaultFlip, "", "");
        CreateTrigger("DefaultFlipW", FlipCandidate, DefaultFlipW, "", "");

        CreateTrigger("CheckTermination", CheckTerminate, CheckTermination, "", "");

        CreateContainerTrigger("DefaultProcedures",
                               "ReadCNF,LitOccurence,CandidateList,InitVarsFromFile,DefaultStateInfo,DefaultInitVars,DefaultFlip,CheckTermination,VerifyVsBest");
        CreateContainerTrigger("DefaultProceduresW",
                               "ReadCNF,LitOccurence,CandidateList,InitVarsFromFile,DefaultStateInfo,DefaultInitVars,DefaultFlipW,CheckTermination,VerifyVsBest");

        CreateTrigger("CreateFalseClauseList", CreateStateInfo, CreateFalseClauseList, "", "");
        CreateTrigger("InitFalseClauseList", InitStateInfo, InitFalseClauseList, "InitDefaultStateInfo", "");
        CreateTrigger("UpdateFalseClauseList", UpdateStateInfo, UpdateFalseClauseList, "", "");
        CreateContainerTrigger("FalseClauseList", "CreateFalseClauseList,InitFalseClauseList,UpdateFalseClauseList");
        CreateTrigger("Flip+FalseClauseList", FlipCandidate, FlipFalseClauseList, "FalseClauseList",
                      "DefaultFlip,UpdateFalseClauseList");
        CreateTrigger("Flip+FalseClauseListW", FlipCandidate, FlipFalseClauseListW, "FalseClauseList",
                      "DefaultFlipW,UpdateFalseClauseList");

        CreateTrigger("CreateVarScore", CreateStateInfo, CreateVarScore, "", "");
        CreateTrigger("InitVarScore", InitStateInfo, InitVarScore, "InitDefaultStateInfo", "");
        CreateTrigger("UpdateVarScore", UpdateStateInfo, UpdateVarScore, "", "");
        CreateContainerTrigger("VarScore", "CreateVarScore,InitVarScore,UpdateVarScore");
        CreateTrigger("Flip+VarScore", FlipCandidate, FlipVarScore, "VarScore", "DefaultFlip,UpdateVarScore");

        CreateTrigger("CreateVarScoreW", CreateStateInfo, CreateVarScoreW, "", "");
        CreateTrigger("InitVarScoreW", InitStateInfo, InitVarScoreW, "InitDefaultStateInfo", "");
        CreateTrigger("UpdateVarScoreW", UpdateStateInfo, UpdateVarScoreW, "", "");
        CreateContainerTrigger("VarScoreW", "CreateVarScoreW,InitVarScoreW,UpdateVarScoreW");
        CreateTrigger("Flip+VarScoreW", FlipCandidate, FlipVarScoreW, "VarScoreW", "DefaultFlipW,UpdateVarScoreW");

        CreateTrigger("CreateMakeBreak", CreateStateInfo, CreateMakeBreak, "", "");
        CreateTrigger("InitMakeBreak", InitStateInfo, InitMakeBreak, "InitDefaultStateInfo", "");
        CreateTrigger("UpdateMakeBreak", UpdateStateInfo, UpdateMakeBreak, "", "");
        CreateContainerTrigger("MakeBreak", "CreateMakeBreak,InitMakeBreak,UpdateMakeBreak");
        CreateTrigger("Flip+MakeBreak", FlipCandidate, FlipMakeBreak, "MakeBreak", "DefaultFlip,UpdateMakeBreak");

        CreateTrigger("CreateMakeBreakW", CreateStateInfo, CreateMakeBreakW, "", "");
        CreateTrigger("InitMakeBreakW", InitStateInfo, InitMakeBreakW, "InitDefaultStateInfo", "");
        CreateTrigger("UpdateMakeBreakW", UpdateStateInfo, UpdateMakeBreakW, "", "");
        CreateContainerTrigger("MakeBreakW", "CreateMakeBreakW,InitMakeBreakW,UpdateMakeBreakW");
        CreateTrigger("Flip+MakeBreakW", FlipCandidate, FlipMakeBreakW, "MakeBreakW", "DefaultFlipW,UpdateMakeBreakW");

        CreateTrigger("CreateVarInFalse", CreateStateInfo, CreateVarInFalse, "CreateMakeBreak", "");
        CreateTrigger("InitVarInFalse", InitStateInfo, InitVarInFalse, "InitMakeBreak", "");
        CreateTrigger("UpdateVarInFalse", UpdateStateInfo, UpdateVarInFalse, "", "UpdateMakeBreak");
        CreateContainerTrigger("VarInFalse", "CreateVarInFalse,InitVarInFalse,UpdateVarInFalse");
        CreateTrigger("Flip+VarInFalse", FlipCandidate, FlipVarInFalse, "VarInFalse", "DefaultFlip,UpdateVarInFalse");

        CreateTrigger("CreateVarLastChange", CreateStateInfo, CreateVarLastChange, "", "");
        CreateTrigger("InitVarLastChange", InitStateInfo, InitVarLastChange, "", "");
        CreateTrigger("UpdateVarLastChange", UpdateStateInfo, UpdateVarLastChange, "", "");
        CreateContainerTrigger("VarLastChange", "CreateVarLastChange,InitVarLastChange,UpdateVarLastChange");

        CreateTrigger("CreateTrackChanges", CreateStateInfo, CreateTrackChanges, "CreateVarScore", "");
        CreateTrigger("InitTrackChanges", InitStateInfo, InitTrackChanges, "InitVarScore", "");
        CreateTrigger("UpdateTrackChanges", UpdateStateInfo, UpdateTrackChanges, "", "UpdateVarScore");
        CreateContainerTrigger("TrackChanges", "CreateTrackChanges,InitTrackChanges,UpdateTrackChanges");
        CreateTrigger("Flip+TrackChanges", FlipCandidate, FlipTrackChanges, "TrackChanges",
                      "DefaultFlip,UpdateTrackChanges,UpdateVarScore");

        CreateTrigger("Flip+TrackChanges+FCL", FlipCandidate, FlipTrackChangesFCL, "TrackChanges,FalseClauseList",
                      "DefaultFlip,UpdateTrackChanges,UpdateVarScore,UpdateFalseClauseList");

        CreateTrigger("CreateTrackChangesW", CreateStateInfo, CreateTrackChangesW, "CreateVarScoreW", "");
        CreateTrigger("InitTrackChangesW", InitStateInfo, InitTrackChangesW, "InitVarScoreW", "");
        CreateTrigger("UpdateTrackChangesW", UpdateStateInfo, UpdateTrackChangesW, "", "UpdateVarScoreW");
        CreateContainerTrigger("TrackChangesW", "CreateTrackChangesW,InitTrackChangesW,UpdateTrackChangesW");

        CreateTrigger("Flip+TrackChangesW", FlipCandidate, FlipTrackChangesW, "TrackChangesW",
                      "DefaultFlipW,UpdateTrackChangesW,UpdateVarScoreW");
        CreateTrigger("Flip+TrackChanges+FCL+W", FlipCandidate, FlipTrackChangesFCLW, "TrackChangesW,FalseClauseList",
                      "DefaultFlipW,UpdateTrackChangesW,UpdateVarScoreW,UpdateFalseClauseList");

        CreateTrigger("CreateDecPromVars", CreateStateInfo, CreateDecPromVars, "CreateTrackChanges", "");
        CreateTrigger("InitDecPromVars", InitStateInfo, InitDecPromVars, "InitTrackChanges", "");
        CreateTrigger("UpdateDecPromVars", UpdateStateInfo, UpdateDecPromVars, "UpdateTrackChanges", "");
        CreateContainerTrigger("DecPromVars", "CreateDecPromVars,InitDecPromVars,UpdateDecPromVars");

        CreateTrigger("Flip+DecPromVars+FCL", FlipCandidate, FlipDecPromVarsFCL, "DecPromVars,FalseClauseList",
                      "DefaultFlip,UpdateTrackChanges,UpdateVarScore,UpdateFalseClauseList,CreateTrackChanges,InitTrackChanges,UpdateTrackChanges");

        CreateTrigger("CreateDecPromVarsW", CreateStateInfo, CreateDecPromVarsW, "CreateTrackChangesW", "");
        CreateTrigger("InitDecPromVarsW", InitStateInfo, InitDecPromVarsW, "InitTrackChangesW", "");
        CreateTrigger("UpdateDecPromVarsW", UpdateStateInfo, UpdateDecPromVarsW, "UpdateTrackChangesW", "");
        CreateContainerTrigger("DecPromVarsW", "CreateDecPromVarsW,InitDecPromVarsW,UpdateDecPromVarsW");

        CreateTrigger("CreateBestScoreList", CreateStateInfo, CreateBestScoreList, "", "");
        CreateTrigger("InitBestScoreList", InitStateInfo, InitBestScoreList, "InitVarScore", "");
        CreateTrigger("UpdateBestScoreList", UpdateStateInfo, UpdateBestScoreList, "", "");
        CreateContainerTrigger("BestScoreList",
                               "TrackChanges,CreateBestScoreList,InitBestScoreList,UpdateBestScoreList");

        CreateTrigger("CreateClausePenaltyFL", CreateStateInfo, CreateClausePenaltyFL, "", "");
        CreateTrigger("InitClausePenaltyFL", InitStateInfo, InitClausePenaltyFL, "", "");
        CreateContainerTrigger("ClausePenaltyFL", "CreateClausePenaltyFL,InitClausePenaltyFL");

        CreateTrigger("InitClausePenaltyFLW", InitStateInfo, InitClausePenaltyFLW, "", "InitClausePenaltyFL");

        CreateTrigger("CreateMakeBreakPenaltyFL", CreateStateInfo, CreateMakeBreakPenaltyFL, "CreateClausePenaltyFL",
                      "");
        CreateTrigger("InitMakeBreakPenaltyFL", InitStateInfo, InitMakeBreakPenaltyFL, "InitClausePenaltyFL", "");
        CreateTrigger("UpdateMakeBreakPenaltyFL", UpdateStateInfo, UpdateMakeBreakPenaltyFL, "", "");
        CreateContainerTrigger("MakeBreakPenaltyFL",
                               "CreateMakeBreakPenaltyFL,InitMakeBreakPenaltyFL,UpdateMakeBreakPenaltyFL");
        CreateTrigger("Flip+MBPFL+FCL+VIF", FlipCandidate, FlipMBPFLandFCLandVIF,
                      "MakeBreakPenaltyFL,FalseClauseList,VarInFalse",
                      "DefaultFlip,UpdateMakeBreakPenaltyFL,UpdateFalseClauseList,UpdateVarInFalse");
        CreateTrigger("Flip+MBPFL+FCL+VIF+W", FlipCandidate, FlipMBPFLandFCLandVIFandW,
                      "MakeBreakPenaltyFL,FalseClauseList,VarInFalse",
                      "DefaultFlipW,UpdateMakeBreakPenaltyFL,UpdateFalseClauseList,UpdateVarInFalse");

        CreateTrigger("CreateClausePenaltyINT", CreateStateInfo, CreateClausePenaltyINT, "", "");
        CreateTrigger("InitClausePenaltyINT", InitStateInfo, InitClausePenaltyINT, "", "");
        CreateContainerTrigger("ClausePenaltyINT", "CreateClausePenaltyINT,InitClausePenaltyINT");

        CreateTrigger("InitClausePenaltyINTW", InitStateInfo, InitClausePenaltyINTW, "", "InitClausePenaltyINT");

        CreateTrigger("CreateMakeBreakPenaltyINT", CreateStateInfo, CreateMakeBreakPenaltyINT, "CreateClausePenaltyINT",
                      "");
        CreateTrigger("InitMakeBreakPenaltyINT", InitStateInfo, InitMakeBreakPenaltyINT, "InitClausePenaltyINT", "");
        CreateTrigger("UpdateMakeBreakPenaltyINT", UpdateStateInfo, UpdateMakeBreakPenaltyINT, "", "");
        CreateContainerTrigger("MakeBreakPenaltyINT",
                               "CreateMakeBreakPenaltyINT,InitMakeBreakPenaltyINT,UpdateMakeBreakPenaltyINT");
        CreateTrigger("Flip+MBPINT+FCL+VIF", FlipCandidate, FlipMBPINTandFCLandVIF,
                      "MakeBreakPenaltyINT,FalseClauseList,VarInFalse",
                      "DefaultFlip,UpdateMakeBreakPenaltyINT,UpdateFalseClauseList,UpdateVarInFalse");
        CreateTrigger("Flip+MBPINT+FCL+VIF+W", FlipCandidate, FlipMBPINTandFCLandVIFandW,
                      "MakeBreakPenaltyINT,FalseClauseList,VarInFalse",
                      "DefaultFlipW,UpdateMakeBreakPenaltyINT,UpdateFalseClauseList,UpdateVarInFalse");

        CreateTrigger("InitNullFlips", PreRun, InitNullFlips, "", "");
        CreateTrigger("UpdateNullFlips", UpdateStateInfo, UpdateNullFlips, "", "");
        CreateContainerTrigger("NullFlips", "InitNullFlips,UpdateNullFlips");

        CreateTrigger("InitLocalMins", PreRun, InitLocalMins, "", "");
        CreateTrigger("UpdateLocalMins", UpdateStateInfo, UpdateLocalMins, "", "");
        CreateContainerTrigger("LocalMins", "InitLocalMins,UpdateLocalMins");

        CreateTrigger("LogDist", CreateData, CreateLogDist, "", "");

        // save the best evaluation and the first step to reach
        CreateTrigger("InitBestFalse", PreRun, InitBestFalse, "", "");
        CreateTrigger("UpdateBestFalse", PostStep, UpdateBestFalse, "", "");
        CreateContainerTrigger("BestFalse", "InitBestFalse,UpdateBestFalse");


        CreateTrigger("CreateSaveBest", CreateStateInfo, CreateSaveBest, "", "");
        CreateTrigger("UpdateSaveBest", PostStep, UpdateSaveBest, "BestFalse", "");
        CreateContainerTrigger("SaveBest", "CreateSaveBest,UpdateSaveBest");

        CreateTrigger("StartFalse", PostStep, UpdateStartFalse, "", "");

        CreateTrigger("ImproveMean", RunCalculations, CalcImproveMean, "StartFalse,BestFalse", "");

        CreateTrigger("InitFirstLM", PreRun, InitFirstLM, "", "");
        CreateTrigger("UpdateFirstLM", PostStep, UpdateFirstLM, "", "");
        CreateContainerTrigger("FirstLM", "InitFirstLM,UpdateFirstLM");

        CreateTrigger("FirstLMRatio", RunCalculations, CalcFirstLMRatio, "FirstLM,StartFalse,BestFalse", "");

        CreateTrigger("UpdateTrajBestLM", PostStep, UpdateTrajBestLM, "BestFalse", "");
        CreateTrigger("CalcTrajBestLM", RunCalculations, CalcTrajBestLM, "BestFalse", "");
        CreateContainerTrigger("TrajBestLM", "UpdateTrajBestLM,CalcTrajBestLM");

        CreateTrigger("NoImprove", CheckTerminate, CheckNoImprove, "BestFalse", "");

        CreateTrigger("EarlyTerm", CheckTerminate, CheckEarlyTerm, "", "");
        CreateTrigger("Strikes", PostRun, CheckStrikes, "", "");

        CreateTrigger("StartSeed", PreRun, StartSeed, "", "");

        CreateTrigger("SetupCountRandom", PreStart, SetupCountRandom, "", "");
        CreateTrigger("InitCountRandom", PreRun, InitCountRandom, "", "");
        CreateContainerTrigger("CountRandom", "SetupCountRandom,InitCountRandom");

        CreateTrigger("CheckTimeout", CheckTerminate, CheckTimeout, "", "");

        CreateTrigger("CheckForRestarts", CheckRestart, CheckForRestarts, "", "");

        CreateTrigger("CreateFlipCounts", CreateStateInfo, CreateFlipCounts, "", "");
        CreateTrigger("InitFlipCounts", InitStateInfo, InitFlipCounts, "", "");
        CreateTrigger("UpdateFlipCounts", UpdateStateInfo, UpdateFlipCounts, "", "");
        CreateContainerTrigger("FlipCounts", "CreateFlipCounts,InitFlipCounts,UpdateFlipCounts");

        CreateTrigger("FlipCountStats", RunCalculations, FlipCountStats, "FlipCounts", "");

        CreateTrigger("CreateBiasCounts", CreateStateInfo, CreateBiasCounts, "", "");
        CreateTrigger("PreInitBiasCounts", PreInit, PreInitBiasCounts, "", "");
        CreateTrigger("UpdateBiasCounts", PreFlip, UpdateBiasCounts, "", "");
        CreateTrigger("FinalBiasCounts", PostRun, FinalBiasCounts, "", "");
        CreateContainerTrigger("BiasCounts",
                               "CreateBiasCounts,PreInitBiasCounts,UpdateBiasCounts,FinalBiasCounts,VarLastChange");

        CreateTrigger("BiasStats", RunCalculations, BiasStats, "BiasCounts", "");

        CreateTrigger("CreateUnsatCounts", CreateStateInfo, CreateUnsatCounts, "", "");
        CreateTrigger("InitUnsatCounts", InitStateInfo, InitUnsatCounts, "", "");
        CreateTrigger("UpdateUnsatCounts", PostStep, UpdateUnsatCounts, "", "");
        CreateContainerTrigger("UnsatCounts", "CreateUnsatCounts,InitUnsatCounts,UpdateUnsatCounts");

        CreateTrigger("UnsatCountStats", RunCalculations, UnsatCountStats, "UnsatCounts", "");

        CreateTrigger("CreateNumFalseCounts", CreateStateInfo, CreateNumFalseCounts, "", "");
        CreateTrigger("InitNumFalseCounts", InitStateInfo, InitNumFalseCounts, "", "");
        CreateTrigger("UpdateNumFalseCounts", PostStep, UpdateNumFalseCounts, "", "");
        CreateContainerTrigger("NumFalseCounts", "CreateNumFalseCounts,InitNumFalseCounts,UpdateNumFalseCounts");

        CreateTrigger("CreateDistanceCounts", CreateStateInfo, CreateDistanceCounts, "", "");
        CreateTrigger("InitDistanceCounts", InitStateInfo, InitDistanceCounts, "", "");
        CreateTrigger("UpdateDistanceCounts", PostStep, UpdateDistanceCounts, "", "");
        CreateContainerTrigger("DistanceCounts",
                               "CreateDistanceCounts,InitDistanceCounts,UpdateDistanceCounts,SolutionDistance");

        CreateTrigger("UnsatCountStats", RunCalculations, UnsatCountStats, "UnsatCounts", "");
        CreateTrigger("CreateClauseLast", CreateStateInfo, CreateClauseLast, "", "");
        CreateTrigger("InitClauseLast", InitStateInfo, InitClauseLast, "", "");
        CreateTrigger("UpdateClauseLast", PostStep, UpdateClauseLast, "", "");
        CreateContainerTrigger("ClauseLast", "CreateClauseLast,InitClauseLast,UpdateClauseLast");

        CreateTrigger("CreateSQGrid", CreateStateInfo, CreateSQGrid, "LogDist", "");
        CreateTrigger("InitSQGrid", PreRun, InitSQGrid, "", "");
        CreateTrigger("UpdateSQGrid", PostStep, UpdateSQGrid, "BestFalse", "");
        CreateTrigger("FinishSQGrid", PostRun, FinishSQGrid, "", "");
        CreateContainerTrigger("SQGrid", "CreateSQGrid,InitSQGrid,UpdateSQGrid,FinishSQGrid");

        CreateTrigger("CreatePenaltyStats", CreateStateInfo, CreatePenaltyStats, "", "");
        CreateTrigger("InitPenaltyStats", PreRun, InitPenaltyStats, "", "");
        CreateTrigger("UpdatePenaltyStatsStep", StepCalculations, UpdatePenaltyStatsStep, "", "");
        CreateTrigger("UpdatePenaltyStatsRun", RunCalculations, UpdatePenaltyStatsRun, "", "");
        CreateContainerTrigger("PenaltyStats",
                               "CreatePenaltyStats,InitPenaltyStats,UpdatePenaltyStatsStep,UpdatePenaltyStatsRun");

        CreateTrigger("CreateVarFlipHistory", PreStart, CreateVarFlipHistory, "", "");
        CreateTrigger("UpdateVarFlipHistory", PostStep, UpdateVarFlipHistory, "", "");
        CreateContainerTrigger("VarFlipHistory", "CreateVarFlipHistory,UpdateVarFlipHistory");

        CreateTrigger("CreateMobilityWindow", PreStart, CreateMobilityWindow, "CreateVarFlipHistory", "");
        CreateTrigger("InitMobilityWindow", PreRun, InitMobilityWindow, "", "");
        CreateTrigger("UpdateMobilityWindow", PostStep, UpdateMobilityWindow, "UpdateVarFlipHistory", "");
        CreateContainerTrigger("MobilityWindow", "CreateMobilityWindow,InitMobilityWindow,UpdateMobilityWindow");

        CreateTrigger("CreateMobilityFixedFrequencies", PreStart, CreateMobilityFixedFrequencies,
                      "CreateMobilityWindow", "");
        CreateTrigger("InitMobilityFixedFrequencies", PreRun, InitMobilityFixedFrequencies, "InitMobilityWindow", "");
        CreateTrigger("UpdateMobilityFixedFrequencies", PostStep, UpdateMobilityFixedFrequencies,
                      "UpdateMobilityWindow", "");
        CreateContainerTrigger("MobilityFixedFrequencies",
                               "CreateMobilityFixedFrequencies,InitMobilityFixedFrequencies,UpdateMobilityFixedFrequencies");

        CreateTrigger("CreateVarAgeFrequencies", PreStart, CreateVarAgeFrequencies, "", "");
        CreateTrigger("InitVarAgeFrequencies", PreRun, InitVarAgeFrequencies, "", "");
        CreateTrigger("UpdateVarAgeFrequencies", PreFlip, UpdateVarAgeFrequencies, "", "");
        CreateContainerTrigger("VarAgeFrequencies",
                               "CreateVarAgeFrequencies,InitVarAgeFrequencies,UpdateVarAgeFrequencies,VarLastChange");

        CreateTrigger("CreateAutoCorr", PreStart, CreateAutoCorr, "", "");
        CreateTrigger("InitAutoCorr", PreRun, InitAutoCorr, "", "");
        CreateTrigger("UpdateAutoCorr", PostStep, UpdateAutoCorr, "", "");
        CreateTrigger("CalcAutoCorr", RunCalculations, CalcAutoCorr, "", "");
        CreateContainerTrigger("AutoCorr", "CreateAutoCorr,InitAutoCorr,UpdateAutoCorr,CalcAutoCorr");

        CreateTrigger("InitAutoCorrOne", PreRun, InitAutoCorrOne, "", "");
        CreateTrigger("UpdateAutoCorrOne", PostStep, UpdateAutoCorrOne, "", "");
        CreateTrigger("CalcAutoCorrOne", RunCalculations, CalcAutoCorrOne, "", "");
        CreateContainerTrigger("AutoCorrOne", "InitAutoCorrOne,UpdateAutoCorrOne,CalcAutoCorrOne");

        CreateTrigger("BranchFactor", PostStep, BranchFactor, "VarScore", "");
        CreateTrigger("BranchFactorW", PostStep, BranchFactorW, "VarScoreW,CheckWeighted", "");

        CreateTrigger("InitStepsUpDownSide", PreRun, InitStepsUpDownSide, "", "");
        CreateTrigger("UpdateStepsUpDownSide", PostStep, UpdateStepsUpDownSide, "", "");
        CreateContainerTrigger("StepsUpDownSide", "InitStepsUpDownSide,UpdateStepsUpDownSide");

        CreateTrigger("NumRestarts", InitData, NumRestarts, "", "");

        CreateTrigger("LoadKnownSolutions", CreateStateInfo, LoadKnownSolutions, "", "");

        CreateTrigger("CreateSolutionDistance", CreateStateInfo, CreateSolutionDistance, "", "");
        CreateTrigger("UpdateSolutionDistance", PostStep, UpdateSolutionDistance, "", "");
        CreateContainerTrigger("SolutionDistance", "CreateSolutionDistance,UpdateSolutionDistance,LoadKnownSolutions");

        CreateTrigger("InitFDCRun", PreRun, InitFDCRun, "", "");
        CreateTrigger("UpdateFDCRun", UpdateStateInfo, UpdateFDCRun, "UpdateSolutionDistance", "");
        CreateTrigger("CalcFDCRun", RunCalculations, CalcFDCRun, "", "");
        CreateContainerTrigger("FDCRun", "InitFDCRun,UpdateFDCRun,CalcFDCRun,SolutionDistance,LoadKnownSolutions");

        CreateTrigger("CreateFileRandom", PostParameters, CreateFileRandom, "", "");
        CreateTrigger("CloseFileRandom", FinalReports, CloseFileRandom, "", "");
        CreateContainerTrigger("FileRandom", "CreateFileRandom,CloseFileRandom");

        CreateTrigger("FileAbort", PostRun, FileAbort, "", "");

        CreateTrigger("DynamicParms", PostRead, DynamicParms, "", "");

        CreateTrigger("FlushBuffers", PreRun, FlushBuffers, "", "");

        CreateTrigger("CheckWeighted", PostRead, CheckWeighted, "", "");

        CreateTrigger("CreateUniqueSolutions", CreateStateInfo, CreateUniqueSolutions, "", "");
        CreateTrigger("UpdateUniqueSolutions", RunCalculations, UpdateUniqueSolutions, "", "");
        CreateContainerTrigger("UniqueSolutions", "CreateUniqueSolutions,UpdateUniqueSolutions");

        CreateTrigger("VarsShareClauses", CreateData, CreateVarsShareClauses, "LitOccurence", "");

        /** px related triggers **/
        CreateTrigger("InitVIG", CreateStateInfo, InitVIG, "MemLim", "");

        // 'save' indicates storing the solution bit string together with the evaluation
        CreateTrigger("CreateSaveEqualBest", CreateStateInfo, CreateSaveEqualBest, "", "");
        CreateTrigger("UpdateSaveEqualBest", PostStep, UpdateSaveEqualBest, "BestFalse", "");
        CreateContainerTrigger("SaveEqualBest",
                               "CreateSaveEqualBest,UpdateSaveEqualBest");

        CreateTrigger("CreateSaveBestInLastNStep", CreateStateInfo, CreateSaveBestInLastNStep, "", "");
        CreateTrigger("InitSaveBestInLastNStep", PreRun, InitSaveBestInLastNStep, "", "");
        CreateTrigger("UpdateSaveBestInLastNStep", PostStep, UpdateSaveBestInLastNStep, "BestFalse", "");
        CreateContainerTrigger("PxNoImprove",
                               "CreateSaveBestInLastNStep,InitSaveBestInLastNStep,UpdateSaveBestInLastNStep,InitVIG");

        CreateTrigger("InitPxSuccRate", PreRun, InitPxSuccRate, "", "");
        CreateTrigger("CalPxSuccRate", RunCalculations, CalPxSuccRate, "", "");
        CreateContainerTrigger("PxSuccRate", "InitPxSuccRate,CalPxSuccRate");

        CreateTrigger("InitPxNumComp", PreRun, InitPxNumComp, "", "");
        CreateTrigger("CalPxMeanNumComp", RunCalculations, CalPxMeanNumComp, "", "");
        CreateContainerTrigger("PxNumComp", "InitPxNumComp,CalPxMeanNumComp");

        CreateTrigger("VerifyVsBest", PostRun, VerifyVsBest, "SaveBest", "");

        CreateTrigger("MemLim", PreStart, SetMemLim, "", "");

        // require data structures in InitVIG()
        CreateTrigger("AcrossRunPx", RunCalculations, AcrossRunPx, "InitVIG,SaveBest", "");

        CreateTrigger("LoadBestSolutions", CreateStateInfo, LoadBestSolutions, "InitVIG", "");


    }


    char sLine[MAXCNFLINELEN];

    void ReadCNF() {
        UINT32 j;
        UINT32 k;
        UINT32 bIsWCNF;
        SINT32 l;
        UBIGINT w;
        SINT32 iScanRet;
        BOOL bHaveWarnedUnitClause;

        UBIGINT iHardTopWeight;


        LITTYPE *pData;
        LITTYPE *pNextLit;
        LITTYPE *pLastLit;

        FILE *filInput;

        bIsWCNF = 0;

        iNumClauses = 0;

        SetupFile(&filInput, "r", sFilenameIn, stdin, 0);

        while (iNumClauses == 0) {
            if (!fgets(sLine, MAXCNFLINELEN, filInput)) {
                break;
            }
            if (strlen(sLine) == MAXCNFLINELEN - 1) {
                ReportPrint1(pRepErr, "Unexpected Error: increase constant MAXCNFLINELEN [%d]\n", MAXCNFLINELEN);
                AbnormalExit();
                exit(1);
            }

            if (strncmp(sLine, "p wcnf", 6) == 0) {
                bIsWCNF = 1;
            }

            if (sLine[0] == 'p') {
                if (bWeighted) {
                    if (bIsWCNF) {
                        iScanRet = sscanf(sLine, "p wcnf %"SCAN32" %"SCAN32" %"SCAN64, &iNumVars, &iNumClauses,
                                          &iHardTopWeight);
                        if (iScanRet == 2) {
                            iHardTopWeight = 0;
                        }
                    } else {
                        ReportHdrPrefix(pRepErr);
                        ReportHdrPrint(pRepErr, "Warning! reading .cnf file and setting all weights = 1\n");
                        iScanRet = sscanf(sLine, "p cnf %"SCAN32" %"SCAN32, &iNumVars, &iNumClauses);
                        if (iScanRet != 2) {
                            ReportPrint(pRepErr, "Error: invalid instance file\n");
                            AbnormalExit();
                            exit(1);
                        }
                    }
                } else {
                    if (bIsWCNF) {
                        ReportHdrPrefix(pRepErr);
                        ReportHdrPrint(pRepErr, "Warning! reading .wcnf file and ignoring all weights\n");
                        iScanRet = sscanf(sLine, "p wcnf %"SCAN32" %"SCAN32, &iNumVars, &iNumClauses);
                        if (iScanRet != 2) {
                            ReportPrint(pRepErr, "Error: invalid instance file\n");
                            AbnormalExit();
                            exit(1);
                        }
                    } else {
                        iScanRet = sscanf(sLine, "p cnf %"SCAN32" %"SCAN32, &iNumVars, &iNumClauses);
                        if (iScanRet != 2) {
                            ReportPrint(pRepErr, "Error: invalid instance file\n");
                            AbnormalExit();
                            exit(1);
                        }
                        if (iNumClauses == 0) {
                            ReportPrint(pRepErr, "Error: empty instance file\n");
                            AbnormalExit();
                            exit(1);
                        }
                    }
                }
            } else {
                if (sLine[0] == 'c') {

                } else {
                    ReportHdrPrefix(pRepErr);
                    ReportHdrPrint1(pRepErr, "Warning: Ignoring line in input file:\n   %s", sLine);
                }
            }
        }

        if ((iNumVars == 0) || (iNumClauses == 0)) {
            ReportPrint(pRepErr, "Error: invalid instance file\n");
            AbnormalExit();
            exit(1);
        }

        iVARSTATELen = (iNumVars >> 3) + 1;
        if ((iNumVars & 0x07) == 0) {
            iVARSTATELen--;
        }

        aClauseLen = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
        pClauseLits = (LITTYPE **) AllocateRAM(iNumClauses * sizeof(LITTYPE *), HeapData);
        if (bWeighted) {
            aClauseWeight = (UBIGINT *) AllocateRAM(iNumClauses * sizeof(UBIGINT), HeapData);
        }

        pLastLit = pNextLit = pData = 0;

        iNumLits = 0;
        iMaxClauseLen = 0;
        bHaveWarnedUnitClause = 0;

        if ((bWeighted) || (iTarget) || (iTargetWeight)) {
            bHaveWarnedUnitClause = 1;
        }

        for (j = 0; j < iNumClauses; j++) {
            if (bWeighted) {
                if (bIsWCNF) {
                    iScanRet = fscanf(filInput, "%"SCAN64, &w);
                    if (iScanRet != 1) {
                        ReportHdrPrefix(pRepErr);
                        ReportHdrPrint1(pRepErr, "Error reading clause weight at clause [%"P32"]\n", j);
                        ReportHdrPrint1(pRepErr, "  at or near: %s\n", sLine);
                        aClauseWeight[j] = 1;
                    }
                    aClauseWeight[j] = w;
                } else {
                    aClauseWeight[j] = 1;
                }
                iTotalClauseWeight += aClauseWeight[j];
            } else {
                if (bIsWCNF) {
                    fscanf(filInput, "%"SCAN64, &w);
                }
            }

            pClauseLits[j] = pNextLit;
            aClauseLen[j] = 0;

            do {
                iScanRet = fscanf(filInput, "%"SCANS32, &l);

                while (iScanRet != 1) {
                    if (iScanRet == 0) {
                        fgets(sLine, MAXCNFLINELEN, filInput);

                        if (sLine[0] == 'c') {
                            ReportHdrPrefix(pRepErr);
                            ReportHdrPrint1(pRepErr, "Warning: Ingoring comment line mid instance:\n   %s", sLine);
                            iScanRet = fscanf(filInput, "%"SCANS32, &l);
                        } else {
                            ReportPrint1(pRepErr, "Error reading instance at clause [%"P32"]\n", j);
                            ReportPrint1(pRepErr, "  at or near: %s\n", sLine);
                            AbnormalExit();
                            exit(1);
                        }
                    } else {
                        ReportPrint1(pRepErr, "Error reading instance. at clause [%"P32"]\n", j);
                        AbnormalExit();
                        exit(1);
                    }
                }

                if (l) {

                    if (pNextLit >= pLastLit) {
                        pData = (LITTYPE *) AllocateRAM(LITSPERCHUNK * sizeof(LITTYPE), HeapData);
                        pNextLit = pData;
                        pLastLit = pData + LITSPERCHUNK;
                        for (k = 0; k < aClauseLen[j]; k++) {
                            *pNextLit = pClauseLits[j][k];
                            pNextLit++;
                        }
                        pClauseLits[j] = pData;
                    }

                    *pNextLit = SetLitFromFile(l);

                    if (GetVarFromLit(*pNextLit) > iNumVars) {
                        ReportPrint2(pRepErr, "Error: Invalid Literal [%"P32"] in clause [%"P32"]\n", l, j);
                        AbnormalExit();
                        exit(1);
                    }

                    pNextLit++;
                    aClauseLen[j]++;
                    iNumLits++;
                }
            } while (l != 0);

            if (aClauseLen[j] > iMaxClauseLen) {
                iMaxClauseLen = aClauseLen[j];
            }

            if (aClauseLen[j] == 0) {
                ReportPrint1(pRepErr, "Error: Reading .cnf, clause [%"P32"] is empty\n", j);
                AbnormalExit();
                exit(1);
            }
            if ((!bHaveWarnedUnitClause) && (aClauseLen[j] == 1)) {
                ReportHdrPrefix(pRepErr);
                ReportHdrPrint(pRepErr, "Warning! Unit clause detected: consider using a pre-processor\n");
                bHaveWarnedUnitClause = 1;
            }
        }

        AdjustLastRAM((pNextLit - pData) * sizeof(LITTYPE));

        CloseSingleFile(filInput);

    }

    void CreateLitOccurence() {

        UINT32 j, k;
        LITTYPE *pLit;
        UINT32 *pCur;

        aNumLitOcc = (UINT32 *) AllocateRAM((iNumVars + 1) * 2 * sizeof(UINT32), HeapData);
        pLitClause = (UINT32 **) AllocateRAM((iNumVars + 1) * 2 * sizeof(UINT32 *), HeapData);
        aLitOccData = (UINT32 *) AllocateRAM(iNumLits * sizeof(UINT32), HeapData);

        memset(aNumLitOcc, 0, (iNumVars + 1) * 2 * sizeof(UINT32));

        for (j = 0; j < iNumClauses; j++) {
            pLit = pClauseLits[j];
            for (k = 0; k < aClauseLen[j]; k++) {
                aNumLitOcc[*pLit]++;
                pLit++;
            }
        }

        pCur = aLitOccData;
        for (j = 0; j < (iNumVars + 1) * 2; j++) {
            pLitClause[j] = pCur;
            pCur += aNumLitOcc[j];
        }

        memset(aNumLitOcc, 0, (iNumVars + 1) * 2 * sizeof(UINT32));

        for (j = 0; j < iNumClauses; j++) {
            pLit = pClauseLits[j];
            for (k = 0; k < aClauseLen[j]; k++) {
                pCur = pLitClause[*pLit] + aNumLitOcc[*pLit];
                *pCur = j;
                aNumLitOcc[*pLit]++;
                pLit++;
            }
        }
    }

    void CreateCandidateList() {
        aCandidateList = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
        iMaxCandidates = iNumClauses;
    }

    char sInitLine[MAXPARMLINELEN];

    void InitVarsFromFile() {
        SINT32 iLit;
        UINT32 iVar;
        UINT32 iValue;
        UINT32 j;
        FILE *filInit;
        char *pStart;
        char *pPos;

        for (j = 1; j <= iNumVars; j++) {
            aVarInit[j] = 2;
        }

        if (strcmp(sFilenameVarInit, "")) {
            SetupFile(&filInit, "r", sFilenameVarInit, stdin, 0);

            while (!feof(filInit)) {
                if (fgets(sInitLine, MAXPARMLINELEN, filInit)) {
                    if (strlen(sInitLine) == MAXPARMLINELEN - 1) {
                        ReportPrint1(pRepErr, "Unexpected Error: increase constant MAXPARMLINELEN [%d]\n",
                                     MAXPARMLINELEN);
                        AbnormalExit();
                        exit(1);
                    }
                    if ((*sInitLine) && (*sInitLine != '#')) {
                        pStart = sInitLine;
                        pPos = strchr(pStart, ' ');
                        while (pPos) {
                            if (pPos == pStart) {
                                pStart++;
                            } else {
                                *pPos++ = 0;

                                sscanf(pStart, "%"SCANS32, &iLit);

                                if (iLit) {
                                    if (iLit > 0) {
                                        iValue = 1;
                                        iVar = (UINT32) iLit;
                                    } else {
                                        iValue = 0;
                                        iVar = (UINT32) (-iLit);
                                    }
                                    aVarInit[iVar] = iValue;
                                }

                                pStart = pPos;
                            }
                            pPos = strchr(pStart, ' ');
                        }
                        pPos = strchr(pStart, 10);

                        if (pPos) {
                            *pPos = 0;
                        }
                        pPos = strchr(pStart, 13);
                        if (pPos) {
                            *pPos = 0;
                        }

                        if (strlen(pStart)) {
                            sscanf(pStart, "%"SCANS32, &iLit);

                            if (iLit) {
                                if (iLit > 0) {
                                    iValue = 1;
                                    iVar = (UINT32) iLit;
                                } else {
                                    iValue = 0;
                                    iVar = (UINT32) (-iLit);
                                }
                                aVarInit[iVar] = iValue;
                            }
                        }
                    }
                }
            }
        }
    }


    void CreateDefaultStateInfo() {
        aNumTrueLit = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
        aVarValue = (BOOL *) AllocateRAM((iNumVars + 1) * sizeof(BOOL), HeapData);
        aVarInit = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
    }

    void InitDefaultStateInfo() {
        UINT32 j, k;
        LITTYPE litCur;
        UINT32 *pClause;

        memset(aNumTrueLit, 0, iNumClauses * sizeof(UINT32));
        iNumFalse = 0;

        for (j = 1; j <= iNumVars; j++) {
            litCur = GetTrueLit(j);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; k++) {
                aNumTrueLit[*pClause]++;
                pClause++;
            }
        }

        for (j = 0; j < iNumClauses; j++) {
            if (aNumTrueLit[j] == 0) {
                iNumFalse++;
            }
        }

        if (bWeighted) {
            iSumFalseWeight = 0;
            for (j = 0; j < iNumClauses; j++) {
                if (aNumTrueLit[j] == 0) {
                    iSumFalseWeight += aClauseWeight[j];
                }
            }
        }
    }

    void DefaultInitVars() {
        UINT32 k;
        UINT32 j;
        UINT32 iVar = 0;
        BOOL bAdded;
        UINT32 iNumPos;
        UINT32 iNumNeg;
        BOOL iNextAlternating = 0;

        if (bVarInitGreedy) {
            for (j = 1; j <= iNumVars; j++) {
                iNumPos = aNumLitOcc[GetPosLit(j)];
                iNumNeg = aNumLitOcc[GetNegLit(j)];

                if (iNumPos > iNumNeg) {
                    aVarInit[j] = 1;
                } else {
                    if (iNumNeg > iNumPos) {
                        aVarInit[j] = 0;
                    } else {
                        /* otherwise, alternate between 0 and 1 */
                        aVarInit[j] = 3;
                    }
                }
            }
        }

        for (j = 1; j <= iNumVars; j++) {
            if (aVarInit[j] >= 3) {
                aVarValue[j] = iNextAlternating;
                iNextAlternating = !iNextAlternating;
            } else {
                if (aVarInit[j] == 2) {
                    if (RandomInt(2)) {
                        aVarValue[j] = 1;
                    } else {
                        aVarValue[j] = 0;
                    }
                } else {
                    if (aVarInit[j]) {
                        aVarValue[j] = 1;
                    } else {
                        aVarValue[j] = 0;
                    }
                }
            }
        }

        if (iInitVarFlip) {

            if (iInitVarFlip > iNumVars) {
                ReportHdrPrefix(pRepErr);
                ReportHdrPrint(pRepErr, "Warning! -varinitflip value is greater than the number of variables\n");
                iInitVarFlip = iNumVars;
            }

            for (j = 0; j < iInitVarFlip; j++) {
                do {
                    bAdded = 1;
                    iVar = RandomInt(iNumVars) + 1;
                    if (j > 0) {
                        for (k = 0; k < j; k++) {
                            if (aCandidateList[k] == iVar) {
                                bAdded = 0;
                                break;
                            }
                        }
                    }
                } while (bAdded == 0);
                aCandidateList[j] = iVar;
                aVarValue[iVar] = !aVarValue[iVar];
            }
        }

    }

    void DefaultFlip() {
        UINT32 j;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        UINT32 *pClause;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        // break
        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            if (--aNumTrueLit[*pClause] == 0) { // the only true lit in the clause
                iNumFalse++;
            }
            pClause++;
        }

        // make
        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            if (++aNumTrueLit[*pClause] == 1) { // previously zero true lit in the clause
                iNumFalse--;
            }
            pClause++;
        }
    }

    void DefaultFlipW() {

        UINT32 j;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        UINT32 *pClause;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            if (--aNumTrueLit[*pClause] == 0) {
                iNumFalse++;
                iSumFalseWeight += aClauseWeight[*pClause];
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            if (++aNumTrueLit[*pClause] == 1) {
                iNumFalse--;
                iSumFalseWeight -= aClauseWeight[*pClause];
            }
            pClause++;
        }
    }

    void CheckTermination() {
        if (iNumFalse <= iTarget) {
            bSolutionFound = 1;
        }
        if (bWeighted) {
            if (iSumFalseWeight <= iTargetWeight) {
                bSolutionFound = 1;
            }
        }
    }

    void CreateFalseClauseList() {
        aFalseList = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
        aFalseListPos = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
    }

    void InitFalseClauseList() {
        UINT32 j;

        iNumFalseList = 0;

        for (j = 0; j < iNumClauses; j++) {
            if (aNumTrueLit[j] == 0) {
                aFalseList[iNumFalseList] = j;
                aFalseListPos[j] = iNumFalseList++;
            }
        }
    }

    void UpdateFalseClauseList() {
        UINT32 j;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        UINT32 *pClause;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetFalseLit(iFlipCandidate);
        litWasFalse = GetTrueLit(iFlipCandidate);

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            if (aNumTrueLit[*pClause] == 0) {
                aFalseList[iNumFalseList] = *pClause;
                aFalseListPos[*pClause] = iNumFalseList++;
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            if (aNumTrueLit[*pClause] == 1) {
                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalseList];
                aFalseListPos[aFalseList[iNumFalseList]] = aFalseListPos[*pClause];
            }
            pClause++;
        }
    }

    void FlipFalseClauseList() {
        UINT32 j;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        UINT32 *pClause;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {
                aFalseList[iNumFalse] = *pClause;
                aFalseListPos[*pClause] = iNumFalse++;
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {
                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalse];
                aFalseListPos[aFalseList[iNumFalse]] = aFalseListPos[*pClause];
            }
            pClause++;
        }
    }

    void FlipFalseClauseListW() {

        UINT32 j;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        UINT32 *pClause;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {
                aFalseList[iNumFalse] = *pClause;
                aFalseListPos[*pClause] = iNumFalse++;
                iSumFalseWeight += aClauseWeight[*pClause];
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {
                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalse];
                aFalseListPos[aFalseList[iNumFalse]] = aFalseListPos[*pClause];
                iSumFalseWeight -= aClauseWeight[*pClause];
            }
            pClause++;
        }
    }

    void CreateVarScore() {
        aVarScore = (SINT32 *) AllocateRAM((iNumVars + 1) * sizeof(SINT32), HeapData);
        aCritSat = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
    }

    void InitVarScore() {
        UINT32 j;
        UINT32 k;
        UINT32 iVar;
        LITTYPE *pLit;

        memset(aVarScore, 0, (iNumVars + 1) * sizeof(int));
        memset(aCritSat, 0, iNumClauses * sizeof(UINT32));

        for (j = 0; j < iNumClauses; j++) {
            if (aNumTrueLit[j] == 0) {
                for (k = 0; k < aClauseLen[j]; k++) {
                    aVarScore[GetVar(j, k)]--;
                }
            } else if (aNumTrueLit[j] == 1) {
                pLit = pClauseLits[j];
                for (k = 0; k < aClauseLen[j]; k++) {
                    if IsLitTrue(*pLit) {
                        iVar = GetVarFromLit(*pLit);
                        aVarScore[iVar]++;
                        aCritSat[j] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
        }
    }

    void UpdateVarScore() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetFalseLit(iFlipCandidate);
        litWasFalse = GetTrueLit(iFlipCandidate);

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            if (aNumTrueLit[*pClause] == 0) {

                aVarScore[iFlipCandidate]--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    aVarScore[GetVarFromLit(*pLit)]--;
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aVarScore[iVar]++;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aVarScore[iVar]++;
                    pLit++;
                }
                aVarScore[iFlipCandidate]++;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aVarScore[aCritSat[*pClause]]--;
            }
            pClause++;
        }
    }

    void FlipVarScore() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                iNumFalse++;

                aVarScore[iFlipCandidate]--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    aVarScore[GetVarFromLit(*pLit)]--;
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aVarScore[iVar]++;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                iNumFalse--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aVarScore[iVar]++;
                    pLit++;
                }
                aVarScore[iFlipCandidate]++;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aVarScore[aCritSat[*pClause]]--;
            }
            pClause++;
        }
    }


    void CreateVarScoreW() {
        aVarScoreWeight = (SBIGINT *) AllocateRAM((iNumVars + 1) * sizeof(SBIGINT), HeapData);
        aCritSat = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
    }

    void InitVarScoreW() {
        UINT32 j;
        UINT32 k;
        UINT32 iVar;
        LITTYPE *pLit;

        memset(aVarScoreWeight, 0, (iNumVars + 1) * sizeof(SBIGINT));
        memset(aCritSat, 0, iNumClauses * sizeof(UINT32));

        for (j = 0; j < iNumClauses; j++) {
            if (aNumTrueLit[j] == 0) {
                for (k = 0; k < aClauseLen[j]; k++) {
                    aVarScoreWeight[GetVar(j, k)] -= aClauseWeight[j];
                }
            } else if (aNumTrueLit[j] == 1) {
                pLit = pClauseLits[j];
                for (k = 0; k < aClauseLen[j]; k++) {
                    if IsLitTrue(*pLit) {
                        iVar = GetVarFromLit(*pLit);
                        aVarScoreWeight[iVar] += aClauseWeight[j];
                        aCritSat[j] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
        }
    }

    void UpdateVarScoreW() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetFalseLit(iFlipCandidate);
        litWasFalse = GetTrueLit(iFlipCandidate);

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            if (aNumTrueLit[*pClause] == 0) {

                aVarScoreWeight[iFlipCandidate] -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    aVarScoreWeight[GetVarFromLit(*pLit)] -= aClauseWeight[*pClause];
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aVarScoreWeight[iVar] += aClauseWeight[*pClause];
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aVarScoreWeight[iVar] += aClauseWeight[*pClause];
                    pLit++;
                }
                aVarScoreWeight[iFlipCandidate] += aClauseWeight[*pClause];
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aVarScoreWeight[aCritSat[*pClause]] -= aClauseWeight[*pClause];
            }
            pClause++;
        }
    }

    void FlipVarScoreW() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                iNumFalse++;
                iSumFalseWeight += aClauseWeight[*pClause];

                aVarScoreWeight[iFlipCandidate] -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    aVarScoreWeight[GetVarFromLit(*pLit)] -= aClauseWeight[*pClause];
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aVarScoreWeight[iVar] += aClauseWeight[*pClause];
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                iNumFalse--;
                iSumFalseWeight -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aVarScoreWeight[iVar] += aClauseWeight[*pClause];
                    pLit++;
                }
                aVarScoreWeight[iFlipCandidate] += aClauseWeight[*pClause];
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aVarScoreWeight[aCritSat[*pClause]] -= aClauseWeight[*pClause];
            }
            pClause++;
        }
    }


    void FlipVarScoreFalseClauseList() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                aFalseList[iNumFalse] = *pClause;
                aFalseListPos[*pClause] = iNumFalse++;

                aVarScore[iFlipCandidate]--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    aVarScore[GetVarFromLit(*pLit)]--;
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aVarScore[iVar]++;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalse];
                aFalseListPos[aFalseList[iNumFalse]] = aFalseListPos[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aVarScore[iVar]++;
                    pLit++;
                }
                aVarScore[iFlipCandidate]++;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aVarScore[aCritSat[*pClause]]--;
            }
            pClause++;
        }
    }

    void CreateMakeBreak() {
        aBreakCount = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
        aMakeCount = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
        aCritSat = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
    }

    void InitMakeBreak() {
        UINT32 j;
        UINT32 k;
        UINT32 iVar;
        LITTYPE *pLit;

        memset(aMakeCount, 0, (iNumVars + 1) * sizeof(UINT32));
        memset(aBreakCount, 0, (iNumVars + 1) * sizeof(UINT32));
        memset(aCritSat, 0, iNumClauses * sizeof(UINT32));

        for (j = 0; j < iNumClauses; j++) {
            if (aNumTrueLit[j] == 0) {
                for (k = 0; k < aClauseLen[j]; k++) {
                    aMakeCount[GetVar(j, k)]++;
                }
            } else if (aNumTrueLit[j] == 1) {
                pLit = pClauseLits[j];
                for (k = 0; k < aClauseLen[j]; k++) {
                    if IsLitTrue(*pLit) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakCount[iVar]++;
                        aCritSat[j] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
        }
    }


    void UpdateMakeBreak() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetFalseLit(iFlipCandidate);
        litWasFalse = GetTrueLit(iFlipCandidate);

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            if (aNumTrueLit[*pClause] == 0) {
                aBreakCount[iFlipCandidate]--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    aMakeCount[GetVarFromLit(*pLit)]++;
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakCount[iVar]++;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakeCount[iVar]--;
                    pLit++;
                }
                aBreakCount[iFlipCandidate]++;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aBreakCount[aCritSat[*pClause]]--;
            }
            pClause++;
        }
    }


    void FlipMakeBreak() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                aFalseList[iNumFalse] = *pClause;
                aFalseListPos[*pClause] = iNumFalse++;

                aBreakCount[iFlipCandidate]--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    aMakeCount[GetVarFromLit(*pLit)]++;
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakCount[iVar]++;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalse];
                aFalseListPos[aFalseList[iNumFalse]] = aFalseListPos[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakeCount[iVar]--;
                    pLit++;
                }
                aBreakCount[iFlipCandidate]++;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aBreakCount[aCritSat[*pClause]]--;
            }
            pClause++;
        }
    }

    void CreateMakeBreakW() {
        aBreakCountWeight = (UBIGINT *) AllocateRAM((iNumVars + 1) * sizeof(UBIGINT), HeapData);
        amakeCountWeight = (UBIGINT *) AllocateRAM((iNumVars + 1) * sizeof(UBIGINT), HeapData);
        aCritSat = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
    }

    void InitMakeBreakW() {
        UINT32 j;
        UINT32 k;
        UINT32 iVar;
        LITTYPE *pLit;

        memset(amakeCountWeight, 0, (iNumVars + 1) * sizeof(UBIGINT));
        memset(aBreakCountWeight, 0, (iNumVars + 1) * sizeof(UBIGINT));
        memset(aCritSat, 0, iNumClauses * sizeof(UINT32));

        for (j = 0; j < iNumClauses; j++) {
            if (aNumTrueLit[j] == 0) {
                for (k = 0; k < aClauseLen[j]; k++) {
                    amakeCountWeight[GetVar(j, k)] += aClauseWeight[j];
                }
            } else if (aNumTrueLit[j] == 1) {
                pLit = pClauseLits[j];
                for (k = 0; k < aClauseLen[j]; k++) {
                    if IsLitTrue(*pLit) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakCountWeight[iVar] += aClauseWeight[j];
                        aCritSat[j] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
        }
    }


    void UpdateMakeBreakW() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetFalseLit(iFlipCandidate);
        litWasFalse = GetTrueLit(iFlipCandidate);

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            if (aNumTrueLit[*pClause] == 0) {
                aBreakCountWeight[iFlipCandidate] -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    amakeCountWeight[GetVarFromLit(*pLit)] += aClauseWeight[*pClause];
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakCountWeight[iVar] += aClauseWeight[*pClause];
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    amakeCountWeight[iVar] -= aClauseWeight[*pClause];
                    pLit++;
                }
                aBreakCountWeight[iFlipCandidate] += aClauseWeight[*pClause];
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aBreakCountWeight[aCritSat[*pClause]] -= aClauseWeight[*pClause];
            }
            pClause++;
        }
    }


    void FlipMakeBreakW() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                aFalseList[iNumFalse] = *pClause;
                aFalseListPos[*pClause] = iNumFalse++;

                iSumFalseWeight += aClauseWeight[*pClause];

                aBreakCountWeight[iFlipCandidate] -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    amakeCountWeight[GetVarFromLit(*pLit)] += aClauseWeight[*pClause];
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakCountWeight[iVar] += aClauseWeight[*pClause];
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalse];
                aFalseListPos[aFalseList[iNumFalse]] = aFalseListPos[*pClause];

                iSumFalseWeight -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    amakeCountWeight[iVar] -= aClauseWeight[*pClause];
                    pLit++;
                }
                aBreakCountWeight[iFlipCandidate] += aClauseWeight[*pClause];
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aBreakCountWeight[aCritSat[*pClause]] -= aClauseWeight[*pClause];
            }
            pClause++;
        }
    }

    void CreateVarInFalse() {
        aVarInFalseList = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
        aVarInFalseListPos = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
    }

    void InitVarInFalse() {
        UINT32 j;

        iNumVarsInFalseList = 0;

        for (j = 1; j <= iNumVars; j++) {
            if (aMakeCount[j]) {
                aVarInFalseList[iNumVarsInFalseList] = j;
                aVarInFalseListPos[j] = iNumVarsInFalseList++;
            }
        }
    }

    void UpdateVarInFalse() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetFalseLit(iFlipCandidate);
        litWasFalse = GetTrueLit(iFlipCandidate);

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            if (aNumTrueLit[*pClause] == 0) {

                aBreakCount[iFlipCandidate]--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {

                    iVar = GetVarFromLit(*pLit);

                    aMakeCount[iVar]++;

                    if (aMakeCount[iVar] == 1) {
                        aVarInFalseList[iNumVarsInFalseList] = iVar;
                        aVarInFalseListPos[iVar] = iNumVarsInFalseList++;
                    }

                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakCount[iVar]++;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            if (aNumTrueLit[*pClause] == 1) {

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakeCount[iVar]--;

                    if (aMakeCount[iVar] == 0) {
                        aVarInFalseList[aVarInFalseListPos[iVar]] = aVarInFalseList[--iNumVarsInFalseList];
                        aVarInFalseListPos[aVarInFalseList[iNumVarsInFalseList]] = aVarInFalseListPos[iVar];
                    }

                    pLit++;
                }
                aBreakCount[iFlipCandidate]++;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aBreakCount[aCritSat[*pClause]]--;
            }
            pClause++;
        }
    }

    void FlipVarInFalse() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                iNumFalse++;

                aBreakCount[iFlipCandidate]--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {

                    iVar = GetVarFromLit(*pLit);

                    aMakeCount[iVar]++;

                    if (aMakeCount[iVar] == 1) {
                        aVarInFalseList[iNumVarsInFalseList] = iVar;
                        aVarInFalseListPos[iVar] = iNumVarsInFalseList++;
                    }

                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakCount[iVar]++;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                iNumFalse--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakeCount[iVar]--;

                    if (aMakeCount[iVar] == 0) {
                        aVarInFalseList[aVarInFalseListPos[iVar]] = aVarInFalseList[--iNumVarsInFalseList];
                        aVarInFalseListPos[aVarInFalseList[iNumVarsInFalseList]] = aVarInFalseListPos[iVar];
                    }

                    pLit++;
                }
                aBreakCount[iFlipCandidate]++;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aBreakCount[aCritSat[*pClause]]--;
            }
            pClause++;
        }
    }

    void CreateVarLastChange() {
        aVarLastChange = (UBIGINT *) AllocateRAM((iNumVars + 1) * sizeof(UBIGINT), HeapData);
    }

    void InitVarLastChange() {
        UINT32 j;

        for (j = 0; j <= iNumVars; j++) {
            aVarLastChange[j] = iStep - 1;
        }
        iVarLastChangeReset = iStep;
    }

    void UpdateVarLastChange() {
        aVarLastChange[iFlipCandidate] = iStep;
    }


    void CreateTrackChanges() {
        aChangeList = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
        aChangeLastStep = (UBIGINT *) AllocateRAM((iNumVars + 1) * sizeof(UBIGINT), HeapData);
        aChangeOldScore = (SINT32 *) AllocateRAM((iNumVars + 1) * sizeof(SINT32), HeapData);
    }

    void InitTrackChanges() {
        memset(aChangeLastStep, 0, (iNumVars + 1) * sizeof(UBIGINT));
    }

    void UpdateTrackChanges() {
        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        iNumChanges = 0;

        litWasTrue = GetFalseLit(iFlipCandidate);
        litWasFalse = GetTrueLit(iFlipCandidate);

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            if (aNumTrueLit[*pClause] == 0) {

                UpdateChange(iFlipCandidate);
                aVarScore[iFlipCandidate]--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    UpdateChange(iVar);
                    aVarScore[iVar]--;
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        UpdateChange(iVar);
                        aVarScore[iVar]++;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            if (aNumTrueLit[*pClause] == 1) {

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    UpdateChange(iVar);
                    aVarScore[iVar]++;
                    pLit++;
                }
                UpdateChange(iFlipCandidate);
                aVarScore[iFlipCandidate]++;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                iVar = aCritSat[*pClause];
                UpdateChange(iVar);
                aVarScore[iVar]--;
            }
            pClause++;
        }
    }

    void FlipTrackChanges() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        iNumChanges = 0;

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                iNumFalse++;

                UpdateChange(iFlipCandidate);
                aVarScore[iFlipCandidate]--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    UpdateChange(iVar);
                    aVarScore[iVar]--;
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        UpdateChange(iVar);
                        aVarScore[iVar]++;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                iNumFalse--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    UpdateChange(iVar);
                    aVarScore[iVar]++;
                    pLit++;
                }
                UpdateChange(iFlipCandidate);
                aVarScore[iFlipCandidate]++;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                iVar = aCritSat[*pClause];
                UpdateChange(iVar);
                aVarScore[iVar]--;
            }
            pClause++;
        }
    }

    void FlipTrackChangesFCL() {
        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        iNumChanges = 0;

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                aFalseList[iNumFalse] = *pClause;
                aFalseListPos[*pClause] = iNumFalse++;

                UpdateChange(iFlipCandidate);
                aVarScore[iFlipCandidate]--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    UpdateChange(iVar);
                    aVarScore[iVar]--;
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        UpdateChange(iVar);
                        aVarScore[iVar]++;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalse];
                aFalseListPos[aFalseList[iNumFalse]] = aFalseListPos[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    UpdateChange(iVar);
                    aVarScore[iVar]++;
                    pLit++;
                }
                UpdateChange(iFlipCandidate);
                aVarScore[iFlipCandidate]++;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                iVar = aCritSat[*pClause];
                UpdateChange(iVar);
                aVarScore[iVar]--;
            }
            pClause++;
        }
    }

    void FlipDecPromVarsFCL() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        iNumChanges = 0;

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            if (aNumTrueLit[*pClause] == 1) {

                aFalseList[iNumFalse] = *pClause;
                aFalseListPos[*pClause] = iNumFalse++;

                aVarScore[iFlipCandidate]--;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aVarScore[iVar]--;
                    if (aVarScore[iVar] == -1) {
                        aDecPromVarsList[iNumDecPromVars++] = iVar;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            if (aNumTrueLit[*pClause] == 1) {
                iVar = aCritSat[*pClause];
                aVarScore[iVar]--;
                if (aVarScore[iVar] == -1) {
                    aDecPromVarsList[iNumDecPromVars++] = iVar;
                }
            }
            pClause++;
        }

        // 2nd pass

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aVarScore[iVar]++;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalse];
                aFalseListPos[aFalseList[iNumFalse]] = aFalseListPos[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aVarScore[iVar]++;
                    pLit++;
                }
                aVarScore[iFlipCandidate]++;
                aCritSat[*pClause] = iFlipCandidate;
            }
            pClause++;
        }

        j = 0;
        k = 0;
        while (j < iNumDecPromVars) {
            iVar = aDecPromVarsList[k];
            if ((aVarScore[iVar] >= 0) || (iVar == iFlipCandidate)) {
                iNumDecPromVars--;
            } else {
                aDecPromVarsList[j++] = aDecPromVarsList[k];
            }
            k++;
        }
    }


#define UpdateChangeW(var) {if(aChangeLastStepWeight[var]!=iStep) {aChangeOldScoreWeight[var] = aVarScoreWeight[var]; aChangeLastStepWeight[var]=iStep; aChangeListW[iNumChangesW++]=var;}}

    void CreateTrackChangesW() {
        aChangeListW = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
        aChangeLastStepWeight = (UBIGINT *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
        aChangeOldScoreWeight = (SBIGINT *) AllocateRAM((iNumVars + 1) * sizeof(SBIGINT), HeapData);
    }

    void InitTrackChangesW() {
        memset(aChangeLastStepWeight, 0, (iNumVars + 1) * sizeof(UBIGINT));
    }

    void UpdateTrackChangesW() {
        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        iNumChangesW = 0;

        litWasTrue = GetFalseLit(iFlipCandidate);
        litWasFalse = GetTrueLit(iFlipCandidate);

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            if (aNumTrueLit[*pClause] == 0) {

                UpdateChangeW(iFlipCandidate);
                aVarScoreWeight[iFlipCandidate] -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    UpdateChangeW(iVar);
                    aVarScoreWeight[iVar] -= aClauseWeight[*pClause];
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        UpdateChangeW(iVar);
                        aVarScoreWeight[iVar] += aClauseWeight[*pClause];
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            if (aNumTrueLit[*pClause] == 1) {

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    UpdateChangeW(iVar);
                    aVarScoreWeight[iVar] += aClauseWeight[*pClause];
                    pLit++;
                }
                UpdateChangeW(iFlipCandidate);
                aVarScoreWeight[iFlipCandidate] += aClauseWeight[*pClause];
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                iVar = aCritSat[*pClause];
                UpdateChangeW(iVar);
                aVarScoreWeight[iVar] -= aClauseWeight[*pClause];
            }
            pClause++;
        }
    }

    void FlipTrackChangesW() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        iNumChangesW = 0;

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                iNumFalse++;
                iSumFalseWeight += aClauseWeight[*pClause];

                UpdateChangeW(iFlipCandidate);
                aVarScoreWeight[iFlipCandidate] -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    UpdateChangeW(iVar);
                    aVarScoreWeight[iVar] -= aClauseWeight[*pClause];
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        UpdateChangeW(iVar);
                        aVarScoreWeight[iVar] += aClauseWeight[*pClause];
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                iNumFalse--;
                iSumFalseWeight -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    UpdateChangeW(iVar);
                    aVarScoreWeight[iVar] += aClauseWeight[*pClause];
                    pLit++;
                }
                UpdateChangeW(iFlipCandidate);
                aVarScoreWeight[iFlipCandidate] += aClauseWeight[*pClause];
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                iVar = aCritSat[*pClause];
                UpdateChangeW(iVar);
                aVarScoreWeight[iVar] -= aClauseWeight[*pClause];
            }
            pClause++;
        }
    }

    void FlipTrackChangesFCLW() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        if (iFlipCandidate == 0) {
            return;
        }

        iNumChangesW = 0;

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                aFalseList[iNumFalse] = *pClause;
                aFalseListPos[*pClause] = iNumFalse++;
                iSumFalseWeight += aClauseWeight[*pClause];

                UpdateChangeW(iFlipCandidate);
                aVarScoreWeight[iFlipCandidate] -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    UpdateChangeW(iVar);
                    aVarScoreWeight[iVar] -= aClauseWeight[*pClause];
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        UpdateChangeW(iVar);
                        aVarScoreWeight[iVar] += aClauseWeight[*pClause];
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalse];
                aFalseListPos[aFalseList[iNumFalse]] = aFalseListPos[*pClause];
                iSumFalseWeight -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    UpdateChangeW(iVar);
                    aVarScoreWeight[iVar] += aClauseWeight[*pClause];
                    pLit++;
                }
                UpdateChangeW(iFlipCandidate);
                aVarScoreWeight[iFlipCandidate] += aClauseWeight[*pClause];
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                iVar = aCritSat[*pClause];
                UpdateChangeW(iVar);
                aVarScoreWeight[iVar] -= aClauseWeight[*pClause];
            }
            pClause++;
        }
    }


    void CreateDecPromVars() {

        aDecPromVarsList = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
    }

    void InitDecPromVars() {

        UINT32 j;

        iNumDecPromVars = 0;

        for (j = 1; j <= iNumVars; j++) {
            if (aVarScore[j] < 0) {
                aDecPromVarsList[iNumDecPromVars++] = j;
            }
        }
    }

    void UpdateDecPromVars() {

        UINT32 j, k;
        UINT32 iVar;

        for (j = 0; j < iNumChanges; j++) {
            iVar = aChangeList[j];
            if ((aVarScore[iVar] < 0) && (aChangeOldScore[iVar] >= 0)) {
                aDecPromVarsList[iNumDecPromVars++] = iVar;
            }
        }
        j = 0;
        k = 0;
        while (j < iNumDecPromVars) {
            iVar = aDecPromVarsList[k];
            if ((aVarScore[iVar] >= 0) || (iVar == iFlipCandidate)) {
                iNumDecPromVars--;
            } else {
                aDecPromVarsList[j++] = aDecPromVarsList[k];
            }
            k++;
        }
    }

    void CreateDecPromVarsW() {

        aDecPromVarsListW = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);

    }

    void InitDecPromVarsW() {

        UINT32 j;

        iNumDecPromVarsW = 0;

        for (j = 1; j <= iNumVars; j++) {
            if (aVarScoreWeight[j] < 0) {
                aDecPromVarsListW[iNumDecPromVarsW++] = j;
            }
        }
    }

    void UpdateDecPromVarsW() {

        UINT32 j, k;
        UINT32 iVar;

        for (j = 0; j < iNumChangesW; j++) {
            iVar = aChangeListW[j];
            if ((aVarScoreWeight[iVar] < 0) && (aChangeOldScoreWeight[iVar] >= 0)) {
                aDecPromVarsListW[iNumDecPromVarsW++] = iVar;
            }
        }
        j = 0;
        k = 0;
        while (j < iNumDecPromVarsW) {
            iVar = aDecPromVarsListW[k];
            if ((aVarScoreWeight[iVar] >= 0) || (iVar == iFlipCandidate)) {
                iNumDecPromVarsW--;
            } else {
                aDecPromVarsListW[j++] = aDecPromVarsListW[k];
            }
            k++;
        }
    }


    void CreateBestScoreList() {
        aBestScoreList = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
        aBestScoreListPos = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
    }


    void InitBestScoreList() {
        UINT32 j;

        iBestScore = (SINT32) iNumClauses;
        iNumBestScoreList = 0;

        for (j = 1; j <= iNumVars; j++) {
            if (aVarScore[j] == iBestScore) {
                aBestScoreListPos[j] = iNumBestScoreList;
                aBestScoreList[iNumBestScoreList++] = j;
            } else if (aVarScore[j] < iBestScore) {
                iBestScore = aVarScore[j];
                iNumBestScoreList = 1;
                aBestScoreList[0] = j;
                aBestScoreListPos[j] = 0;
            }
        }
    }

    void UpdateBestScoreList() {
        UINT32 j;
        SINT32 iBestNewScore;
        UINT32 iVar;

        if (iNumChanges == 0) {
            return;
        }

        iBestNewScore = (SINT32) iNumClauses;

        for (j = 0; j < iNumChanges; j++) {
            if (aVarScore[aChangeList[j]] < iBestNewScore) {
                iBestNewScore = aVarScore[aChangeList[j]];
            };
        }

        if (iBestNewScore < iBestScore) {
            iBestScore = iBestNewScore;
            iNumBestScoreList = 0;
            for (j = 0; j < iNumChanges; j++) {
                iVar = aChangeList[j];
                if (aVarScore[iVar] == iBestScore) {
                    aBestScoreListPos[iVar] = iNumBestScoreList;
                    aBestScoreList[iNumBestScoreList++] = iVar;
                }
            }
        } else {
            for (j = 0; j < iNumChanges; j++) {
                iVar = aChangeList[j];
                if (aVarScore[iVar] == iBestScore) {
                    if (aChangeOldScore[iVar] != iBestScore) {
                        aBestScoreListPos[iVar] = iNumBestScoreList;
                        aBestScoreList[iNumBestScoreList++] = iVar;
                    }
                } else if (aChangeOldScore[iVar] == iBestScore) {
                    aBestScoreList[aBestScoreListPos[iVar]] = aBestScoreList[--iNumBestScoreList];
                    aBestScoreListPos[aBestScoreList[iNumBestScoreList]] = aBestScoreListPos[iVar];
                }
            }
        }
        if (iNumBestScoreList == 0) {
            InitBestScoreList();
        }
    }

    void CreateClausePenaltyFL() {
        aClausePenaltyFL = (FLOAT *) AllocateRAM(iNumClauses * sizeof(FLOAT), HeapData);
        bClausePenaltyCreated = 1;
        bClausePenaltyFLOAT = 1;
    }

    void InitClausePenaltyFL() {
        UINT32 j;

        for (j = 0; j < iNumClauses; j++) {
            aClausePenaltyFL[j] = 1.0f;
        }

        fBasePenaltyFL = 1.0f;
        fTotalPenaltyFL = (FLOAT) iNumClauses;

    }

    void InitClausePenaltyFLW() {
        UINT32 j;

        fTotalPenaltyFL = FLOATZERO;

        for (j = 0; j < iNumClauses; j++) {
            aClausePenaltyFL[j] = (FLOAT) aClauseWeight[j];
            fTotalPenaltyFL += aClausePenaltyFL[j];
        }

        fBasePenaltyFL = 1.0f;

    }


    void CreateMakeBreakPenaltyFL() {
        aBreakPenaltyFL = (FLOAT *) AllocateRAM((iNumVars + 1) * sizeof(FLOAT), HeapData);
        aMakePenaltyFL = (FLOAT *) AllocateRAM((iNumVars + 1) * sizeof(FLOAT), HeapData);
        aCritSat = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
    }

    void InitMakeBreakPenaltyFL() {

        UINT32 j;
        UINT32 k;
        UINT32 iVar;
        LITTYPE *pLit;

        for (j = 1; j <= iNumVars; j++) {
            aMakePenaltyFL[j] = FLOATZERO;
            aBreakPenaltyFL[j] = FLOATZERO;
        }

        for (j = 0; j < iNumClauses; j++) {
            if (aNumTrueLit[j] == 0) {
                for (k = 0; k < aClauseLen[j]; k++) {
                    aMakePenaltyFL[GetVar(j, k)] += aClausePenaltyFL[j];
                }
            } else if (aNumTrueLit[j] == 1) {
                pLit = pClauseLits[j];
                for (k = 0; k < aClauseLen[j]; k++) {
                    if IsLitTrue(*pLit) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakPenaltyFL[iVar] += aClausePenaltyFL[j];
                        aCritSat[j] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
        }
    }

    void UpdateMakeBreakPenaltyFL() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        FLOAT fPenalty;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetFalseLit(iFlipCandidate);
        litWasFalse = GetTrueLit(iFlipCandidate);

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            fPenalty = aClausePenaltyFL[*pClause];
            if (aNumTrueLit[*pClause] == 0) {

                aBreakPenaltyFL[iFlipCandidate] -= fPenalty;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakePenaltyFL[iVar] += fPenalty;
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakPenaltyFL[iVar] += fPenalty;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            fPenalty = aClausePenaltyFL[*pClause];
            if (aNumTrueLit[*pClause] == 1) {

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakePenaltyFL[iVar] -= fPenalty;
                    pLit++;

                }
                aBreakPenaltyFL[iFlipCandidate] += fPenalty;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aBreakPenaltyFL[aCritSat[*pClause]] -= fPenalty;
            }
            pClause++;
        }
    }


    void FlipMBPFLandFCLandVIF() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        FLOAT fPenalty;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            fPenalty = aClausePenaltyFL[*pClause];
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                aFalseList[iNumFalse] = *pClause;
                aFalseListPos[*pClause] = iNumFalse++;

                aBreakCount[iFlipCandidate]--;

                aBreakPenaltyFL[iFlipCandidate] -= fPenalty;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakeCount[iVar]++;
                    aMakePenaltyFL[iVar] += fPenalty;

                    if (aMakeCount[iVar] == 1) {
                        aVarInFalseList[iNumVarsInFalseList] = iVar;
                        aVarInFalseListPos[iVar] = iNumVarsInFalseList++;
                    }

                    pLit++;

                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakCount[iVar]++;
                        aBreakPenaltyFL[iVar] += fPenalty;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            fPenalty = aClausePenaltyFL[*pClause];
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalse];
                aFalseListPos[aFalseList[iNumFalse]] = aFalseListPos[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakeCount[iVar]--;
                    aMakePenaltyFL[iVar] -= fPenalty;

                    if (aMakeCount[iVar] == 0) {
                        aVarInFalseList[aVarInFalseListPos[iVar]] = aVarInFalseList[--iNumVarsInFalseList];
                        aVarInFalseListPos[aVarInFalseList[iNumVarsInFalseList]] = aVarInFalseListPos[iVar];
                    }

                    pLit++;

                }
                aBreakCount[iFlipCandidate]++;
                aBreakPenaltyFL[iFlipCandidate] += fPenalty;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aBreakCount[aCritSat[*pClause]]--;
                aBreakPenaltyFL[aCritSat[*pClause]] -= fPenalty;
            }
            pClause++;
        }
    }

    void FlipMBPFLandFCLandVIFandW() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        FLOAT fPenalty;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            fPenalty = aClausePenaltyFL[*pClause];
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                aFalseList[iNumFalse] = *pClause;
                aFalseListPos[*pClause] = iNumFalse++;

                iSumFalseWeight += aClauseWeight[*pClause];

                aBreakCount[iFlipCandidate]--;

                aBreakPenaltyFL[iFlipCandidate] -= fPenalty;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakeCount[iVar]++;
                    aMakePenaltyFL[iVar] += fPenalty;

                    if (aMakeCount[iVar] == 1) {
                        aVarInFalseList[iNumVarsInFalseList] = iVar;
                        aVarInFalseListPos[iVar] = iNumVarsInFalseList++;
                    }

                    pLit++;

                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakCount[iVar]++;
                        aBreakPenaltyFL[iVar] += fPenalty;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            fPenalty = aClausePenaltyFL[*pClause];
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalse];
                aFalseListPos[aFalseList[iNumFalse]] = aFalseListPos[*pClause];

                iSumFalseWeight -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakeCount[iVar]--;
                    aMakePenaltyFL[iVar] -= fPenalty;

                    if (aMakeCount[iVar] == 0) {
                        aVarInFalseList[aVarInFalseListPos[iVar]] = aVarInFalseList[--iNumVarsInFalseList];
                        aVarInFalseListPos[aVarInFalseList[iNumVarsInFalseList]] = aVarInFalseListPos[iVar];
                    }

                    pLit++;

                }
                aBreakCount[iFlipCandidate]++;
                aBreakPenaltyFL[iFlipCandidate] += fPenalty;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aBreakCount[aCritSat[*pClause]]--;
                aBreakPenaltyFL[aCritSat[*pClause]] -= fPenalty;
            }
            pClause++;
        }
    }

    void CreateClausePenaltyINT() {
        aClausePenaltyINT = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
        bClausePenaltyCreated = 1;
        bClausePenaltyFLOAT = 0;
        iInitPenaltyINT = 1;
    }

    void InitClausePenaltyINT() {
        UINT32 j;

        for (j = 0; j < iNumClauses; j++) {
            aClausePenaltyINT[j] = iInitPenaltyINT;
        }
        iBasePenaltyINT = iInitPenaltyINT;
        iTotalPenaltyINT = iNumClauses * iInitPenaltyINT;

    }

    void InitClausePenaltyINTW() {
        UINT32 j;

        ReportPrint(pRepErr, "Warning! InitClausePenaltyINTW() May need to be modified to suit your algorithm\n");

        iTotalPenaltyINT = 0;

        for (j = 0; j < iNumClauses; j++) {
            aClausePenaltyINT[j] = (UINT32) aClauseWeight[j];
            iTotalPenaltyINT += aClausePenaltyINT[j];
        }

        iBasePenaltyINT = 1;

    }


    void CreateMakeBreakPenaltyINT() {
        aBreakPenaltyINT = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
        aMakePenaltyINT = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
        aCritSat = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
    }

    void InitMakeBreakPenaltyINT() {

        UINT32 j;
        UINT32 k;
        UINT32 iVar;
        LITTYPE *pLit;

        for (j = 1; j <= iNumVars; j++) {
            aMakePenaltyINT[j] = 0;
            aBreakPenaltyINT[j] = 0;
        }

        for (j = 0; j < iNumClauses; j++) {
            if (aNumTrueLit[j] == 0) {
                for (k = 0; k < aClauseLen[j]; k++) {
                    aMakePenaltyINT[GetVar(j, k)] += aClausePenaltyINT[j];
                }
            } else if (aNumTrueLit[j] == 1) {
                pLit = pClauseLits[j];
                for (k = 0; k < aClauseLen[j]; k++) {
                    if IsLitTrue(*pLit) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakPenaltyINT[iVar] += aClausePenaltyINT[j];
                        aCritSat[j] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
        }
    }

    void UpdateMakeBreakPenaltyINT() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        UINT32 iPenalty;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetFalseLit(iFlipCandidate);
        litWasFalse = GetTrueLit(iFlipCandidate);

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            iPenalty = aClausePenaltyINT[*pClause];
            if (aNumTrueLit[*pClause] == 0) {

                aBreakPenaltyINT[iFlipCandidate] -= iPenalty;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakePenaltyINT[iVar] += iPenalty;
                    pLit++;
                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakPenaltyINT[iVar] += iPenalty;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            iPenalty = aClausePenaltyINT[*pClause];
            if (aNumTrueLit[*pClause] == 1) {

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakePenaltyINT[iVar] -= iPenalty;
                    pLit++;

                }
                aBreakPenaltyINT[iFlipCandidate] += iPenalty;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aBreakPenaltyINT[aCritSat[*pClause]] -= iPenalty;
            }
            pClause++;
        }
    }


    void FlipMBPINTandFCLandVIF() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        UINT32 iPenalty;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            iPenalty = aClausePenaltyINT[*pClause];
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                aFalseList[iNumFalse] = *pClause;
                aFalseListPos[*pClause] = iNumFalse++;

                aBreakCount[iFlipCandidate]--;

                aBreakPenaltyINT[iFlipCandidate] -= iPenalty;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakeCount[iVar]++;
                    aMakePenaltyINT[iVar] += iPenalty;

                    if (aMakeCount[iVar] == 1) {
                        aVarInFalseList[iNumVarsInFalseList] = iVar;
                        aVarInFalseListPos[iVar] = iNumVarsInFalseList++;
                    }

                    pLit++;

                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakCount[iVar]++;
                        aBreakPenaltyINT[iVar] += iPenalty;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            iPenalty = aClausePenaltyINT[*pClause];
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalse];
                aFalseListPos[aFalseList[iNumFalse]] = aFalseListPos[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakeCount[iVar]--;
                    aMakePenaltyINT[iVar] -= iPenalty;

                    if (aMakeCount[iVar] == 0) {
                        aVarInFalseList[aVarInFalseListPos[iVar]] = aVarInFalseList[--iNumVarsInFalseList];
                        aVarInFalseListPos[aVarInFalseList[iNumVarsInFalseList]] = aVarInFalseListPos[iVar];
                    }

                    pLit++;

                }
                aBreakCount[iFlipCandidate]++;
                aBreakPenaltyINT[iFlipCandidate] += iPenalty;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aBreakCount[aCritSat[*pClause]]--;
                aBreakPenaltyINT[aCritSat[*pClause]] -= iPenalty;
            }
            pClause++;
        }
    }

    void FlipMBPINTandFCLandVIFandW() {

        UINT32 j;
        UINT32 k;
        UINT32 *pClause;
        UINT32 iVar;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        UINT32 iPenalty;

        if (iFlipCandidate == 0) {
            return;
        }

        litWasTrue = GetTrueLit(iFlipCandidate);
        litWasFalse = GetFalseLit(iFlipCandidate);

        aVarValue[iFlipCandidate] = !aVarValue[iFlipCandidate];

        pClause = pLitClause[litWasTrue];
        for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
            iPenalty = aClausePenaltyINT[*pClause];
            aNumTrueLit[*pClause]--;
            if (aNumTrueLit[*pClause] == 0) {

                aFalseList[iNumFalse] = *pClause;
                aFalseListPos[*pClause] = iNumFalse++;

                iSumFalseWeight += aClauseWeight[*pClause];

                aBreakCount[iFlipCandidate]--;

                aBreakPenaltyINT[iFlipCandidate] -= iPenalty;

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakeCount[iVar]++;
                    aMakePenaltyINT[iVar] += iPenalty;

                    if (aMakeCount[iVar] == 1) {
                        aVarInFalseList[iNumVarsInFalseList] = iVar;
                        aVarInFalseListPos[iVar] = iNumVarsInFalseList++;
                    }

                    pLit++;

                }
            }
            if (aNumTrueLit[*pClause] == 1) {
                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    if (IsLitTrue(*pLit)) {
                        iVar = GetVarFromLit(*pLit);
                        aBreakCount[iVar]++;
                        aBreakPenaltyINT[iVar] += iPenalty;
                        aCritSat[*pClause] = iVar;
                        break;
                    }
                    pLit++;
                }
            }
            pClause++;
        }

        pClause = pLitClause[litWasFalse];
        for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
            iPenalty = aClausePenaltyINT[*pClause];
            aNumTrueLit[*pClause]++;
            if (aNumTrueLit[*pClause] == 1) {

                aFalseList[aFalseListPos[*pClause]] = aFalseList[--iNumFalse];
                aFalseListPos[aFalseList[iNumFalse]] = aFalseListPos[*pClause];

                iSumFalseWeight -= aClauseWeight[*pClause];

                pLit = pClauseLits[*pClause];
                for (k = 0; k < aClauseLen[*pClause]; k++) {
                    iVar = GetVarFromLit(*pLit);
                    aMakeCount[iVar]--;
                    aMakePenaltyINT[iVar] -= iPenalty;

                    if (aMakeCount[iVar] == 0) {
                        aVarInFalseList[aVarInFalseListPos[iVar]] = aVarInFalseList[--iNumVarsInFalseList];
                        aVarInFalseListPos[aVarInFalseList[iNumVarsInFalseList]] = aVarInFalseListPos[iVar];
                    }

                    pLit++;

                }
                aBreakCount[iFlipCandidate]++;
                aBreakPenaltyINT[iFlipCandidate] += iPenalty;
                aCritSat[*pClause] = iFlipCandidate;
            }
            if (aNumTrueLit[*pClause] == 2) {
                aBreakCount[aCritSat[*pClause]]--;
                aBreakPenaltyINT[aCritSat[*pClause]] -= iPenalty;
            }
            pClause++;
        }
    }

    void InitNullFlips() {
        iNumNullFlips = 0;
    }

    void UpdateNullFlips() {
        if (iFlipCandidate == 0) {
            iNumNullFlips++;
        }
    }

    void InitLocalMins() {
        iNumLocalMins = 0;
    }

    void UpdateLocalMins() {
        if (IsLocalMinimum(bWeighted)) {
            iNumLocalMins++;
        }
    }

    void CreateLogDist() {
        UINT32 iMaxLogDistValues;
        FLOAT fCurValue;
        FLOAT fFactor;

        iMaxLogDistValues = ((UINT32) log10((FLOAT) iCutoff)) * iLogDistStepsPerDecade + 2;
        aLogDistValues = (UBIGINT *) AllocateRAM(iMaxLogDistValues * sizeof(UBIGINT), HeapReports);

        fFactor = powf(10.0f, 1.0f / (float) iLogDistStepsPerDecade);
        fCurValue = 10.0f;

        iNumLogDistValues = 0;

        while (fCurValue <= (FLOAT) iCutoff) {
            aLogDistValues[iNumLogDistValues++] = (UBIGINT) fCurValue;
            fCurValue *= fFactor;
        }
        if (aLogDistValues[iNumLogDistValues - 1] != iCutoff) {
            aLogDistValues[iNumLogDistValues++] = iCutoff;
        }
    }

    void InitBestFalse() {
        iBestNumFalse = iNumClauses + 1;
        iBestStepNumFalse = iStep;

        iBestSumFalseWeight = UBIGINTMAX;
        iBestStepSumFalseWeight = iStep;
    }

    void UpdateBestFalse() {
        /* Only record the iBestStepNumFalse on first meet.
         * SaveBest check on iBestStepNumFalse, thus update bsf on first meet. */
        if (iNumFalse < iBestNumFalse) {
            iBestNumFalse = iNumFalse;
            iBestStepNumFalse = iStep;
        }
        if (bWeighted) {
            if (iSumFalseWeight < iBestSumFalseWeight) {
                iBestSumFalseWeight = iSumFalseWeight;
                iBestStepSumFalseWeight = iStep;
            }
        }
    }

    void CreateSaveBest() {
        vsBest = NewVarState();
    }

    void UpdateSaveBest() {
        BOOL bSave = 0;

        if (bWeighted) {
            if (iBestStepSumFalseWeight == iStep) {
                bSave = 1;
            }
        } else {
            if (iBestStepNumFalse == iStep) {
                bSave = 1;
            }
        }
        if (bSave) {
            SetCurVarState(vsBest);
        }
    }


    void UpdateStartFalse() {
        if (iStep == 1) {
            iStartNumFalse = iNumFalse;
            if (bWeighted) {
                iStartSumFalseWeight = iSumFalseWeight;
            }
        }
    }


    void CalcImproveMean() {
        if (iBestStepNumFalse <= 1) {
            fImproveMean = FLOATZERO;
        } else {
            fImproveMean = (FLOAT) (iStartNumFalse - iBestNumFalse) / (FLOAT) (iBestStepNumFalse - 1);
        }
        if (bWeighted) {
            if (iBestStepSumFalseWeight <= 1) {
                fImproveMeanW = FLOATZERO;
            } else {
                fImproveMeanW =
                        (FLOAT) (iStartSumFalseWeight - iBestSumFalseWeight) / (FLOAT) (iBestStepSumFalseWeight - 1);
            }
        }
    }


    void InitFirstLM() {
        iFirstLM = 0;
        iFirstLMStep = 0;
        iFirstLMWeight = 0;
        iFirstLMStepW = 0;
    }

    void UpdateFirstLM() {
        if (iFirstLMStep == 0) {
            if (IsLocalMinimum(0)) {
                iFirstLM = iNumFalse;
                iFirstLMStep = iStep;
            }
        }
        if ((bWeighted) && (iFirstLMStepW == FLOATZERO)) {
            if (IsLocalMinimum(1)) {
                iFirstLMWeight = iSumFalseWeight;
                iFirstLMStepW = iStep;
            }
        }
    }


    void CalcFirstLMRatio() {
        fFirstLMRatio = FLOATZERO;
        fFirstLMRatioW = FLOATZERO;
        if (iStartNumFalse != iBestNumFalse) {
            fFirstLMRatio = (FLOAT) (iStartNumFalse - iFirstLM) / (FLOAT) (iStartNumFalse - iBestNumFalse);
        }
        if (bWeighted) {
            if (iStartSumFalseWeight != iBestSumFalseWeight) {
                fFirstLMRatioW =
                        (FLOAT) (iStartSumFalseWeight - iFirstLMWeight) / (iStartSumFalseWeight - iBestSumFalseWeight);
            }
        }
    }

    void UpdateTrajBestLM() {

        if (iStep == 1) {
            iTrajBestLMCount = 0;
            fTrajBestLMSum = FLOATZERO;
            fTrajBestLMSum2 = FLOATZERO;
            if (bWeighted) {
                iTrajBestLMCountW = 0;
                fTrajBestLMSumW = FLOATZERO;
                fTrajBestLMSum2W = FLOATZERO;
            }
        } else {
            if (iBestStepNumFalse == (iStep - 1)) {
                iTrajBestLMCount++;
                fTrajBestLMSum += (FLOAT) iBestNumFalse;
                fTrajBestLMSum2 += (FLOAT) (iBestNumFalse * iBestNumFalse);
            }
            if (bWeighted) {
                if (iBestStepSumFalseWeight == (iStep - 1)) {
                    iTrajBestLMCountW++;
                    fTrajBestLMSumW += (FLOAT) iBestSumFalseWeight;
                    fTrajBestLMSum2W += (FLOAT) (iBestSumFalseWeight * iBestSumFalseWeight);
                }
            }
        }
    }


    void CalcTrajBestLM() {
        FLOAT fStdDev;
        if (iBestStepNumFalse == iStep) {
            iTrajBestLMCount++;
            fTrajBestLMSum += (FLOAT) iBestNumFalse;
            fTrajBestLMSum2 += (FLOAT) (iBestNumFalse * iBestNumFalse);
        }
        if (bWeighted) {
            if (iBestStepSumFalseWeight == (iStep - 1)) {
                iTrajBestLMCountW++;
                fTrajBestLMSumW += (FLOAT) iBestSumFalseWeight;
                fTrajBestLMSum2W += (FLOAT) (iBestSumFalseWeight * iBestSumFalseWeight);
            }
        }

        CalculateStats(&fTrajBestLMMean, &fStdDev, &fTrajBestLMCV, fTrajBestLMSum, fTrajBestLMSum2, iTrajBestLMCount);
        if (bWeighted) {
            CalculateStats(&fTrajBestLMMeanW, &fStdDev, &fTrajBestLMCVW, fTrajBestLMSumW, fTrajBestLMSum2W,
                           iTrajBestLMCountW);
        }
    }


    void CheckNoImprove() {
        if (iNoImprove) {
            if (iStep > (iBestStepNumFalse + iNoImprove)) {
                bTerminateRun = 1;
            }
            if (bWeighted) {
                if (iStep > (iBestStepSumFalseWeight + iNoImprove)) {
                    bTerminateRun = 1;
                }
            }
        }
    }


    void CheckEarlyTerm() {
        if (iEarlyTermSteps) {
            if (iStep == iEarlyTermSteps) {
                if (bWeighted) {
                    if (iEarlyTermQualWeight) {
                        if (iBestSumFalseWeight > iEarlyTermQualWeight) {
                            bTerminateRun = 1;
                        }
                    }
                } else {
                    if (iEarlyTermQual) {
                        if (iBestNumFalse > iEarlyTermQual) {
                            bTerminateRun = 1;
                        }
                    }
                }
            }
        }
    }


    void CheckStrikes() {
        if (!bSolutionFound) {
            if ((iRun - iNumSolutionsFound) >= iStrikes) {
                bTerminateAllRuns = 1;
            }
        }
    }


    void StartSeed() {
        if (iRun == 1) {
            iStartSeed = iSeed;
        } else {
            iStartSeed = RandomMax();
            RandomSeed(iStartSeed);
        }
    }

    void CheckTimeout() {
        double fTimeElapsed;

        if ((iTimeResolution <= 1) || ((iTimeResolution % iTimeResolution) == 0)) {
            if (fTimeOut > FLOATZERO) {
                fTimeElapsed = RunTimeElapsed();
                if (fTimeElapsed > (double) fTimeOut) {
                    bTerminateRun = 1;
                }
            }
            if (fGlobalTimeOut > FLOATZERO) {
                fTimeElapsed = TotalTimeElapsed();
                if (fTimeElapsed > (double) fGlobalTimeOut) {
                    bTerminateRun = 1;
                    bTerminateAllRuns = 1;
                }
            }
        }
    }

    void CheckForRestarts() {
        if (iPeriodicRestart) {
            if ((iStep % iPeriodicRestart) == 0) {
                bRestart = 1;
            }
        }
        if (iProbRestart) {
            if (RandomProb(iProbRestart)) {
                bRestart = 1;
            }
        }
        if (iStagnateRestart) {
            if (iStep > (iBestStepNumFalse + iStagnateRestart)) {
                bRestart = 1;
                InitBestFalse();
            }
            if (bWeighted) {
                if (iStep > (iBestStepSumFalseWeight + iStagnateRestart)) {
                    bRestart = 1;
                    InitBestFalse();
                }
            }
        }
    }

    void CreateFlipCounts() {
        aFlipCounts = (UBIGINT *) AllocateRAM((iNumVars + 1) * sizeof(UBIGINT), HeapReports);
    }

    void InitFlipCounts() {
        if (iStep == 1) {
            memset(aFlipCounts, 0, (iNumVars + 1) * sizeof(UBIGINT));
        }
    }

    void UpdateFlipCounts() {
        aFlipCounts[iFlipCandidate]++;
    }

    void FlipCountStats() {
        UINT32 j;
        FLOAT fSum = FLOATZERO;
        FLOAT fSum2 = FLOATZERO;

        for (j = 1; j < (iNumVars + 1); j++) {
            fSum += (FLOAT) aFlipCounts[j];
            fSum2 += (FLOAT) (aFlipCounts[j] * aFlipCounts[j]);
        }

        CalculateStats(&fFlipCountsMean, &fFlipCountsStdDev, &fFlipCountsCV, fSum, fSum2, iNumVars);
    }

    void CreateBiasCounts() {
        aBiasTrueCounts = (UBIGINT *) AllocateRAM((iNumVars + 1) * sizeof(UBIGINT), HeapReports);
        aBiasFalseCounts = (UBIGINT *) AllocateRAM((iNumVars + 1) * sizeof(UBIGINT), HeapReports);
    }

    void PreInitBiasCounts() {
        UINT32 j;
        if (iStep == 1) {
            memset(aBiasTrueCounts, 0, (iNumVars + 1) * sizeof(UBIGINT));
            memset(aBiasFalseCounts, 0, (iNumVars + 1) * sizeof(UBIGINT));
        } else {
            for (j = 1; j <= iNumVars; j++) {
                if (aVarValue[j]) {
                    aBiasTrueCounts[j] += (iStep - 1 - (aVarLastChange[j]));
                } else {
                    aBiasFalseCounts[j] += (iStep - 1 - (aVarLastChange[j]));
                }
            }
        }
    }

    void UpdateBiasCounts() {
        if (iFlipCandidate) {
            if (aVarValue[iFlipCandidate]) {
                aBiasTrueCounts[iFlipCandidate] += (iStep - (aVarLastChange[iFlipCandidate]));
            } else {
                aBiasFalseCounts[iFlipCandidate] += (iStep - (aVarLastChange[iFlipCandidate]));
            }
        }
    }

    void FinalBiasCounts() {
        UINT32 j;
        for (j = 1; j <= iNumVars; j++) {
            if (aVarValue[j]) {
                aBiasTrueCounts[j] += (iStep - (aVarLastChange[j]));
            } else {
                aBiasFalseCounts[j] += (iStep - (aVarLastChange[j]));
            }
        }
    }

    void BiasStats() {
        UBIGINT iCountSame;
        UBIGINT iCountDiff;
        UBIGINT iCountMax;
        UBIGINT iCountMin;
        UINT32 j;

        iCountSame = 0;
        iCountDiff = 0;
        iCountMax = 0;
        iCountMin = 0;

        for (j = 1; j <= iNumVars; j++) {
            if (aVarValue[j]) {
                iCountSame += aBiasTrueCounts[j];
                iCountDiff += aBiasFalseCounts[j];
            } else {
                iCountSame += aBiasFalseCounts[j];
                iCountDiff += aBiasTrueCounts[j];
            }
            if (aBiasTrueCounts[j] > aBiasFalseCounts[j]) {
                iCountMax += aBiasTrueCounts[j];
                iCountMin += aBiasFalseCounts[j];
            } else {
                iCountMax += aBiasFalseCounts[j];
                iCountMin += aBiasTrueCounts[j];
            }
        }
        if (iCountSame + iCountDiff > 0) {
            fMeanFinalBias = ((FLOAT) iCountSame) / ((FLOAT) (iCountSame + iCountDiff));
        } else {
            fMeanFinalBias = FLOATZERO;
        }
        if (iCountMax + iCountMin > 0) {
            fMeanMaxBias = ((FLOAT) iCountMax) / ((FLOAT) (iCountMax + iCountMin));
        } else {
            fMeanMaxBias = FLOATZERO;
        }
    }

    void CreateUnsatCounts() {
        aUnsatCounts = (UBIGINT *) AllocateRAM(iNumClauses * sizeof(UBIGINT), HeapReports);
    }

    void InitUnsatCounts() {
        if (iStep == 1) {
            memset(aUnsatCounts, 0, iNumClauses * sizeof(UBIGINT));
        }
    }

    void UpdateUnsatCounts() {
        UINT32 j;
        for (j = 0; j < iNumClauses; j++) {
            if (aNumTrueLit[j] == 0) {
                aUnsatCounts[j]++;
            }
        }
    }

    void UnsatCountStats() {
        UINT32 j;
        FLOAT fSum = FLOATZERO;
        FLOAT fSum2 = FLOATZERO;

        for (j = 0; j < iNumClauses; j++) {
            fSum += (FLOAT) aUnsatCounts[j];
            fSum2 += (FLOAT) (aUnsatCounts[j] * aUnsatCounts[j]);
        }

        CalculateStats(&fUnsatCountsMean, &fUnsatCountsStdDev, &fUnsatCountsCV, fSum, fSum2, iNumClauses);
    }

    void CreateNumFalseCounts() {
        aNumFalseCounts = (UBIGINT *) AllocateRAM((iNumClauses + 1) * sizeof(UBIGINT), HeapReports);
        if (iReportFalseHistCount) {
            aNumFalseCountsWindow = (UBIGINT *) AllocateRAM(iReportFalseHistCount * sizeof(UBIGINT), HeapReports);
        }
    }

    void InitNumFalseCounts() {
        if (iStep == 1) {
            memset(aNumFalseCounts, 0, (iNumClauses + 1) * sizeof(UBIGINT));
            if (iReportFalseHistCount) {
                memset(aNumFalseCountsWindow, 0, iReportFalseHistCount * sizeof(UBIGINT));
            }
        }
    }

    void UpdateNumFalseCounts() {
        aNumFalseCounts[iNumFalse]++;

        if (iReportFalseHistCount) {
            if (iStep > iReportFalseHistCount) {
                aNumFalseCounts[aNumFalseCountsWindow[iStep % iReportFalseHistCount]]--;
            }
            aNumFalseCountsWindow[iStep % iReportFalseHistCount] = iNumFalse;
        }
    }

    void CreateDistanceCounts() {
        aDistanceCounts = (UBIGINT *) AllocateRAM((iNumVars + 1) * sizeof(UBIGINT), HeapReports);
        if (iReportDistHistCount) {
            aDistanceCountsWindow = (UBIGINT *) AllocateRAM(iReportDistHistCount * sizeof(UBIGINT), HeapReports);
        }
    }

    void InitDistanceCounts() {
        if (iStep == 1) {
            memset(aDistanceCounts, 0, (iNumVars + 1) * sizeof(UBIGINT));
            if (iReportDistHistCount) {
                memset(aDistanceCountsWindow, 0, iReportDistHistCount * sizeof(UBIGINT));
            }
        }
    }

    void UpdateDistanceCounts() {

        aDistanceCounts[iSolutionDistance]++;

        if (iReportDistHistCount) {
            if (iStep > iReportDistHistCount) {
                aDistanceCounts[aDistanceCountsWindow[iStep % iReportDistHistCount]]--;
            }
            aDistanceCountsWindow[iStep % iReportDistHistCount] = iSolutionDistance;
        }
    }

    void CreateClauseLast() {
        aClauseLast = (UBIGINT *) AllocateRAM(iNumClauses * sizeof(UBIGINT), HeapData);
    }

    void InitClauseLast() {
        if (iStep == 1) {
            memset(aClauseLast, 0, iNumClauses * sizeof(UBIGINT));
        }
    }

    void UpdateClauseLast() {
        UINT32 j;
        for (j = 0; j < iNumClauses; j++) {
            if (aNumTrueLit[j] == 0) {
                aClauseLast[j] = iStep;
            }
        }
    }

    UINT32 iNextSQGridCol;

    void CreateSQGrid() {
        if (bWeighted) {
            aSQGridWeight = (UBIGINT *) AllocateRAM(iNumLogDistValues * iNumRuns * sizeof(UBIGINT), HeapReports);
        } else {
            aSQGrid = (UINT32 *) AllocateRAM(iNumLogDistValues * iNumRuns * sizeof(UINT32), HeapReports);
        }
    }

    void InitSQGrid() {
        iNextSQGridCol = 0;
    }

    void UpdateSQGrid() {

        if (iStep != aLogDistValues[iNextSQGridCol]) {
            return;
        }

        if (bWeighted) {
            aSQGridWeight[iNumLogDistValues * (iRun - 1) + iNextSQGridCol++] = iBestSumFalseWeight;
        } else {
            aSQGrid[iNumLogDistValues * (iRun - 1) + iNextSQGridCol++] = iBestNumFalse;
        }
    }

    void FinishSQGrid() {
        if (bWeighted) {
            while (iNextSQGridCol < iNumLogDistValues) {
                aSQGridWeight[iNumLogDistValues * (iRun - 1) + iNextSQGridCol++] = iBestSumFalseWeight;
            }
        } else {
            while (iNextSQGridCol < iNumLogDistValues) {
                aSQGrid[iNumLogDistValues * (iRun - 1) + iNextSQGridCol++] = iBestNumFalse;
            }
        }
    }

    void CreatePenaltyStats() {
        aPenaltyStatsMean = (FLOAT *) AllocateRAM(iNumClauses * sizeof(FLOAT), HeapReports);
        aPenaltyStatsStddev = (FLOAT *) AllocateRAM(iNumClauses * sizeof(FLOAT), HeapReports);
        aPenaltyStatsCV = (FLOAT *) AllocateRAM(iNumClauses * sizeof(FLOAT), HeapReports);

        aPenaltyStatsSum = (FLOAT *) AllocateRAM(iNumClauses * sizeof(FLOAT), HeapReports);
        aPenaltyStatsSum2 = (FLOAT *) AllocateRAM(iNumClauses * sizeof(FLOAT), HeapReports);

        aPenaltyStatsMeanSum = (FLOAT *) AllocateRAM(iNumClauses * sizeof(FLOAT), HeapReports);
        aPenaltyStatsMeanSum2 = (FLOAT *) AllocateRAM(iNumClauses * sizeof(FLOAT), HeapReports);
        aPenaltyStatsStddevSum = (FLOAT *) AllocateRAM(iNumClauses * sizeof(FLOAT), HeapReports);
        aPenaltyStatsStddevSum2 = (FLOAT *) AllocateRAM(iNumClauses * sizeof(FLOAT), HeapReports);
        aPenaltyStatsCVSum = (FLOAT *) AllocateRAM(iNumClauses * sizeof(FLOAT), HeapReports);
        aPenaltyStatsCVSum2 = (FLOAT *) AllocateRAM(iNumClauses * sizeof(FLOAT), HeapReports);

        memset(aPenaltyStatsMeanSum, 0, iNumClauses * sizeof(FLOAT));
        memset(aPenaltyStatsMeanSum2, 0, iNumClauses * sizeof(FLOAT));
        memset(aPenaltyStatsStddevSum, 0, iNumClauses * sizeof(FLOAT));
        memset(aPenaltyStatsStddevSum2, 0, iNumClauses * sizeof(FLOAT));
        memset(aPenaltyStatsCVSum, 0, iNumClauses * sizeof(FLOAT));
        memset(aPenaltyStatsCVSum2, 0, iNumClauses * sizeof(FLOAT));

    }

    void InitPenaltyStats() {
        memset(aPenaltyStatsSum, 0, iNumClauses * sizeof(FLOAT));
        memset(aPenaltyStatsSum2, 0, iNumClauses * sizeof(FLOAT));
    }

    void UpdatePenaltyStatsStep() {
        UINT32 j;
        FLOAT fCurPen;

        for (j = 0; j < iNumClauses; j++) {
            if (bClausePenaltyFLOAT) {
                if (bReportPenaltyReNormFraction) {
                    fCurPen = aClausePenaltyFL[j] / fTotalPenaltyFL;
                } else {
                    if (bReportPenaltyReNormBase) {
                        fCurPen = aClausePenaltyFL[j] / fBasePenaltyFL;
                    } else {
                        fCurPen = aClausePenaltyFL[j];
                    }
                }
            } else {
                if (bReportPenaltyReNormFraction) {
                    fCurPen = ((FLOAT) aClausePenaltyINT[j]) / ((FLOAT) iTotalPenaltyINT);
                } else {
                    if (bReportPenaltyReNormBase) {
                        fCurPen = ((FLOAT) aClausePenaltyINT[j]) / ((FLOAT) iBasePenaltyINT);
                    } else {
                        fCurPen = (FLOAT) aClausePenaltyINT[j];
                    }
                }
            }

            aPenaltyStatsSum[j] += fCurPen;
            aPenaltyStatsSum2[j] += (fCurPen * fCurPen);

            CalculateStats(&aPenaltyStatsMean[j], &aPenaltyStatsStddev[j], &aPenaltyStatsCV[j], aPenaltyStatsSum[j],
                           aPenaltyStatsSum2[j], iStep);
        }
    }

    void UpdatePenaltyStatsRun() {
        UINT32 j;
        for (j = 0; j < iNumClauses; j++) {
            aPenaltyStatsMeanSum[j] += aPenaltyStatsMean[j];
            aPenaltyStatsMeanSum2[j] += (aPenaltyStatsMean[j] * aPenaltyStatsMean[j]);
            aPenaltyStatsStddevSum[j] += aPenaltyStatsStddev[j];
            aPenaltyStatsStddevSum2[j] += (aPenaltyStatsStddev[j] * aPenaltyStatsStddev[j]);
            aPenaltyStatsCVSum[j] += aPenaltyStatsCV[j];
            aPenaltyStatsCVSum2[j] += (aPenaltyStatsCV[j] * aPenaltyStatsCV[j]);
        }
    }

    void CreateVarFlipHistory() {

        iVarFlipHistoryLen = 0;

        if (pRepMobility->bActive) {
            if (iReportMobilityDisplay == 0) {
                iReportMobilityDisplay = iNumVars;
            }

            iVarFlipHistoryLen = iReportMobilityDisplay + 1;
        }

        if ((pRepMobFixed->bActive) || (pRepMobFixedFreq->bActive) || (bMobilityColXActive)) {
            if (iMobFixedWindow == 0) {
                iMobFixedWindow = iNumVars;
            }

            if (iVarFlipHistoryLen < iMobFixedWindow + 1) {
                iVarFlipHistoryLen = iMobFixedWindow + 1;
            }
        }

        if (bMobilityColNActive) {
            if (iVarFlipHistoryLen < iNumVars + 1) {
                iVarFlipHistoryLen = iNumVars + 1;
            }
        }

        if (iVarFlipHistoryLen == 0) {
            ReportPrint1(pRepErr, "Warning! Unknown Mobility Window Size requested (setting to %"P32")\n", iNumVars);
            iVarFlipHistoryLen = iNumVars + 1;
        }

        aVarFlipHistory = (UINT32 *) AllocateRAM(iVarFlipHistoryLen * sizeof(UINT32), HeapData);
    }

    void UpdateVarFlipHistory() {
        aVarFlipHistory[iStep % iVarFlipHistoryLen] = iFlipCandidate;
    }


    void CreateMobilityWindow() {
        if (iVarFlipHistoryLen == 0) {
            ReportPrint(pRepErr, "Unexpected Error: Variable Flip History Length = 0\n");
        }
        aMobilityWindowVarChange = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapReports);
        aMobilityWindow = (UINT32 *) AllocateRAM((iVarFlipHistoryLen + 1) * sizeof(UINT32), HeapReports);
        aMobilityWindowSum = (FLOAT *) AllocateRAM((iVarFlipHistoryLen + 1) * sizeof(FLOAT), HeapReports);
        aMobilityWindowSum2 = (FLOAT *) AllocateRAM((iVarFlipHistoryLen + 1) * sizeof(FLOAT), HeapReports);
    }

    void InitMobilityWindow() {
        memset(aMobilityWindow, 0, (iVarFlipHistoryLen + 1) * sizeof(UINT32));
        memset(aMobilityWindowSum, 0, (iVarFlipHistoryLen + 1) * sizeof(FLOAT));
        memset(aMobilityWindowSum2, 0, (iVarFlipHistoryLen + 1) * sizeof(FLOAT));
    }

    void UpdateMobilityWindow() {

        UINT32 iWindowLen;
        UBIGINT iStopWindow;
        SINT32 iCurPos;
        UINT32 iCurVar;

        if (iStep == 1) {
            return;
        }

        memset(aMobilityWindowVarChange, 0, (iNumVars + 1) * sizeof(UINT32));

        if (iStep < iVarFlipHistoryLen) {
            iStopWindow = iStep;
        } else {
            iStopWindow = iVarFlipHistoryLen;
        }

        aMobilityWindow[0] = 0;
        iCurPos = (SINT32) (iStep % iVarFlipHistoryLen);

        for (iWindowLen = 1; iWindowLen < iStopWindow; iWindowLen++) {

            aMobilityWindow[iWindowLen] = aMobilityWindow[iWindowLen - 1];

            iCurVar = aVarFlipHistory[iCurPos];

            if (iCurVar) {

                aMobilityWindowVarChange[iCurVar] = 1 - aMobilityWindowVarChange[iCurVar];

                if (aMobilityWindowVarChange[iCurVar]) {
                    aMobilityWindow[iWindowLen]++;
                } else {
                    aMobilityWindow[iWindowLen]--;
                }
            }

            aMobilityWindowSum[iWindowLen] += (FLOAT) aMobilityWindow[iWindowLen];
            aMobilityWindowSum2[iWindowLen] += (FLOAT) (aMobilityWindow[iWindowLen] * aMobilityWindow[iWindowLen]);

            iCurPos--;
            if (iCurPos == -1) {
                iCurPos = (SINT32) (iVarFlipHistoryLen - 1);
            }
        }
    }


    void CreateMobilityFixedFrequencies() {
        if (iMobFixedWindow == 0) {
            ReportPrint(pRepErr, "Unexpected Error: Mobility Fixed Size = 0\n");
        }

        aMobilityFixedFrequencies = (UINT32 *) AllocateRAM((iMobFixedWindow + 1) * sizeof(UINT32), HeapReports);
    }

    void InitMobilityFixedFrequencies() {
        memset(aMobilityFixedFrequencies, 0, (iMobFixedWindow + 1) * sizeof(UINT32));
    }

    void UpdateMobilityFixedFrequencies() {
        if (iStep <= iMobFixedWindow) {
            if (bMobilityFixedIncludeStart) {
                aMobilityFixedFrequencies[aMobilityWindow[iStep - 1]]++;
            }
        } else {
            aMobilityFixedFrequencies[aMobilityWindow[iMobFixedWindow]]++;
        }
    }


    void CreateVarAgeFrequencies() {
        if (iMaxVarAgeFrequency == 0) {
            iMaxVarAgeFrequency = iNumVars * 10;
        }
        aVarAgeFrequency = (UBIGINT *) AllocateRAM((iMaxVarAgeFrequency + 1) * sizeof(UBIGINT), HeapReports);
    }

    void InitVarAgeFrequencies() {
        memset(aVarAgeFrequency, 0, (iMaxVarAgeFrequency + 1) * sizeof(UBIGINT));
    }

    void UpdateVarAgeFrequencies() {
        UBIGINT iAge;
        if (iFlipCandidate > 0) {
            iAge = iStep - aVarLastChange[iFlipCandidate];
            if (iAge > iMaxVarAgeFrequency) {
                iAge = iMaxVarAgeFrequency;
            }
            aVarAgeFrequency[iAge]++;
        }
    }


    void CreateAutoCorr() {

        if (iAutoCorrMaxLen == 0) {
            iAutoCorrMaxLen = iNumVars;
        }
        aAutoCorrValues = (FLOAT *) AllocateRAM((iAutoCorrMaxLen) * sizeof(FLOAT), HeapReports);
        aAutoCorrStartBuffer = (FLOAT *) AllocateRAM((iAutoCorrMaxLen) * sizeof(FLOAT), HeapReports);
        aAutoCorrEndCircBuffer = (FLOAT *) AllocateRAM((iAutoCorrMaxLen) * sizeof(FLOAT), HeapReports);
        aAutoCorrCrossSum = (FLOAT *) AllocateRAM((iAutoCorrMaxLen) * sizeof(FLOAT), HeapReports);
    }

    void InitAutoCorr() {
        fAutoCorrSum = FLOATZERO;
        fAutoCorrSum2 = FLOATZERO;
        memset(aAutoCorrCrossSum, 0, iAutoCorrMaxLen * sizeof(FLOAT));
    }

    void UpdateAutoCorr() {

        FLOAT fCurValue;
        UINT32 iLoopIndex;
        UINT32 k;

        if (bWeighted) {
            fCurValue = (FLOAT) iSumFalseWeight;
        } else {
            fCurValue = (FLOAT) iNumFalse;
        }

        if (iStep <= (UBIGINT) iAutoCorrMaxLen) {
            aAutoCorrStartBuffer[iStep - 1] = fCurValue;
            iLoopIndex = (UINT32) (iStep - 1);
        } else {
            iLoopIndex = iAutoCorrMaxLen;
        }

        fAutoCorrSum += fCurValue;
        fAutoCorrSum2 += (fCurValue * fCurValue);

        for (k = 0; k < iLoopIndex; k++) {
            aAutoCorrCrossSum[k] += (fCurValue * aAutoCorrEndCircBuffer[(iStep - k - 1) % iAutoCorrMaxLen]);
        }

        aAutoCorrEndCircBuffer[(iStep % iAutoCorrMaxLen)] = fCurValue;

    }

    void CalcAutoCorr() {

        UINT32 k;
        UINT32 iLoopIndex;

        FLOAT fValue;
        FLOAT fSumStart = fAutoCorrSum;
        FLOAT fSumStart2 = fAutoCorrSum2;
        FLOAT fSumEnd = fAutoCorrSum;
        FLOAT fSumEnd2 = fAutoCorrSum2;

        if (iStep <= (UBIGINT) iAutoCorrMaxLen) {
            iLoopIndex = (UINT32) iStep - 1;
            for (k = (UINT32) iStep; k < iAutoCorrMaxLen; k++) {
                aAutoCorrValues[k] = FLOATZERO;
            }
        } else {
            iLoopIndex = iAutoCorrMaxLen;
        }

        for (k = 0; k < iLoopIndex; k++) {
            fValue = aAutoCorrStartBuffer[k];
            fSumStart -= fValue;
            fSumStart2 -= (fValue * fValue);
            fValue = aAutoCorrEndCircBuffer[(iStep - k) % iAutoCorrMaxLen];
            fSumEnd -= fValue;
            fSumEnd2 -= (fValue * fValue);

            aAutoCorrValues[k] = CorrelationCoeff(fSumStart, fSumStart2, fSumEnd, fSumEnd2, aAutoCorrCrossSum[k],
                                                  iStep - k - 1);

        }

        for (k = 0; k < iAutoCorrMaxLen; k++) {
            if (aAutoCorrValues[k] < fAutoCorrCutoff) {
                iLoopIndex = k + 1;
                break;
            }
        }

        iAutoCorrLen = iLoopIndex;

    }

    void InitAutoCorrOne() {
        fAutoCorrOneLast = FLOATZERO;
        fAutoCorrOneSum = FLOATZERO;
        fAutoCorrOneSum2 = FLOATZERO;
        fAutoCorrOneCrossSum = FLOATZERO;
    }

    void UpdateAutoCorrOne() {

        FLOAT fCurValue;

        if (bWeighted) {
            fCurValue = (FLOAT) iSumFalseWeight;
        } else {
            fCurValue = (FLOAT) iNumFalse;
        }

        fAutoCorrOneSum += fCurValue;
        fAutoCorrOneSum2 += (fCurValue * fCurValue);

        if (iStep == 1) {
            fAutoCorrOneStart = fCurValue;
        } else {
            fAutoCorrOneCrossSum += (fCurValue * fAutoCorrOneLast);
        }

        fAutoCorrOneLast = fCurValue;

    }

    void CalcAutoCorrOne() {

        FLOAT fSumStart;
        FLOAT fSumStart2;
        FLOAT fSumEnd;
        FLOAT fSumEnd2;

        if (iStep == 1) {
            fAutoCorrOneVal = FLOATZERO;
            fAutoCorrOneEst = FLOATZERO;
        } else {
            fSumStart = fAutoCorrOneSum - fAutoCorrOneLast;
            fSumStart2 = fAutoCorrOneSum2 - (fAutoCorrOneLast * fAutoCorrOneLast);
            fSumEnd = fAutoCorrOneSum - fAutoCorrOneStart;
            fSumEnd2 = fAutoCorrOneSum2 - (fAutoCorrOneStart * fAutoCorrOneStart);

            fAutoCorrOneVal = CorrelationCoeff(fSumStart, fSumStart2, fSumEnd, fSumEnd2, fAutoCorrOneCrossSum,
                                               iStep - 1);

            if (fAutoCorrOneVal == FLOATZERO) {
                fAutoCorrOneEst = FLOATZERO;
            } else {
                fAutoCorrOneEst = log(fabs(fAutoCorrOneVal));
                if (fAutoCorrOneEst == 1.0f) {
                    fAutoCorrOneEst = FLOATZERO;
                } else {
                    fAutoCorrOneEst = -1 / fAutoCorrOneEst;
                }
            }
        }

    }

    void BranchFactor() {
        UINT32 j;
        fBranchFactor = FLOATZERO;
        for (j = 1; j <= iNumVars; j++) {
            if (aVarScore[j] == 0) {
                fBranchFactor += 1.0f;
            }
        }
        fBranchFactor /= (FLOAT) iNumVars;
    }

    void BranchFactorW() {
        UINT32 j;
        fBranchFactorW = FLOATZERO;
        for (j = 1; j <= iNumVars; j++) {
            if (aVarScoreWeight[j] == 0) {
                fBranchFactorW += 1.0f;
            }
        }
        fBranchFactorW /= (FLOAT) iNumVars;
    }


    UINT32 iSUDSLastNumFalse;
    UBIGINT iSUDSfLastSumFalseWeight;

    void InitStepsUpDownSide() {
        iNumUpSteps = 0;
        iNumDownSteps = 0;
        iNumSideSteps = 0;
        iNumUpStepsW = 0;
        iNumDownStepsW = 0;
        iNumSideStepsW = 0;

        iSUDSLastNumFalse = iNumClauses;
        iSUDSfLastSumFalseWeight = UBIGINTMAX;
    }

    void UpdateStepsUpDownSide() {
        if (iNumFalse < iSUDSLastNumFalse) {
            iNumDownSteps++;
        } else {
            if (iNumFalse > iSUDSLastNumFalse) {
                iNumUpSteps++;
            } else {
                iNumSideSteps++;
            }
        }
        iSUDSLastNumFalse = iNumFalse;

        if (bWeighted) {
            if (iSumFalseWeight < iSUDSfLastSumFalseWeight) {
                iNumDownStepsW++;
            } else {
                if (iSumFalseWeight > iSUDSfLastSumFalseWeight) {
                    iNumUpStepsW++;
                } else {
                    iNumSideStepsW++;
                }
            }
            iSUDSfLastSumFalseWeight = iSumFalseWeight;
        }
    }


    void NumRestarts() {
        if (iStep == 1) {
            iNumRestarts = 0;
        } else {
            iNumRestarts++;
        }
    }


    void LoadKnownSolutions() {

        VARSTATE vsKnownNew;
        FILE *filKnown;
        char *sKnownLine;

        UINT32 iLineLen = iNumVars + 5; // why "+5" ?

        if (strcmp(sFilenameSoln, "")) {
            SetupFile(&filKnown, "r", sFilenameSoln, stdin, 0);

            sKnownLine = (char *) AllocateRAM((iLineLen + 2) * sizeof(char), HeapAdmin);
            vsKnownNew = NewVarState();

            while (!feof(filKnown)) {
                if (fgets(sKnownLine, (int) iLineLen, filKnown)) {
                    if ((*sKnownLine) && (*sKnownLine != '#')) {
                        if (SetCurVarStateString(vsKnownNew, sKnownLine)) {
                            AddToVarStateList(&vslKnownSoln, vsKnownNew);
                            bKnownSolutions = 1;
                        }
                    }
                }
            }
            CloseSingleFile(filKnown);
        } else {
            ReportPrint(pRepErr, "Warning! Expecting [-filesol] to specify file with known solutions\n");
        }
    }

    void CreateSolutionDistance() {
        vsSolutionDistanceCurrent = NewVarState();
    }

    void UpdateSolutionDistance() {
        SetCurVarState(vsSolutionDistanceCurrent);
        vsSolutionDistanceClosest = FindClosestVarState(&vslKnownSoln, vsSolutionDistanceCurrent);
        iSolutionDistance = HammingDistVarState(vsSolutionDistanceClosest, vsSolutionDistanceCurrent);
    }

    void InitFDCRun() {
        fFDCRunHeightDistanceSum = FLOATZERO;
        fFDCRunHeightSum = FLOATZERO;
        fFDCRunHeightSum2 = FLOATZERO;
        fFDCRunDistanceSum = FLOATZERO;
        fFDCRunDistanceSum2 = FLOATZERO;
        iFDCRunCount = 0;
        fFDCRun = FLOATZERO;
    }

    void UpdateFDCRun() {

        FLOAT fDist;
        FLOAT fHeight;

        if (IsLocalMinimum(bWeighted)) {

            fDist = (FLOAT) (iSolutionDistance);

            if (bWeighted) {
                fHeight = (FLOAT) iSumFalseWeight;
            } else {
                fHeight = (FLOAT) iNumFalse;
            }

            fFDCRunHeightDistanceSum += fDist * fHeight;
            fFDCRunHeightSum += fHeight;
            fFDCRunHeightSum2 += fHeight * fHeight;
            fFDCRunDistanceSum += fDist;
            fFDCRunDistanceSum2 += fDist * fDist;
            iFDCRunCount++;

        }
    }

    void CalcFDCRun() {
        fFDCRun = CorrelationCoeff(fFDCRunHeightSum, fFDCRunHeightSum2, fFDCRunDistanceSum, fFDCRunDistanceSum2,
                                   fFDCRunHeightDistanceSum, iFDCRunCount);
    }

    void DynamicParms() {
        ActivateDynamicParms();
    }


    void FlushBuffers() {
        if (bReportFlush) {
            fflush(NULL);
        }
    }

    void CheckWeighted() {
        if (!bWeighted) {
            ReportPrint(pRepErr,
                        "Unexpected Error: some weighted features unavailable for current (unweighted) algorithm\n");
            AbnormalExit();
            exit(1);
        }
    }


    void CreateUniqueSolutions() {
        vsCheckUnique = NewVarState();
    }

    void UpdateUniqueSolutions() {
        if (bSolutionFound) {
            SetCurVarState(vsCheckUnique);
            if (AddUniqueToVarStateList(&vslUnique, vsCheckUnique)) {
                iNumUniqueSolutions++;
                if (iNumUniqueSolutions == iFindUnique) {
                    bTerminateAllRuns = 1;
                }
                iLastUnique = iRun;
            }
        }
    }


    void CreateVarsShareClauses() {

        UINT32 j, k, l, m;
        UINT32 iVar;
        UINT32 iVar2;
        BOOL bAlreadyShareClause;
        LITTYPE *pLit;
        LITTYPE *pLit2;

        aNumVarsShareClause = (UINT32 *) AllocateRAM((iNumVars + 1) * sizeof(UINT32), HeapData);
        pVarsShareClause = (UINT32 **) AllocateRAM((iNumVars + 1) * sizeof(UINT32 *), HeapData);

        memset(aNumVarsShareClause, 0, (iNumVars + 1) * sizeof(UINT32));

        for (j = 0; j < iNumClauses; j++) {
            pLit = pClauseLits[j];
            for (k = 0; k < aClauseLen[j]; k++) {
                iVar = GetVarFromLit(*pLit);
                aNumVarsShareClause[iVar] += (aClauseLen[j] - 1);
                pLit++;
            }
        }

        for (j = 0; j < (iNumVars + 1); j++) {
            pVarsShareClause[j] = (UINT32 *) AllocateRAM(aNumVarsShareClause[j] * sizeof(UINT32), HeapData);;
        }

        memset(aNumVarsShareClause, 0, (iNumVars + 1) * sizeof(UINT32));

        for (j = 0; j < iNumClauses; j++) {
            pLit = pClauseLits[j];
            for (k = 0; k < aClauseLen[j]; k++) {
                iVar = GetVarFromLit(*pLit);
                pLit2 = pClauseLits[j];
                for (l = 0; l < aClauseLen[j]; l++) {
                    iVar2 = GetVarFromLit(*pLit2);
                    if ((l != k) && (iVar != iVar2)) {
                        bAlreadyShareClause = 0;
                        for (m = 0; m < aNumVarsShareClause[iVar]; m++) {
                            if (pVarsShareClause[iVar][m] == iVar2) {
                                bAlreadyShareClause = 1;
                                break;
                            }
                        }
                        if (!bAlreadyShareClause) {
                            pVarsShareClause[iVar][aNumVarsShareClause[iVar]] = iVar2;
                            aNumVarsShareClause[iVar]++;
                        }
                    }
                    pLit2++;
                }
                pLit++;
            }
        }
    }

    /***************************** PX *****************************/

    void InitVIG() {
        using namespace boost;
        UINT32 j, ki, kj;
        LITTYPE *pLit;

        double fStartTime = StartClock();

        iPxNoImprove = fPxNoImproveRatio * iNumVars;

        G = new Graph(iNumVars + 1);
        for (j = 0; j < iNumClauses; j++) {
            pLit = pClauseLits[j];
            for (ki = 0; ki < aClauseLen[j] - 1; ++ki) {
                for (kj = ki + 1; kj < aClauseLen[j]; ++kj) {
                    add_edge(GetVarFromLit(pLit[ki]),
                             GetVarFromLit(pLit[kj]),
                             *G);
                }
            }
        }


        vVarToComponentId = new std::vector<UINT32>(iNumVars + 1);
        vPxVars = new std::vector<UINT32>();
        aTempNumTrueLit = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
        aPseudoBackbone = (BOOL *) AllocateRAM((iNumVars + 1) * sizeof(BOOL), HeapData);
        fPxInitTime = TimeElapsed(fStartTime);
    }


    void CreateSaveEqualBest() {
        aEqualBest = (BOOL *) AllocateRAM((iNumVars + 1) * sizeof(BOOL), HeapData);
    }

    void UpdateSaveEqualBest() {
        /* SaveEqualBest requires BestFalse, which keeps iBestNumFalse up to date.
         * So iNumFalse is never < iBestNumFalse */
        if (iNumFalse == iBestNumFalse) {
            std::copy(aVarValue, aVarValue + iNumVars + 1, aEqualBest);
        }
    }


    void CreateSaveBestInLastNStep() {
        if (iPxId == 0)
            aBestInLastNStep = (BOOL *) AllocateRAM((iNumVars + 1) * sizeof(BOOL), HeapData);

        switch (iPxId) {
            case 0: // bestn X pn
                fxnPx = PxBestnPn;
                fxnUpdateSaveBestInLastNStep = UpdateSaveBestInLastNStepPx0;
                break;
            case 1: // multi bsf
                fxnPx = PxMultiBsf;
                fxnUpdateSaveBestInLastNStep = UpdateSaveBestInLastNStepPx1;
                break;
            default: ReportPrint1(pRepErr, "Unexpected Error: unknown -pxid %"
                    P32
                    "\n", iPxId);
                AbnormalExit();
                exit(1);
        }
    }

    void InitSaveBestInLastNStep() {
        iBestNumFalseInLastNStep = iNumClauses + 1;
        iBestStepInLastNStep = iStep;
        bStuck = false;
        iNumPx = 0;
    }


    /***************************** best(n) X p(n) ***************************cs**/

    /*
     * px0: bestn X pn
     *
     * If there is an improvement over the last N steps, store.
     * If not, check steps since the last improvement:
     *      if > N * pxratio, reset iBestNumFalseInLastNStep to current iNumFalse. Apply px.
     */
    void UpdateSaveBestInLastNStepPx0() {
        if (iNumFalse < iBestNumFalseInLastNStep) {
            // improvement
            iBestNumFalseInLastNStep = iNumFalse;
            iBestStepInLastNStep = iStep;

            if (iNumPx != 0) {
//                printf("copy\n");
                std::copy(aVarValue, aVarValue + iNumVars + 1, aBestInLastNStep);
            }
        } else {
            // no improvement over N steps, and we are not currently consuming Px Vars
            if (iStep > (iBestStepInLastNStep + iPxNoImprove)) {
                if (iNumPx == 0) {
                    for (int j = 1; j <= iNumVars; ++j) {
                        aBestInLastNStep[j] = GetVarStateBit(vsBest, j - 1);
                    }
                }
                // apply px to best(n) and p(n)
                fxnPx();
                iBestNumFalseInLastNStep = iNumFalse;
                iBestStepInLastNStep = iStep;
            }
        }
    }

    /**
     * Find pseudo backbone between bsf and cur sol, and use the result as the new offspring
     *
     * @return true if the two solution are different.
     */
    BOOL FindPseudoBackboneBestnPn() {
        UINT32 j;
        memset(aPseudoBackbone, false, (iNumVars + 1) * sizeof(BOOL));
        UINT32 iNumDiff = 0;

        for (j = 1; j <= iNumVars; ++j) {
            aPseudoBackbone[j] = aVarValue[j] == aBestInLastNStep[j];
            if (!aPseudoBackbone[j]) {
                ++iNumDiff;
            }
        }
        vNumDiffBits.push_back(iNumDiff);
        if (iNumDiff == 0) {
            vNumComp.push_back(0);
            vEvalImpr.push_back(0);
        }

//        printf("numdiff %d\n", iNumDiff);
        return iNumDiff != 0;
    }


    void PxBestnPn() {
//        printf("within run px\n");
        ++iNumPx;
        // find common variable between cur and bsf
        // skip px if parents are the same
        if (!FindPseudoBackboneBestnPn()) return;

        // simplify VIG with fixed variables and find connected components of simplified graph
        // todo: two sat specific optimization, unit propagation and removing SAT clauses
        std::vector<std::vector<UINT32>> vvComponentToVar = DecomposeVigPseudoBackbone();
        vNumComp.push_back(vvComponentToVar.size());

        if (vvComponentToVar.size() == 1) {
            vEvalImpr.push_back(0);
            return;
        }

        // find better of the two parents in each connected components
        FindOffspringCur(vvComponentToVar);
    }

    /***************************** multi bsf *****************************/
    /*
     * px1: multi bsf
     *
     * Within run, when there is no improving move w.r.t. bsf for n*ratio iterations,
     * keep running until hit bsf eval again, apply cur.bsf with all previous collected bsf.
     * Clear all saved bsf on a new/better bsf.
     */
    void UpdateSaveBestInLastNStepPx1() {
        UINT32 j;

        if (!vPxVars->empty()) {
            return;
        }


        if (iNumFalse == iBestNumFalseInLastNStep) {

            // equal bsf
            if (bStuck) {
                // been stuck since last bsf and hitting an equal bsf again AND not consuming px vars,
                // probably in a different plateau
                // todo: this is an O(n) operation that might be too costly
                if (bAllBsf) {
                    boost::dynamic_bitset<> vBsf(iNumVars + 1);
                    for (j = 1; j <= iNumVars; ++j) {
                        vBsf[j] = aVarValue[j];
                    }
                    vvMultiBsf.push_back(vBsf);
                }

                if (!bPxDelay) {
//                    printf("stuck\n");
                    // apply px
                    fxnPx();
                }


                // reset bStuck to make sure collected bsf solutiosn are well separated
                bStuck = false;
                iBestStepInLastNStep = iStep;
            }
        } else if (iNumFalse < iBestNumFalseInLastNStep) {
            // improvement over the last bsf
            iBestNumFalseInLastNStep = iNumFalse;
            iBestStepInLastNStep = iStep;

            // clear all previous bsf
            vvMultiBsf.clear();
            boost::dynamic_bitset<> vBsf(iNumVars + 1);
            for (j = 1; j <= iNumVars; ++j) {
                vBsf[j] = aVarValue[j];
            }
            vvMultiBsf.push_back(vBsf);

            bStuck = false;
        } else {
            if (!bStuck && iStep > (iBestStepInLastNStep + iPxNoImprove)) {
                // no improvement for pxratio * n steps, sets the stuck state
                if (bPxDelay && vvMultiBsf.size() > 1) {
                    // apply px
                    fxnPx();
                }
                bStuck = true;
            }
        }
    }

    /*
     * Find pseudo backbone between the two parents presented by bitset. verified
     */
    UINT32 FindPseudoBackbone(const boost::dynamic_bitset<> &p1, const boost::dynamic_bitset<> &p2) {
        UINT32 j;
        memset(aPseudoBackbone, false, (iNumVars + 1) * sizeof(BOOL));
        UINT32 iNumDiff = 0;

        for (j = 1; j <= iNumVars; ++j) {
            aPseudoBackbone[j] = p1[j] == p2[j];
            if (!aPseudoBackbone[j]) {
                ++iNumDiff;
            }
        }
        return iNumDiff;
    }

    /*
     * Find pseudo backbone between the current solution and p2 presented by bitset. verified
     */
    BOOL FindPseudoBackbone(const boost::dynamic_bitset<> &p2) {
        UINT32 j;
        BOOL bDiff = false;
        memset(aPseudoBackbone, false, (iNumVars + 1) * sizeof(BOOL));
        UINT32 iNumDiff = 0;

        for (j = 1; j <= iNumVars; ++j) {
            aPseudoBackbone[j] = aVarValue[j] == p2[j];
            if (!aPseudoBackbone[j]) {
                bDiff = true;
                ++iNumDiff;
            }
        }
//        printf("iNumDIff %d\n", iNumDiff);
        return bDiff;
    }


    /**
     * Apply the last bsf to all previous until the first
     */
    void PxMultiBsf() {
        UINT32 j;
        BOOL bOneSucc, bAnySucc;

        if (bAllBsf) {
            for (j = 0; j < vvMultiBsf.size() - 1; ++j) {
                ++iNumPx;

                // verified: the order of (p1, p2) makes no difference
                if (!FindPseudoBackbone(vvMultiBsf.back(), vvMultiBsf[j])) continue;

                // simplify VIG with fixed variables and find connected components of simplified graph
                // todo: two sat specific optimization, unit propagation and removing SAT clauses
                std::vector<std::vector<UINT32>> vvComponentToVar = DecomposeVigPseudoBackbone();

                if (vvComponentToVar.size() == 1) return;

                if (bPxDelay) {
                    // find better of the two parents in each connected components
                    // use the last best solution since it is closer to current solution,
                    // thus it is cheaper to update
                    FindOffspring(vvMultiBsf.back(), iBestNumFalse, vvComponentToVar, true);
                } else {
                    // short for component score evaluation when using current solution
                    // as one of the parents
                    FindOffspringCur(vvComponentToVar);
                }

                // return on the first improving offspring
                if (!vPxVars->empty()) return;
            }
        } else {
            ++iNumPx;
            if (!FindPseudoBackbone(vvMultiBsf[0])) return;

            // simplify VIG with fixed variables and find connected components of simplified graph
            // todo: two sat specific optimization, unit propagation and removing SAT clauses
            std::vector<std::vector<UINT32>> vvComponentToVar = DecomposeVigPseudoBackbone();

            if (vvComponentToVar.size() == 1) return;

            if (bPxDelay) {
                // find better of the two parents in each connected components
                // use the last best solution since it is closer to current solution,
                // thus it is cheaper to update
                FindOffspring(vvMultiBsf.back(), iBestNumFalse, vvComponentToVar, true);
            } else {
                // short for component score evaluation when using current solution
                // as one of the parents
                FindOffspringCur(vvComponentToVar);
            }
        }
    }

    /**
     * Simplfy VIG using pseudo backbone
     */
    std::vector<std::vector<UINT32>> DecomposeVigPseudoBackbone() {
        using namespace boost;
        Vertex_Filter<Graph> filter;
        UINT32 i;
        FilteredGraphType filteredGraph(*G, keep_all(), filter); // (graph, EdgePredicate, VertexPredicate)

        std::fill(vVarToComponentId->begin(), vVarToComponentId->end(), 0);
        int num = connected_components(filteredGraph, &(*vVarToComponentId)[0]) - 1;

        std::vector<std::vector<UINT32>> vvComponentToVar(num);
        if (num) {
            for (i = 1; i <= iNumVars; ++i) {
                if ((*vVarToComponentId)[i]) {
                    vvComponentToVar[(*vVarToComponentId)[i] - 1].push_back(i);
                }
            }
        }
        return vvComponentToVar;
    }

    /***************************** Across Run *****************************/

    /**
     * Find pseudo backbone between bsf and the solution given as bitset, and use the result as the new offspring
     *
     * @return true if the two solution are different.
     */
    BOOL FindPseudoBackboneBsf(const boost::dynamic_bitset<> &p2) {
        UINT32 j;
        BOOL bDiff = false;
        memset(aPseudoBackbone, false, (iNumVars + 1) * sizeof(BOOL));
        UINT32 iNumDiff = 0;

        for (j = 1; j <= iNumVars; ++j) {
            aPseudoBackbone[j] = GetVarStateBit(vsBest, j - 1) == p2[j];
            if (!aPseudoBackbone[j]) {
                bDiff = true;
                ++iNumDiff;
            }
        }
        printf("num diff %d\n", iNumDiff);
        return bDiff;
    }


    /**
     * Apply px to bsf of the current run to the bsf of all previous runs at PostRun stage
     */
    void AcrossRunPx() {
        UINT32 j;
        UINT32 iOffspringEval;
        UINT32 iBestIdx;
        boost::dynamic_bitset<> vBsf(iNumVars + 1);

        printf("before across run px, best eval %d at step %d\n", iBestNumFalse, iBestStepNumFalse);

        std::vector<std::vector<UINT32>> vvPxVars;
        std::vector<UINT32> vOffspringEval;

        for (j = 1; j <= iNumVars; ++j) {
            vBsf[j] = GetVarStateBit(vsBest, j - 1);
        }

        for (j = 0; j < vvAcrossRunBsf.size(); ++j) {
            printf("\nacross run px with run %d\n", j + 1);
            if (!FindPseudoBackbone(vBsf, vvAcrossRunBsf[j])) {
                printf("same solution, skip\n");
                continue;
            }

            // simplify VIG with fixed variables and find connected components of simplified graph
            // todo: two sat specific optimization, unit propagation
            std::vector<std::vector<UINT32>> vvComponentToVar = DecomposeVigPseudoBackbone();

            if (vvComponentToVar.size() == 1) {
                printf("single component, skip\n");
                continue;
            }

            iOffspringEval = FindOffspring(vBsf, iBestNumFalse, vvComponentToVar, false);

            if (!vPxVars->empty()) {
                printf("across run px succ\n");
                vvPxVars.push_back(*vPxVars);
                vOffspringEval.push_back(iOffspringEval);
            }
        }

        // vvPxVars verified
//        printf("vvPxVars size\n");
//        for (j = 0; j < vvPxVars.size(); ++j){
//            printf("%d\n", vvPxVars[j].size());
//        }

        if (!vOffspringEval.empty()) {

            // find the best offspring (with min evaluation) and make the moves
            iBestIdx = std::distance(
                    vOffspringEval.begin(),
                    std::min_element(
                            vOffspringEval.begin(),
                            vOffspringEval.end()
                    )
            );

            printf("best offspring eval %d\n", vOffspringEval[iBestIdx]);

            // make the flips
            for (auto it = vvPxVars[iBestIdx].begin(); it != vvPxVars[iBestIdx].end(); ++it) {
                vBsf[*it].flip();
            }

            VerifySol(vBsf, vOffspringEval[iBestIdx]);

            // set best eval
            iBestNumFalse = vOffspringEval[iBestIdx];
            iNumFalse = iBestNumFalse;


            // set best sol
            SetCurVarStateBitset(vsBest, vBsf);

            iStep += vvPxVars[iBestIdx].size();
            iBestStepNumFalse = iStep;

            // update output column
            RunProcedures(StepCalculations);

            // flip back
            for (auto it = vvPxVars[iBestIdx].begin(); it != vvPxVars[iBestIdx].end(); ++it) {
                vBsf[*it].flip();
            }
        }

        // clear the generated px vars
        vPxVars->clear();

        vvAcrossRunBsf.push_back(vBsf);
    }

    /***************************** Component Score *****************************/

    /**
     * Compute full evaluation using naive enumeration give bitset
     *
     * @param vSol
     * @return
     */
    UINT32 NaiveComputeEvaluation(boost::dynamic_bitset<> &vSol) {
        UINT32 iEval = 0;
        UINT32 j, k;
        LITTYPE litCur;
        UINT32 *pClause;

        memset(aTempNumTrueLit, 0, iNumClauses * sizeof(UINT32));

        // count the number of true literals in each clause
        for (j = 1; j <= iNumVars; ++j) {
            litCur = GetTrueLitVarValue(j, vSol);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; ++k) {
                ++aTempNumTrueLit[*pClause];
                ++pClause;
            }
        }

        // go through every clause to check whether it is satisfied
        for (j = 0; j < iNumClauses; ++j) {
            if (!aTempNumTrueLit[j]) {
                ++iEval;
            }
        }

        return iEval;
    }


    /**
     * Compute full evaluation using naive enumeration give bitset (weighted version)
     *
     * @param vSol
     * @return
     */
    UBIGINT NaiveComputeEvaluationW(boost::dynamic_bitset<> &vSol) {
        UBIGINT iEval = 0;
        UINT32 j, k;
        LITTYPE litCur;
        UINT32 *pClause;

        memset(aTempNumTrueLit, 0, iNumClauses * sizeof(UINT32));

        // count the number of true literals in each clause
        for (j = 1; j <= iNumVars; ++j) {
            litCur = GetTrueLitVarValue(j, vSol);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; ++k) {
                ++aTempNumTrueLit[*pClause];
                ++pClause;
            }
        }

        // go through every clause to check whether it is satisfied
        for (j = 0; j < iNumClauses; ++j) {
            if (!aTempNumTrueLit[j]) {
                iEval += aClauseWeight[j];
            }
        }

        return iEval;
    }


    /**
     * Compute full evaluation using naive enumeration give array pointer
     *
     * @param aSol
     * @return
     */
    UINT32 NaiveComputeEvaluation(BOOL *aSol) {
        UINT32 iEval = 0;
        UINT32 j, k;
        LITTYPE litCur;
        UINT32 *pClause;

        memset(aTempNumTrueLit, 0, iNumClauses * sizeof(UINT32));

        // count the number of true literals in each clause
        for (j = 1; j <= iNumVars; ++j) {
            litCur = GetTrueLitVarValue(j, aSol);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; ++k) {
                ++aTempNumTrueLit[*pClause];
                ++pClause;
            }
        }

        // go through every clause to check whether it is satisfied
        for (j = 0; j < iNumClauses; ++j) {
            if (!aTempNumTrueLit[j]) {
                printf("unsat clause %d\n", j);
                ++iEval;
            }
        }

        return iEval;
    }


    /**
     * Compute the different in evaluation using naive approach
     */
    SINT32 NaiveComputeCompScore(const std::vector<UINT32> &vComponentToVar) {
        UINT32 old_eval = iNumFalse, new_eval = 0;
        UINT32 j, k, iVar;
        LITTYPE litCur;
        UINT32 *pClause;

        // flip all variables in the component
        for (k = 0; k < vComponentToVar.size(); ++k) {
            iVar = vComponentToVar[k];
            aVarValue[iVar] = !aVarValue[iVar];
        }

        memset(aTempNumTrueLit, 0, iNumClauses * sizeof(UINT32));

        // count the number of true literals in each clause
        for (j = 1; j <= iNumVars; ++j) {
            litCur = GetTrueLit(j);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; ++k) {
                ++aTempNumTrueLit[*pClause];
                ++pClause;
            }
        }

        // go through every clause to check whether it is satisfied
        for (j = 0; j < iNumClauses; ++j) {
            if (!aTempNumTrueLit[j]) {
                ++new_eval;
            }
        }

        // revert the flips
        for (k = 0; k < vComponentToVar.size(); ++k) {
            iVar = vComponentToVar[k];
            aVarValue[iVar] = !aVarValue[iVar];
        }

        return old_eval - new_eval;
    }

    /**
     * Compute the different in evaluation using naive approach (weighted version)
     */
    SBIGINT NaiveComputeCompScoreW(const std::vector<UINT32> &vComponentToVar) {
        UBIGINT old_eval = iSumFalseWeight, new_eval = 0;
        UINT32 j, k, iVar;
        LITTYPE litCur;
        UINT32 *pClause;

        // flip all variables in the component
        for (k = 0; k < vComponentToVar.size(); ++k) {
            iVar = vComponentToVar[k];
            aVarValue[iVar] = !aVarValue[iVar];
        }

        memset(aTempNumTrueLit, 0, iNumClauses * sizeof(UINT32));

        // count the number of true literals in each clause
        for (j = 1; j <= iNumVars; ++j) {
            litCur = GetTrueLit(j);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; ++k) {
                ++aTempNumTrueLit[*pClause];
                ++pClause;
            }
        }

        // go through every clause to check whether it is satisfied
        for (j = 0; j < iNumClauses; ++j) {
            if (!aTempNumTrueLit[j]) {
//                printf("unsat clause weight %ld\n", aClauseWeight[j]);
                new_eval += aClauseWeight[j];
            }
        }

        // revert the flips
        for (k = 0; k < vComponentToVar.size(); ++k) {
            iVar = vComponentToVar[k];
            aVarValue[iVar] = !aVarValue[iVar];
        }

//        printf("old eval %ld, new eval %ld\n", old_eval, new_eval);
        return old_eval - new_eval;
    }


    /**
     * Compute the different in evaluation between p1 and p2 using naive approach
     * S(\pi_i) = g^i(p1) - g^i(p2)
     *
     * @param vP1
     * @param iP1Eval
     * @param vComponentToVar
     * @return
     */
    SINT32
    NaiveComputeCompScore(boost::dynamic_bitset<> &vP1, UINT32 iP1Eval,
                          const std::vector<UINT32> &vComponentToVar) {
        UINT32 new_eval = 0;
        UINT32 j, k, iVar;
        LITTYPE litCur;
        UINT32 *pClause;

        // flip all variables in the component, verified
        for (k = 0; k < vComponentToVar.size(); ++k) {
            iVar = vComponentToVar[k];
            vP1[iVar].flip();
//            printf("flip %d to %d\n", iVar, (bool) vP1[iVar]);
        }

//        for (j = 1; j <= iNumVars; ++j){
//            printf("%d:%d ", j, (bool) vP1[j]);
//        }
//        printf("\n");

        memset(aTempNumTrueLit, 0, iNumClauses * sizeof(UINT32));

        // count the number of true literals in each clause
        for (j = 1; j <= iNumVars; ++j) {
            litCur = GetTrueLitVarValue(j, vP1);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; ++k) {
                ++aTempNumTrueLit[*pClause];
                ++pClause;
            }
        }

        // go through every clause to check whether it is satisfied
        for (j = 0; j < iNumClauses; ++j) {
            if (!aTempNumTrueLit[j]) {
                ++new_eval;
            }
        }

        // revert the flips, verified
        for (k = 0; k < vComponentToVar.size(); ++k) {
            iVar = vComponentToVar[k];
            vP1[iVar].flip();
        }

        return iP1Eval - new_eval;
    }


    /**
     * Compute Component Score by focusing on clauses that involves component variables
     */
    SINT32 ComputeCompScoreFocused(const std::vector<UINT32> &vComponentToVar) {
        UINT32 old_eval = iNumFalse, new_eval = 0;
        UINT32 j, k, iVar;
        LITTYPE litCur;
        UINT32 *pClause;

        std::copy(aNumTrueLit, aNumTrueLit + iNumClauses, aTempNumTrueLit);

        for (j = 0; j < vComponentToVar.size(); ++j) {
            iVar = vComponentToVar[j];

            litCur = GetTrueLit(iVar);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; ++k) {
                --aTempNumTrueLit[*pClause];
                ++pClause;
            }

            litCur = GetFalseLit(iVar);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; ++k) {
                ++aTempNumTrueLit[*pClause];
                ++pClause;
            }
        }

        // go through affected clause to check whether it is satisfied
        for (j = 0; j < iNumClauses; ++j) {
            if (!aTempNumTrueLit[j]) {
                ++new_eval;
            }
        }

        return old_eval - new_eval;
    }

    /**
     * Compute Component Score by multiple default flip
     */
    SINT32 ComputeCompScoreDefaultFlip(const std::vector<UINT32> &vComponentToVar) {
        UINT32 old_eval = iNumFalse, new_eval = iNumFalse;
        UINT32 j, k, iVar;
        LITTYPE litCur;
        UINT32 *pClause;

        std::copy(aNumTrueLit, aNumTrueLit + iNumClauses, aTempNumTrueLit);

        for (j = 0; j < vComponentToVar.size(); ++j) {
            iVar = vComponentToVar[j];

            litCur = GetTrueLit(iVar);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; ++k) {
                if (--aTempNumTrueLit[*pClause] == 0) { // the only true lit in the clause
                    new_eval++;
                }
                ++pClause;
            }

            litCur = GetFalseLit(iVar);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; ++k) {
                if (++aTempNumTrueLit[*pClause] == 1) { // previously zero true lit in the clause
                    new_eval--;
                }
                ++pClause;
            }
        }

        return old_eval - new_eval;
    }

    /**
     * Compute Component Score by multiple default flip (weighted version)
     */
    SBIGINT ComputeCompScoreDefaultFlipW(const std::vector<UINT32> &vComponentToVar) {
//        printf("ComputeCompScoreDefaultFlipW\n");
        SBIGINT diff_eval = 0;
//        old_eval = iSumFalseWeight, new_eval = iSumFalseWeight;
        UINT32 j, k, iVar;
        LITTYPE litCur;
        UINT32 *pClause;

        std::copy(aNumTrueLit, aNumTrueLit + iNumClauses, aTempNumTrueLit);

        for (j = 0; j < vComponentToVar.size(); ++j) {
            iVar = vComponentToVar[j];

            litCur = GetTrueLit(iVar);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; ++k) {
                if (--aTempNumTrueLit[*pClause] == 0) { // the only true lit in the clause
//                    printf("add %ld\n",aClauseWeight[*pClause]);
                    diff_eval -= aClauseWeight[*pClause];
                }
                ++pClause;
            }

            litCur = GetFalseLit(iVar);
            pClause = pLitClause[litCur];
            for (k = 0; k < aNumLitOcc[litCur]; ++k) {
                if (++aTempNumTrueLit[*pClause] == 1) { // previously zero true lit in the clause
//                    printf("minus %ld\n", aClauseWeight[*pClause]);
                    diff_eval += aClauseWeight[*pClause];
                }
                ++pClause;
            }
        }

        return diff_eval;
    }


    //todo: move this function after UpdateCompVars()
    void UpdateCompVarsSparrow(const std::vector<UINT32> &vComponentToVar) {
        UINT32 j;
        UINT32 k;
        UINT32 l;
        UINT32 *pClause;
        UINT32 iVar, iTempFlipCandidate;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        SINT32 iPenalty;

        for (l = 0; l < vComponentToVar.size(); ++l) {
            iTempFlipCandidate = vComponentToVar[l];

            litWasTrue = GetTrueLit(iTempFlipCandidate);
            litWasFalse = GetFalseLit(iTempFlipCandidate);

            aVarValue[iTempFlipCandidate] = !aVarValue[iTempFlipCandidate];

            pClause = pLitClause[litWasTrue];
            for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
                iPenalty = aClausePenaltyINT[*pClause];
                aNumTrueLit[*pClause]--;
                if (aNumTrueLit[*pClause] == 0) {
                    ++iNumFalse;
                    aSparrowScore[iTempFlipCandidate] -= iPenalty;
                    pLit = pClauseLits[*pClause];
                    for (k = 0; k < aClauseLen[*pClause]; k++) {
                        iVar = GetVarFromLit(*pLit);
                        aSparrowScore[iVar] -= iPenalty;

                        pLit++;

                    }
                }
                if (aNumTrueLit[*pClause] == 1) {
                    pLit = pClauseLits[*pClause];
                    for (k = 0; k < aClauseLen[*pClause]; k++) {
                        if (IsLitTrue(*pLit)) {
                            iVar = GetVarFromLit(*pLit);
                            aSparrowScore[iVar] += iPenalty;
                            aCritSat[*pClause] = iVar;
                            break;
                        }
                        pLit++;
                    }
                }
                pClause++;
            }

            pClause = pLitClause[litWasFalse];
            for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
                iPenalty = aClausePenaltyINT[*pClause];
                aNumTrueLit[*pClause]++;
                if (aNumTrueLit[*pClause] == 1) {
                    --iNumFalse;

                    pLit = pClauseLits[*pClause];
                    for (k = 0; k < aClauseLen[*pClause]; k++) {
                        iVar = GetVarFromLit(*pLit);
                        aSparrowScore[iVar] += iPenalty;

                        pLit++;

                    }
                    aSparrowScore[iTempFlipCandidate] += iPenalty;
                    aCritSat[*pClause] = iTempFlipCandidate;
                }
                if (aNumTrueLit[*pClause] == 2) {
                    aSparrowScore[aCritSat[*pClause]] -= iPenalty;
                }
                pClause++;
            }
        }
    }


    /**
     * Compute Component Score by considering component change as multiple flips
     */
    SINT32 ComputeCompScoreMultiFlip(const std::vector<UINT32> &vComponentToVar) {
        UINT32 old_eval = iNumFalse;
        UINT32 new_eval;

        // make the flip
        if (!strcmp(sAlgName, "sparrow"))
            UpdateCompVarsSparrow(vComponentToVar);
        else
            UpdateCompVars(vComponentToVar);
        // save evaluation
        new_eval = iNumFalse;
        // revert the flip
        if (!strcmp(sAlgName, "sparrow"))
            UpdateCompVarsSparrow(vComponentToVar);
        else
            UpdateCompVars(vComponentToVar);

        return old_eval - new_eval;
    }

    /**
     * Flip and update every variable in vComponentToVar
     *
     * Initially developed for adaptg2wsat, following FlipTrackChangesFCL
     *
     * @param vComponentToVar
     */
    void UpdateCompVars(const std::vector<UINT32> &vComponentToVar) {
        UINT32 j;
        UINT32 k;
        UINT32 l;
        UINT32 *pClause;
        UINT32 iVar, iTempFlipCandidate;
        LITTYPE litWasTrue;
        LITTYPE litWasFalse;
        LITTYPE *pLit;

        for (l = 0; l < vComponentToVar.size(); ++l) {
            iTempFlipCandidate = vComponentToVar[l];

            litWasTrue = GetTrueLit(iTempFlipCandidate);
            litWasFalse = GetFalseLit(iTempFlipCandidate);

            aVarValue[iTempFlipCandidate] = !aVarValue[iTempFlipCandidate];

            pClause = pLitClause[litWasTrue];
            for (j = 0; j < aNumLitOcc[litWasTrue]; j++) {
                aNumTrueLit[*pClause]--;
                if (aNumTrueLit[*pClause] == 0) {
                    ++iNumFalse;
                    aVarScore[iTempFlipCandidate]--;
                    pLit = pClauseLits[*pClause];
                    for (k = 0; k < aClauseLen[*pClause]; k++) {
                        iVar = GetVarFromLit(*pLit);
                        aVarScore[iVar]--;
                        pLit++;
                    }
                }
                if (aNumTrueLit[*pClause] == 1) {
                    pLit = pClauseLits[*pClause];
                    for (k = 0; k < aClauseLen[*pClause]; k++) {
                        if (IsLitTrue(*pLit)) {
                            iVar = GetVarFromLit(*pLit);
                            aVarScore[iVar]++;
                            aCritSat[*pClause] = iVar;
                            break;
                        }
                        pLit++;
                    }
                }
                pClause++;
            }

            pClause = pLitClause[litWasFalse];
            for (j = 0; j < aNumLitOcc[litWasFalse]; j++) {
                aNumTrueLit[*pClause]++;
                if (aNumTrueLit[*pClause] == 1) {
                    --iNumFalse;

                    pLit = pClauseLits[*pClause];
                    for (k = 0; k < aClauseLen[*pClause]; k++) {
                        iVar = GetVarFromLit(*pLit);
                        aVarScore[iVar]++;
                        pLit++;
                    }
                    aVarScore[iTempFlipCandidate]++;
                    aCritSat[*pClause] = iTempFlipCandidate;
                }
                if (aNumTrueLit[*pClause] == 2) {
                    iVar = aCritSat[*pClause];
                    aVarScore[iVar]--;
                }
                pClause++;
            }
        }
    }



    /***************************** Find OffSpring *****************************/

    /**
     * Find Offpring better current solution and the parent
     * that can be obtained by flipping vvComponentToVar in current solution
     *
     * @param vvComponentToVar
     */
    void FindOffspringCur(const std::vector<std::vector<UINT32>> &vvComponentToVar) {
        BOOL bAnyWorse = false; // there is any worse component in bsf
        UINT32 iImprEval = 0;
        vPxVars->clear();
        for (UINT32 j = 0; j < vvComponentToVar.size(); ++j) {
//            iCompScore = NaiveComputeCompScore(vvComponentToVar[j]);
//            iCompScore = ComputeCompScoreFocused(vvComponentToVar[j]);
//            iCompScore = ComputeCompScoreDefaultFlip(vvComponentToVar[j]);
//            iCompScore = ComputeCompScoreMultiFlip(vvComponentToVar[j]);

            if (bWeighted) {
                SBIGINT iCompScore = ComputeCompScoreDefaultFlipW(vvComponentToVar[j]);
                /* printf("expect %ld, got %ld, score %ld\n", */
                /*        NaiveComputeCompScoreW(vvComponentToVar[j]), */
                /*        ComputeCompScoreDefaultFlipW(vvComponentToVar[j]), */
                /*        iCompScore */
                /* ); */
//                if (NaiveComputeCompScoreW(vvComponentToVar[j]) != iCompScore) {
//                    printf("conflict weighted\n");
//                    exit(-1);
//                }
                if (iCompScore > 0) {
                    std::move(vvComponentToVar[j].begin(), vvComponentToVar[j].end(), std::back_inserter(*vPxVars));
                } else if (iCompScore < 0) {
                    bAnyWorse = true;
                    iImprEval -= iCompScore;
                }
            } else {
                SINT32 iCompScore = ComputeCompScoreDefaultFlip(vvComponentToVar[j]);
//                if (NaiveComputeCompScore(vvComponentToVar[j]) != ComputeCompScoreDefaultFlip(vvComponentToVar[j])) {
//                    printf("conflict\n");
//                    exit(-1);
//                }
                if (iCompScore > 0) {
                    std::move(vvComponentToVar[j].begin(), vvComponentToVar[j].end(), std::back_inserter(*vPxVars));
                } else if (iCompScore < 0) {
                    bAnyWorse = true;
                    iImprEval -= iCompScore;
                }
            }


//            printf("comp id %d, size %d, score %d\n", j + 1, vvComponentToVar[j].size(), iCompScore);
//            Print1dVector(vvComponentToVar[j]);
        }

        if (!bAnyWorse) { // all better, abondon the px varaibles
            vPxVars->clear();
        } else {
            ++iNumPxSucc;
            iSumNumComp += vvComponentToVar.size();
            std::sort(vPxVars->begin(), vPxVars->end());
            std::random_shuffle(vPxVars->begin(), vPxVars->end());
        }
        vEvalImpr.push_back(iImprEval);
//        printf("istep %d, vPxVars size %d,  #comp %d, bsf %d, bsf step %d, cur %d, impr eval %d\n",
//               iStep, vPxVars->size(), vvComponentToVar.size(), iBestNumFalse, iBestStepNumFalse, iNumFalse,
//               iImprEval);
//        printf("\n");
    }

    /**
     * Find the px offspring of two the parents given the decomposition.
     * Px application is succussful iff there are both positve and negative component scores.
     * p2 can be deduced from p1 and vvComponentToVar.
     *
     * @param vP1
     * @param vvComponentToVar
     * @return the evaluation of offspring
     */
    UINT32 FindOffspring(boost::dynamic_bitset<> &vP1, UINT32 iP1Eval,
                         const std::vector<std::vector<UINT32>> &vvComponentToVar, BOOL bReconPxVars) {
        SINT32 iCompScore;
        UINT32 j;
        BOOL bPosScore = false, bNegScore = false; // there is any postive/negative score going from vP1 to p2
        UINT32 iImprEval = 0; // improved evaluation wrt P1

        vPxVars->clear();

        for (j = 0; j < vvComponentToVar.size(); ++j) {
            iCompScore = NaiveComputeCompScore(vP1, iP1Eval, vvComponentToVar[j]);

            if (iCompScore > 0) {
                // p1 is worse than p2, f(p1) > f(p2)
                bPosScore = true;
                std::move(vvComponentToVar[j].begin(), vvComponentToVar[j].end(), std::back_inserter(*vPxVars));
                iImprEval += iCompScore;
            } else if (iCompScore < 0) {
                // p1 is better than p2, f(p1) < f(p2)
                bNegScore = true;
            }
//            printf("comp id %d, size %d, score %d\n", j + 1, vvComponentToVar[j].size(), iCompScore);
//            Print1dVector(vvComponentToVar[j]);
        }
        if (!bPosScore || !bNegScore) {
            // if there is only a single kind of vote
            vPxVars->clear();
        } else {
            ++iNumPxSucc;
            iSumNumComp += vvComponentToVar.size();
        }

        if (!vPxVars->empty() && bReconPxVars) {
            // vPxVars reconstruction with respect to current solution, induce sorting on vPxVars
            boost::dynamic_bitset<> vOffspring(vP1);

            while (!vPxVars->empty()) {
                vOffspring[vPxVars->back()].flip();
                vPxVars->pop_back();
            }

            for (j = 1; j <= iNumVars; ++j) {
                if ((BOOL) vOffspring[j] != aVarValue[j]) {
                    vPxVars->push_back(j);
                }
            }

            std::random_shuffle(vPxVars->begin(), vPxVars->end());
        }

//        printf("istep %d, vPxVars size %d,  #comp %d, bsf %d, bsf step %d, cur %d, impr eval %d, offspring eval %d\n",
//               iStep, vPxVars->size(), vvComponentToVar.size(), iBestNumFalse, iBestStepNumFalse, iNumFalse,
//               iImprEval, iP1Eval - iImprEval);

        return iP1Eval - iImprEval;
    }

    /**
     * Verify the solution has the given evaluation.
     *
     * If the evaluations don't match, report the error and exit the program if not
     * @param vSol
     * @param iEval
     */
    void VerifySol(boost::dynamic_bitset<> &vSol, UINT32 iEval) {
        if (!aTempNumTrueLit) {
            aTempNumTrueLit = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
        }
        UINT32 iGotEval;

        iGotEval = NaiveComputeEvaluation(vSol);

        if (iEval != iGotEval) {
            printf("error in evaluation: expect %d, got %d\n", iEval, iGotEval);
            AbnormalExit();
            exit(1);
        }
    }


    /**
     * Verify the best solution.
     *
     * If the evaluations don't match, report the error and exit the program if not
     */
    void VerifyVsBest() {
        if (bNoVerifySol) return;
        if (!aTempNumTrueLit) {
            aTempNumTrueLit = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
        }

        boost::dynamic_bitset<> vSol(iNumVars + 1);
        UINT32 j;

        for (j = 1; j <= iNumVars; ++j) {
            vSol[j] = GetVarStateBit(vsBest, j - 1);
        }

        if (bWeighted) {
//            printf("VerifyVsBest weighted\n");
//            printf("weighted evaluation: best %"PRIu64", current %"PRIu64"\n", iBestSumFalseWeight, iSumFalseWeight);
            UBIGINT iGotEval = NaiveComputeEvaluationW(vSol);
            if (iBestSumFalseWeight != iGotEval) {
                printf("error in weighted evaluation: expect %"PRIu64", got %"PRIu64"\n", iBestSumFalseWeight, iGotEval);
                AbnormalExit();
                exit(1);
            }
        } else {
//            printf("VerifyVsBest unweighted\n");
            UINT32 iGotEval = NaiveComputeEvaluation(vSol);
            if (iBestNumFalse != iGotEval) {
                printf("error in unweighted evaluation: expect %d, got %d\n", iBestNumFalse, iGotEval);
                AbnormalExit();
                exit(1);
            }
        }
    }

    /**
     * Verify the current solution.
     *
     * If the evaluations don't match, report the error and exit the program if not
     */
    void VerifyCurrent() {
        if (bNoVerifySol) return;
        if (!aTempNumTrueLit) {
            aTempNumTrueLit = (UINT32 *) AllocateRAM(iNumClauses * sizeof(UINT32), HeapData);
        }

        boost::dynamic_bitset<> vSol(iNumVars + 1);
        UINT32 j;

        for (j = 1; j <= iNumVars; ++j) {
            vSol[j] = aVarValue[j];
        }

        if (bWeighted) {
//            printf("VerifyVsBest weighted\n");
            UBIGINT iGotEval = NaiveComputeEvaluationW(vSol);
            if (iSumFalseWeight != iGotEval) {
                printf("error in weighted evaluation: expect %ld, got %ld\n", iSumFalseWeight, iGotEval);
                AbnormalExit();
                exit(1);
            }
        } else {
//            printf("VerifyVsBest unweighted\n");
            UINT32 iGotEval = NaiveComputeEvaluation(vSol);
            if (iBestNumFalse != iGotEval) {
                printf("error in unweighted evaluation: expect %d, got %d\n", iBestNumFalse, iGotEval);
                AbnormalExit();
                exit(1);
            }
        }
    }



    void SetMemLim() {
        // Set limit on virtual memory:
        if (iMemLim != 0) {
            rlim_t new_mem_lim = (rlim_t) iMemLim * 1024 * 1024;
            rlimit rl;
            getrlimit(RLIMIT_AS, &rl);
            if (rl.rlim_max == RLIM_INFINITY || new_mem_lim < rl.rlim_max) {
                rl.rlim_cur = new_mem_lim;
                if (setrlimit(RLIMIT_AS, &rl) == -1)
                    printf("c WARNING! Could not set resource limit: Virtual memory.\n");
            }
        }

    }

    /**
     * Load the best solutions from files, and apply px to every pair of solutions
     */
    void LoadBestSolutions() {
        FILE *fp;
        char *sKnownLine = NULL;
        size_t iLineLen = 0;
        char *token, *str, *saveptr;
        UINT32 j;

        if (strcmp(sFilenamePxSoln, "")) {
            // if sFilenamePxSoln is non-empty
            SetupFile(&fp, "r", sFilenamePxSoln, stdin, 0);

            while ((getline(&sKnownLine, &iLineLen, fp)) != -1) {
                if ((*sKnownLine) && (*sKnownLine != '#')) {
                    // skip comment lines (start with #), and empty lines
                    for (j = 1, str = sKnownLine;; j++, str = NULL) {
                        token = strtok_r(str, " \t\r\n", &saveptr);

                        if (token == NULL) break; // end of file

                        if (j == 3) { // 3th column stores evaluation
                            vBestEvals.push_back(atoi(token));

                        } else if (j == 4) { // 4th column stores vararray
                            // check validity of given var array
                            if (strlen(token) != iNumVars) {
                                printf("Error! Ignoring variable state string: "
                                               "length %d differs from number of variables %d\n",
                                       strlen(token), iNumVars);
                                exit(1);
                            }

                            boost::dynamic_bitset<> vBestSol(iNumVars + 1);
                            for (j = 1; j <= iNumVars; j++) {
                                if (token[j - 1] == '1') {
                                    vBestSol[j] = true;
                                } else if (token[j - 1] == '0') {
                                    vBestSol[j] = false;
                                } else {
                                    ReportPrint1(pRepErr,
                                                 "Error! Ignoring variable state string: (invalid input) %s \n",
                                                 token);
                                    exit(1);
                                }
                            }
                            vvBestSols.push_back(vBestSol);
                        }
                    }
                }
            }
            free(sKnownLine);
            CloseSingleFile(fp);
            PxBestSols();
        } else {
            ReportPrint(pRepErr, "Warning! Expecting [-pxsol] to specify file with known solutions\n");
            exit(1);
        }
    }

    /**
     * Apply px to all
     */
    void PxBestSols() {
        UINT32 j, k;
        UINT32 iOffspringEval;
        UINT32 iHamDist;

        // print header
        printf("run1\trun2\tdist\tcomp\toffspring\n");

        for (j = 0; j < vvBestSols.size() - 1; ++j) {
            for (k = j + 1; k < vvBestSols.size(); ++k) {
                iHamDist = FindPseudoBackbone(vvBestSols[j], vvBestSols[k]);
                if (!iHamDist) {
                    continue;
                }

                // simplify VIG with fixed variables and find connected components of simplified graph
                // todo: two sat specific optimization, unit propagation and removing SAT clauses
                std::vector<std::vector<UINT32>> vvComponentToVar = DecomposeVigPseudoBackbone();

                if (vvComponentToVar.size() == 1) { // skip single component
                    continue;
                }

                iOffspringEval = FindOffspring(vvBestSols[j], vBestEvals[j], vvComponentToVar, false);

                printf("%d\t%d\t%d\t%d\t%d\n", j + 1, k + 1, iHamDist, vvComponentToVar.size(), iOffspringEval);
            }
        }
    }

#ifdef __cplusplus

}
#endif
