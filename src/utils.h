//
// Created by chenwx on 1/17/17.
//

#ifndef UBCSAT_UTILS_H
#define UBCSAT_UTILS_H

template<typename T>
void Print2dVector(std::vector<std::vector<T>> vv) {
    for (auto v = vv.begin(); v != vv.end(); ++v) {
        for (auto i = v->begin(); i != v->end(); ++i) {
            std::cout << *i << ' ';
        }
        std::cout << std::endl;
    }
}

template<typename T>
void Print1dVector(std::vector<T> v, bool idx = false) {
    for (auto i = v.begin(); i != v.end(); ++i) {
        if (!idx) std::cout << *i << ' ';
//        else std::cout << *i + 1 << ' ';
    }

    std::cout << std::endl;
}

#endif //UBCSAT_UTILS_H
