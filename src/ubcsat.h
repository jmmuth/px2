/*

      ##  ##  #####    #####   $$$$$   $$$$   $$$$$$    
      ##  ##  ##  ##  ##      $$      $$  $$    $$      
      ##  ##  #####   ##       $$$$   $$$$$$    $$      
      ##  ##  ##  ##  ##          $$  $$  $$    $$      
       ####   #####    #####  $$$$$   $$  $$    $$      
  ======================================================
  SLS SAT Solver from The University of British Columbia
  ======================================================
  ...... Developed & Maintained by Dave Tompkins .......
  ------------------------------------------------------
  ...... consult legal.txt for legal information .......
  ------------------------------------------------------
  .... project website: http://ubcsat.dtompkins.com ....
  ------------------------------------------------------
  source repository: https://github.com/dtompkins/ubcsat
  ------------------------------------------------------
  ..... contact us at ubcsat [@] googlegroups.com ......
  ------------------------------------------------------

*/

#ifndef UBCSAT_H

#define UBCSAT_H

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

// sys lib added by chenwx
#include <utility>
#include <boost/dynamic_bitset.hpp>
#include <sys/resource.h>

#include "mt19937ar.h"

#include "ubcsat-limits.h"
#include "ubcsat-types.h"

#include "ubcsat-lit.h"
#include "ubcsat-mem.h"
#include "ubcsat-time.h"
#include "ubcsat-io.h"
#include "ubcsat-internal.h"
#include "ubcsat-globals.h"
#include "ubcsat-triggers.h"

#include "algorithms.h"
#include "reports.h"

#include "mylocal.h"

// user lib added by chenwx
#include "vig.h"
#include "utils.h"



#endif

