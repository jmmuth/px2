#!/bin/bash

# upload TD/minisat repository
lftp eggs << EOF
cd sched/
mirror -R ~/workspace/ubcsat -x .git -x cmake-* -x .idea -x res/ -e 
bye
EOF

# compile 
ssh albany.cs.colostate.edu "cd /s/chopin/b/grad/chenwx/sched/ubcsat; 
cmake -DCMAKE_BUILD_TYPE=Release;
make clean;
make -j 32;
cd py;
./genjobs.py"
