#!/usr/bin/python

# script for generating jobs
import os

# fixed settings
bin_file = "/s/chopin/m/proj/sched/chenwx/ubcsat/ubcsat"
cmd_file = "commands.lst"
out_dir = "/s/chopin/m/proj/sched/chenwx/ubcsat/res"  # never use a relative path here


def bsf_p():
    # parameters
    inst_set_path = "/s/chopin/m/proj/sched/chenwx/inst/lists/2014.crafted.indu.small.tw.n.pre.filtered.desk"
    # inst_set_path = "/home/chenwx/workspace/inst/lists/2014.crafted.indu.small.tw.n.pre.flitered.laptop"
    runs = 10
    rseed = 1
    max_flip = 100000
    max_evals = 0
    max_time_sec = 604800  # one day

    alg_range = ["adaptg2wsat"]
    px_range = [0, 1, 2]
    comp1_range = [0, 1]

    with open(cmd_file, 'w') as out, open(inst_set_path, 'r') as inst_set:
        inst_lines = inst_set.readlines()
        for line in inst_lines:
            for alg in alg_range:
                for px in px_range:
                    for comp1 in comp1_range:
                        cnf_name, cnf_file = line.split()
                        out_file = "{}/{}-{}-px{}-compone{}-cutoff{}.sol" \
                            .format(out_dir, cnf_name, alg, px, comp1, max_flip)

                        if not os.path.isfile(out_file):
                            if px and comp1:
                                job = "{} -i {} -inst {} -alg {} -cutoff {} -timeout {} -numevals {} -seed {} -runs {} -px {} -px-comp1 -r bestsol {}" \
                                    .format(bin_file, cnf_file, cnf_name, alg, max_flip, max_time_sec, max_evals, rseed,
                                            runs, px, out_file)
                            else:
                                job = "{} -i {} -inst {} -alg {} -cutoff {} -timeout {} -numevals {} -seed {} -runs {} -px {} -r bestsol {}" \
                                    .format(bin_file, cnf_file, cnf_name, alg, max_flip, max_time_sec, max_evals, rseed,
                                            runs, px, out_file)
                            print >> out, job
                        else:
                            print out_file, "exist"

                        if not px:
                            break


def bestn_pn():
    # parameters
    inst_set_path = "/s/chopin/m/proj/sched/chenwx/inst/lists/2014.crafted.indu.sample.pre.desk"

    runs = 10
    rseed = 1
    max_flip = 'max'
    max_time_sec = 5000
    max_mem = 8192  # maximal jobs on a machine with 16GB memory is therefore 2.

    alg_range = ["sparrow"]

    px_ratio_range = [None, 1, 2, 4, 8, 16]

    with open(cmd_file, 'w') as out, open(inst_set_path, 'r') as inst_set:
        inst_lines = inst_set.readlines()
        for line in inst_lines:
            if line[0] == "#": continue
            for alg in alg_range:
                for px_ratio in px_ratio_range:
                    cnf_name, cnf_file = line.split()
                    file_prefix = "{}/{}-{}-pxratio{}".format(out_dir, cnf_name, alg, px_ratio)
                    sol_file = file_prefix + ".sol"
                    beststep_file = file_prefix + ".beststep"
                    out_file = file_prefix + ".out"
                    err_file = file_prefix + ".err"
                    stat_file = file_prefix + ".stat"

                    if not os.path.isfile(stat_file) or os.stat(stat_file).st_size == 0:
                        if px_ratio:  # px enabled
                            job = "{} -i {} -alg {} -cutoff {} -timeout {} -memlim {} -seed {} -runs {} -pxratio {} -r bestsol {} -r beststep {} -r out {} default,pxsuccrate,pxnumcomp -r err {} -r stats {}" \
                                .format(bin_file, cnf_file, alg, max_flip, max_time_sec, max_mem, rseed, runs,
                                        px_ratio, sol_file, beststep_file, out_file, err_file, stat_file)
                        else:
                            job = "{} -i {} -alg {} -cutoff {} -timeout {} -memlim {} -seed {} -runs {} -r bestsol {} -r beststep {} -r out {} -r err {} -r stats {}" \
                                .format(bin_file, cnf_file, alg, max_flip, max_time_sec, max_mem, rseed, runs,
                                        sol_file, beststep_file, out_file, err_file, stat_file)
                        print >> out, job
                    else:
                        print stat_file, "exist"


def multi_bsf():
    # parameters
    inst_set_path = "/s/chopin/m/proj/sched/chenwx/inst/lists/2014.crafted.indu.sample.pre.desk"

    runs = 10
    rseed = 1
    max_flip = 'max'
    max_time_sec = 5000
    alg = "adaptg2wsat"
    max_mem = 8192

    px_ratio_ranges = [
        [1, 2, 4, 8, 16],  # for px0
        [1, 1 / 2., 1 / 4., 1 / 8., 1 / 16.]  # for px1
    ]

    px_range = [None, 0, 1]
    # px1_delay_range = [0, 1]
    # px1_allbsf_range = [0, 1]

    with open(cmd_file, 'w') as out, open(inst_set_path, 'r') as inst_set:
        inst_lines = inst_set.readlines()
        for line in inst_lines:
            if line[0] == "#": continue
            cnf_name, cnf_file = line.split()
            for px in px_range:
                if px == None:  # original
                    px_ratio = None
                    file_prefix = "{}/{}-px{}-pxratio{}".format(out_dir, cnf_name, px, px_ratio)
                    sol_file = file_prefix + ".sol"
                    out_file = file_prefix + ".out"
                    err_file = file_prefix + ".err"
                    stat_file = file_prefix + ".stat"

                    if not os.path.isfile(stat_file) or os.stat(stat_file).st_size == 0:
                        job = "{} -i {} -alg {} -cutoff {} -timeout {} -memlim {} -seed {} -runs {} -r bestsol {} -r out {} -r stats {} 2>{}" \
                            .format(bin_file, cnf_file, alg, max_flip, max_time_sec, max_mem, rseed, runs,
                                    sol_file, out_file, stat_file, err_file)
                        print >> out, job
                    else:
                        print stat_file, "exist"
                else:  # px enabled
                    px_ratio_range = px_ratio_ranges[px]
                    for px_ratio in px_ratio_range:
                        file_prefix = "{}/{}-px{}-pxratio{}".format(out_dir, cnf_name, px, px_ratio)
                        sol_file = file_prefix + ".sol"
                        out_file = file_prefix + ".out"
                        err_file = file_prefix + ".err"
                        stat_file = file_prefix + ".stat"
                        if not os.path.isfile(stat_file) or os.stat(stat_file).st_size == 0:
                            job = "{} -i {} -alg {} -cutoff {} -timeout {} -memlim {} -seed {} -runs {} -pxid {} -pxratio {} -r bestsol {} -r out {} default,pxsuccrate,pxnumcomp -r stats {} default,pxinittime 2>{}" \
                                .format(bin_file, cnf_file, alg, max_flip, max_time_sec, max_mem, rseed, runs,
                                        px, px_ratio, sol_file, out_file, stat_file, err_file)
                            print >> out, job
                        else:
                            print stat_file, "exist"


def postrun_px():
    # parameters
    inst_set_path = "/s/chopin/m/proj/sched/chenwx/inst/lists/2014.crafted.indu.sample.px.pre.desk"
    sol_dir = '/s/chopin/m/proj/sched/chenwx/0485b46-multibsf'

    runs = 0
    alg = "adaptg2wsat"
    max_mem = 8192

    px_ratio_ranges = [
        [1, 2, 4, 8, 16],  # for px0
        [1, 1 / 2., 1 / 4., 1 / 8., 1 / 16.]  # for px1
    ]
    px_range = [None, 0, 1]

    with open(cmd_file, 'w') as out, open(inst_set_path, 'r') as inst_set:
        inst_lines = inst_set.readlines()
        for line in inst_lines:
            if line[0] == "#": continue
            cnf_name, cnf_file = line.split()
            for px in px_range:
                if px == None:  # original
                    px_ratio = None
                    base_config = '{}-px{}-pxratio{}'.format(cnf_name, px, px_ratio)
                    file_prefix = "{}/{}".format(out_dir, base_config)
                    out_file = file_prefix + ".out"
                    err_file = file_prefix + ".err"
                    if not os.path.isfile(out_file) or os.stat(out_file).st_size == 0:
                        job = "{} -i {} -alg {} -memlim {} -runs {} -pxsol {}/{}.sol -r out null -r stats null 1>{} 2>{}" \
                            .format(bin_file, cnf_file, alg, max_mem, runs, sol_dir, base_config, out_file, err_file)
                        print >> out, job
                    else:
                        print out_file, "exist"

                else:  # px enabled
                    px_ratio_range = px_ratio_ranges[px]
                    for px_ratio in px_ratio_range:
                        base_config = '{}-px{}-pxratio{}'.format(cnf_name, px, px_ratio)
                        file_prefix = "{}/{}".format(out_dir, base_config)
                        out_file = file_prefix + ".out"
                        err_file = file_prefix + ".err"
                        if not os.path.isfile(out_file) or os.stat(out_file).st_size == 0:
                            job = "{} -i {} -alg {} -memlim {} -runs {} -pxsol {}/{}.sol -r out null -r stats null 1>{} 2>{}" \
                                .format(bin_file, cnf_file, alg, max_mem, runs, sol_dir, base_config, out_file,
                                        err_file)
                            print >> out, job
                        else:
                            print out_file, "exist"


def bestn_pn_on_indu_maxsat():
    """evaluating bestn X pn on industrial maxsat instances

    :return:
    """
    # parameters
    inst_set_path = "/s/chopin/m/proj/sched/chenwx/inst/lists/2016.indu.maxsat.desk"

    runs = 10
    rseed = 1
    max_flip = 'max'
    max_time_sec = 300
    alg = "sparrow"
    max_mem = 8192

    px_ratio_ranges = [
        [1 / 2., 1 / 4., 1, 2, 4]  # for px0
        # [1, 1 / 2., 1 / 4., 1 / 8., 1 / 16.]  # for px1
    ]

    px_range = [None, 0]

    with open(cmd_file, 'w') as out, open(inst_set_path, 'r') as inst_set:
        inst_lines = inst_set.readlines()
        for line in inst_lines:
            if line[0] == "#": continue
            cnf_name, cnf_file = line.split()
            for px in px_range:
                if px == None:  # original
                    px_ratio = None
                    file_prefix = "{}/{}-px{}-pxratio{}".format(out_dir, cnf_name, px, px_ratio)
                    sol_file = file_prefix + ".sol"
                    out_file = file_prefix + ".out"
                    err_file = file_prefix + ".err"
                    stat_file = file_prefix + ".stat"

                    if not os.path.isfile(stat_file) or os.stat(stat_file).st_size == 0:
                        job = "{} -i {} -alg {} -cutoff {} -timeout {} -memlim {} -seed {} -runs {} -r bestsol {} -r out {} -r stats {} 2>{}" \
                            .format(bin_file, cnf_file, alg, max_flip, max_time_sec, max_mem, rseed, runs,
                                    sol_file, out_file, stat_file, err_file)
                        print >> out, job
                    else:
                        print stat_file, "exist"
                else:  # px enabled
                    px_ratio_range = px_ratio_ranges[px]
                    for px_ratio in px_ratio_range:
                        file_prefix = "{}/{}-px{}-pxratio{}".format(out_dir, cnf_name, px, px_ratio)
                        sol_file = file_prefix + ".sol"
                        beststep_file = file_prefix + ".beststep"
                        out_file = file_prefix + ".out"
                        err_file = file_prefix + ".err"
                        stat_file = file_prefix + ".stat"
                        if not os.path.isfile(stat_file) or os.stat(stat_file).st_size == 0:
                            job = "{} -i {} -alg {} -cutoff {} -timeout {} -memlim {} -seed {} -runs {} -pxid {} -pxratio {} -r bestsol {} -r beststep {} -r out {} default,pxsuccrate,pxnumcomp -r stats {} default,pxinittime 2>{}" \
                                .format(bin_file, cnf_file, alg, max_flip, max_time_sec, max_mem, rseed, runs,
                                        px, px_ratio, sol_file, beststep_file, out_file, stat_file, err_file)
                            print >> out, job
                        else:
                            print stat_file, "exist"


def num_diff_vs_num_comp():
    ratio_range = [1 / 512., 1 / 256., 1 / 128., 1 / 64., 1 / 32., 1 / 16., 1 / 8., 1 / 4., 1 / 2., 1, 2, 4, 8, 16, 32]
    inst_file = '/s/chopin/m/proj/sched/chenwx/inst/lists/2014.crafted.indu.one.sample.pre.desk'

    with open(inst_file, 'r') as inst_set, open(cmd_file, 'w') as out:
        inst_lines = inst_set.readlines()
        for line in inst_lines:
            inst_name, cnf_file = line.split()
            for r in ratio_range:
                prefix_file = '{}/{}-pxratio{}'.format(out_dir, inst_name, r)
                job = "{} -alg sparrow -i {} -seed 1 -runs 3 -cutoff 10000000 -timeout 5000 -memlim 8192 -pxratio {} -r out stdout default,pxsuccrate -r pxcomp {}.comp -r stats {}.stat" \
                    .format(bin_file, cnf_file, r, prefix_file, prefix_file)
                print >> out, job

if __name__ == '__main__':
    num_diff_vs_num_comp()
