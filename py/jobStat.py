#!/usr/bin/env python
import subprocess
import sys
from optparse import OptionParser

import sssched as ss

if __name__ == "__main__":

    cmdLine = OptionParser(description="Simple script scheduler (sssched) for remote job management")
    cmdLine.add_option("-m", "--machines",
                       dest="machinesFileName",
                       action="store",
                       help="File with list of machines to use (default:\"machines.lst\")",
                       type="string")

    (options, args) = cmdLine.parse_args()
    # Get the list of the machines
    if options.machinesFileName is None:
        machinesList = ss.readListFile("machines.lst")
    else:
        machinesList = ss.readListFile(options.machinesFileName)

    for m in machinesList:
        p = subprocess.Popen('ssh -o ConnectTimeout=3 ' + m + ' \"ps -o comm,etime -U chenwx  | sed 1d | grep ubcsat\"',
                             shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
        text = p.stdout.read()
        print "**************************************", m, "**************************************"
        print text

    sys.exit()
